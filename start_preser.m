% ------------------------------------
% MOVED TO START_ME_UP               |
%                                    |
%	Uncomment the part below for start |
% Preser  without graphic inteface.  |
% ------------------------------------
% clear all
% close all
% clc
% addpath_preser;
%

%Reading configuration
% ini = IniConfig();
% ini.ReadFile('Preser.ini');
% 
% matlab_warning = strcmp('true',ini.GetValues('hilas','matlab_warning'));
% java_log = strcmp('true',ini.GetValues('hilas','java_log'));
% matlabcontrol_log = strcmp('true',ini.GetValues('hilas','matlabcontrol_log'));
% start_thread = strcmp('true',ini.GetValues('hilas','start_thread'));
% thread_sleep = ini.GetValues('hilas','thread_sleep');
% init_proc_name = ini.GetValues('hilas', 'init_procedure');
% proc_name = ini.GetValues('hilas', 'procedure');
% robot_name = ini.GetValues('robot', 'name');
% sim_address = ini.GetValues('simulator', 'socketaddr');
% sim_port = ini.GetValues('simulator', 'socketport');
% scene = ini.GetValues('simulator', 'scene');
% startScene = strcmp('true',ini.GetValues('simulator', 'startScene'));

% Loading corke toolbox
startup_rvc;

% Disable warnings
if matlab_warning
	warning('on','all');
else
	warning('off','all');	
end

% Create socket connection with the simulator
vrep = remApi('remoteApi');
vrep.simxFinish(-1);
id = vrep.simxStart(sim_address, sim_port, true, false, 10000, 5);

if id < 0
	fprintf('Failed connecting to remote API server. Exiting.');
	vrep.delete();
	return;
end

fprintf('Connection %d to remote API server open.\n', id);

if(start_scene)
    if (id > -1)
        disp('Connected to remote API server');
        myPath = pwd;
        newPath = [myPath filesep strcat(strcat('simulator',filesep),'scenes')];
        myScene = [newPath filesep scene];
        res = vrep.simxLoadScene(id,myScene,0,vrep.simx_opmode_oneshot_wait);
        vrchk(vrep, res);
        pause(1);
   end
end

% Add path for java execution
javaaddpath 'java'

% This will only work in "continuous remote API server service"
vrep.simxStartSimulation(id, vrep.simx_opmode_oneshot_wait);
vrep.simxSynchronous(id,true);

% Building phase
global robot robot_kine cartesian_ctrl t;

eval(strcat(strcat('robot = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'(vrep,id);'));
eval(strcat(strcat('robot_kine = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Kinematics(vrep, id);'));
cartesian_ctrl = CartesianController();

% Configuration phase
robot.configure();
robot_kine.configure();
cartesian_ctrl.configure();

% Start-up phase
robot.start();
robot_kine.start();
cartesian_ctrl.start();

% Init procedure
eval(init_proc_name);

% First run for remove errors
vrep.simxSynchronousTrigger(id);
t = vrep.simxGetLastCmdTime(id);
eval(proc_name);

if(start_thread)
	% Start Runner
	hilas = HilasRunner(thread_sleep, proc_name, java_log, matlabcontrol_log);
	start(hilas);
end