classdef Hokuyo < ISensor
	properties(Access = private)
		signal
		num_points
		map
	end
		
	properties(Access = public)
		out_sensed_points
		out_laser_points
	end
	
	methods(Access = private)
			function getMap(obj)
			[res,obj.signal] = obj.vrep.simxReadStringStream(obj.id,'rangeDat',obj.vrep.simx_opmode_buffer);
			vrchk(obj.vrep, res);
			obj.map = obj.vrep.simxUnpackFloats(obj.signal);
			obj.out_sensed_points = [];
			
			i = 1; k = 1;
			while i <= (length(obj.map)-3)
				obj.out_sensed_points(:,k) = [obj.map(i) obj.map(i+1) obj.map(i+2)];
				i = i + 3;
				k = k +1;
			end			
		end
		
		function laserPointsToLaserData(obj)
			if size(obj.out_sensed_points) > 0
				points = obj.out_sensed_points;
				obj.out_laser_points.ranges = zeros(1,obj.num_points);
				
				for k=1:length(points)
					x = points(1,k);
					y = points(2,k);
					range_sq = x^2 + y^2;
					angle = atan2(y, x);
					index = floor((angle - obj.out_laser_points.angle_min) / obj.out_laser_points.angle_increment);
					
					if index == 0
						index = 1;
					end
					
					if (angle < obj.out_laser_points.angle_min || angle > obj.out_laser_points.angle_max)
						continue;
					else
						obj.out_laser_points.ranges(index) = sqrt(range_sq);	
					end
				end
				obj.out_laser_points.ranges = obj.out_laser_points.ranges(1:obj.num_points);
			end
			
			% update time
			global t;
			obj.out_laser_points.stamp = [0 t*1000];
		end
	end

	methods(Access = protected)
		function getData(obj)
			obj.getMap();
			obj.laserPointsToLaserData();
		end		
	end
	
	methods
		function obj = Hokuyo(vrep, id)
			obj = obj@ISensor(vrep, id, {'base_laser_front_link'});
			
			obj.out_laser_points = data_laser;

			obj.out_laser_points.angle_min = -pi/2;
			obj.out_laser_points.angle_max = pi/2;
			obj.out_laser_points.angle_increment = 0.031415927;%0.0698;
			obj.out_laser_points.range_min = 0;
			obj.out_laser_points.range_max	= 5;
			obj.out_laser_points.time_increment = 0;
			obj.out_laser_points.stamp = [0 0];
			obj.out_laser_points.ranges = -1;
			obj.out_laser_points.intensities = 0;						

			obj.num_points = 100;			
			obj.vrep.simxReadStringStream(obj.id,'rangeDat',obj.vrep.simx_opmode_streaming);
		end
	end
end