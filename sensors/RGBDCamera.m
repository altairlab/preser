% Simulation is really slow with update ISensor (pose,data).
% Use signal, instead using simxGetVisionSensor

classdef RGBDCamera	< ISensor
    properties(Access = private)
        depth_resolution
        rgb_resolution
        
        
    end
    
    properties(Access = public)
        out_rgb_buffer
        out_depth_buffer
%         out_depth_value       % in V-REP 3.3.2 
        out_points
        depth_sensor
        rgb_sensor
        cameraInfo
    end
    
    methods(Access = private)
        function getDepthMap(obj)
            [res,obj.depth_resolution,obj.out_depth_buffer] = obj.vrep.simxGetVisionSensorDepthBuffer2(obj.id, obj.depth_sensor, obj.vrep.simx_opmode_buffer);
            vrchk(obj.vrep, res);
%             simxGetVisionSensorDepthBuffer 
%             obj.out_depth_buffer.setDataType('singlePtr',1,obj.depth_resolution(1) * obj.depth_resolution(2))
%             obj.out_depth_value = obj.out_depth_buffer.value(pixelIndex);
        end
        
        
        
        function getRGBMap(obj)
            [res,obj.rgb_resolution,obj.out_rgb_buffer] = obj.vrep.simxGetVisionSensorImage2(obj.id, obj.rgb_sensor, 0, obj.vrep.simx_opmode_buffer);
            vrchk(obj.vrep, res);
            %             h_rgb = findobj('Tag','axes_rgb');
            %             imshow(obj.out_rgb_buffer,'Parent',h_rgb);
            %             drawnow;
        end
        
        function getPoints(obj)
            [~,data] = obj.vrep.simxGetStringSignal(obj.id,'mData',obj.vrep.simx_opmode_oneshot_wait);
            [floatValues] = obj.vrep.simxUnpackFloats(data);
            msize = size(floatValues);
            obj.out_points = zeros(msize(1,2)/3 ,3);
            for i=1:msize(1,2)/3
                obj.out_points(i,1) = floatValues(1,i*3-2);
                obj.out_points(i,2) = floatValues(1,i*3-1);
                obj.out_points(i,3) = floatValues(1,i*3);
            end
        end
    end
    
    methods(Access = protected)
        function getData(obj)
%             obj.getDepthMap();
            obj.getRGBMap();
            obj.getPoints();
        end
    end
    
    methods
        function obj = RGBDCamera(vrep, id, sensor_name, camera_name, depth_name)
            obj = obj@ISensor(vrep, id, {sensor_name});
            
            if exist([strcat(evalin('base','PreserPath.ROOT_DIR'),'/common/vrep_cameraCalibration/') ,'cameraParams.mat'],'file')
                disp('Loading Camera Parameters');
                load('cameraParams.mat');
                obj.cameraInfo = cameraParams;
            else
                 obj.cameraInfo = param_script;
                % disp('You Need to Calibrate Camera');
                % save image in path, take a snap and calibrate    
                % cameraCalibrator( strcat(evalin('base','PreserPath.ROOT_DIR'),'/common/vrep_cameraCalibration '), 300 ); % 300 mm , size of square in images
            end
            
            
            [~, obj.depth_sensor] = vrep.simxGetObjectHandle(id, depth_name, vrep.simx_opmode_oneshot_wait) ;    % ISensor(vrep, id, {depth_name});
            [~, obj.rgb_sensor] = vrep.simxGetObjectHandle(id, camera_name, vrep.simx_opmode_oneshot_wait)  ;    %ISensor(vrep, id, {camera_name});
            
            % Enabling buffering
%             obj.vrep.simxGetVisionSensorDepthBuffer2(obj.id, obj.depth_sensor, obj.vrep.simx_opmode_streaming);
            obj.vrep.simxGetVisionSensorImage2(obj.id, obj.rgb_sensor, 0, obj.vrep.simx_opmode_streaming);
        end
    end
end


% TODO
% Interfaccia che controlla braccio
% fare checkbox con
%  [ON OFF] vrep.simxSetIntegerSignal(id, 'camera_ON', 1,vrep.simx_opmode_oneshot);
%  Per prendere dati dalla camera
% figure,scatter3(camera.out_points(:,1),camera.out_points(:,2),camera.out_points(:,3),'.')
% mostra la point cloud