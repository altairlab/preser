robot.Base(1).in_twist_cmd = joypad.out_twist_cmd;

amcl.in_odometry = robot.Base(1).out_odometry;
amcl.in_laser_data = laser.out_laser_points;

joypad.update();
amcl.update();
% laser.update();
robot.Base(1).update();