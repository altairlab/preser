robot.Base(1).in_twist_cmd = joypad.out_twist_cmd;

slam.in_odometry = robot.Base(1).out_odometry;
slam.in_laser_data = laser.out_laser_points;

path_planner.in_odometry = robot.Base(1).out_odometry;
path_planner.in_occupacy_grid = slam.out_occupacy_grid;

joypad.update();
slam.update();
% laser.update();
robot.Base(1).update();

robot.Base(1).out_odometry.pose.pose.position