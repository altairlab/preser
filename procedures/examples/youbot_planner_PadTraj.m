path_planner.in_odometry = robot.Base(1).out_odometry;
if evalin('base','joy') == 1
    robot.Base(1).in_twist_cmd = joypad.out_twist_cmd;
    joypad.update();
else
    robot.Base(1).in_twist_cmd = traj.out_twist_cmd;
    traj.update();
end
% laser.update();
robot.update();