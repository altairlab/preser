PreserPath.ROOT_DIR = pwd;

% Check if robotic toolbox is downloaded
if exist('rvctools', 'dir') == false
	url = 'http://www.petercorke.com/RTB';
	fprintf('Robotic Toolbox not already downloaded!\n');
	fprintf('Download the leatest version from http://www.petercorke.com/ and place the folder rvctools in the root\n');
	web(url, '-browser');
	return;
end

% Adding path for simulator libraries
addpath('simulator/V-REP','simulator/scripts')
addpath(genpath('robots'))
addpath('sensors')
addpath('definitions')

if exist('procedures', 'dir') == false
	mkdir('procedures');
else
		addpath(genpath('procedures'));
end

addpath('common','common/interfaces','common/ini_parser','common/gui','common/vrep_cameraCalibration')
addpath('config');
addpath('api');
addpath(genpath('algorithms'));
addpath('rvctools');