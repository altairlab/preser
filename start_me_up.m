% Clearing workspace
clear all
close all
clc

% Configure workspace
addpath_preser;

% Load last configuration
ini = IniConfig();
api.readConfigFromFile();
joy = 1;

% Main GUI
PreserController();