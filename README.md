# Preser installation #
Clone the repository with the flag --recursive for getting the other dependencies (i.e gmapping c++ core):
```
git clone --recursive -b master https://bitbucket.org/altairlab/preser.git
```
Then download Peter Corke Toolbox [here](http://www.petercorke.com/RTB/) and unzip it in the base folder. Matlab 2014a or later and the following toolboxes are required:

* Simulink Toolbox
* Aerospace Toolbox

## Mexfile ##
To compile a mexfile you will need a c++ compiler installed on your system, then for each algorithm written in c++ that you need to use run the `compile` script. 
## Java ##
If you need to compile the simulation runner, remember to append to the java classpath the path to jmi.jar. For linux user add this command to the bashrc file and adjust the path if needed `export CLASSPATH=$CLASSPATH:matlab-root/java/jar/jmi.jar`.
The runner must be compiled with the same or later version of java embed in Matlab, and to get the Matlab java version use the following command `version -java`.
## Simulator ##
To get the simulator download the latest version of V-REP from Coppelia Robotics (http://www.coppeliarobotics.com/). The simulator standard configuration is ready to be used with remote Api. In case you want to customize port number and debug console you have to edit the **remoteApiConnections.txt** placed in the base folder of the simulator.
# Usage #
After the preliminary setup open Matlab and move the current folder to the Preser base folder, if not exist create a procedure folder to store your code. Then change the **Hilas.ini** file to reflect your desired configuration and type `start_me_up` in the command console. For a better understanding of the software architecture please read something about *Component-based software engineering* (i.e http://en.wikipedia.org/wiki/Component-based_software_engineering).
## Procedures ##
For defining a procedure (i.e your program) you have to create two scripts: a initialization one (invoked only one time before the first simulation step) and the main procedure (invoked every simulation step). These scripts must be written in Matlab code and placed inside the procedures folder. The standard workflow for the main procedure is:

1. Define components port interconnections
2. Call update components function

This is an example for a main procedure that use SLAM GMapping, laser scan, the robot class and a joypad for drive the robot with a twist command.
```
robot.Base(1).m_twist_cmd = joypad.out_twist_cmd;
slam.in_odometry = robot.Base(1).out_odometry;
slam.in_laser_data = laser.out_laser_points;

path_planner.in_odometry = robot.Base(1).out_odometry;
path_planner.in_occupacy_grid = slam.out_occupacy_grid;

joypad.update();
slam.update();
laser.update();
robot.update();
```
In other hand for the initial procedure doesn't exist a standard workflow, mainly this procedures it's used only for the non-standard object instantiation. Below there's an example for an initial procedure: robot, robot_kine and cartesian_ctrl objects are automatically created by the start_me_up script.
```
api.armSetCtrlModes(1,4);
api.baseSetCtrlModes(1,5);

joypad = XBoxJoypad();
laser = Hokuyo(vrep,id);
slam = GMapping();

path_planner = AStar();
```
## Configuration ##
Preser is fully configurable via .ini file that are stored in the config folder. The main configuration file is **Preser.ini** and each section is described below:

Section *hilas*:

| parameter | value | content |
| ----- | --- | ------ |
|start_thread| true/false | Start the simulation loop after the first step|
| thread_sleep      | 1-n | Set the sleep time between each simulation step (ms)|
| init_procedure    | string | Set the name of the initialization procedure (usefull for object instantiate, robot setup etc...). This name reflect the Matlab script name `your_init_procedure.m`|
| procedure         | string | Set the name of the procedure executed every simulation step (set component ports connection, invoke update component etc...) This name reflect the Matlab script name `your_procedure.m`|
| matlab_warning    | true/false | Enable the warning in the Matlab console |
| java_log          | true/false | Enable log for simulation runner |
| matlabcontrol_log | true/false | Enable debug console for jmi.jar |

Section *simulator*:

| parameter | value | content |
| ----- | --- | ------ |
| socketaddr      | ip address | Set the ip address where the simulator resides|
| socketport      | number | Set the port number for the socket connection |
| scene    | string | Set the name of the scene to be loaded at startup (the scene name doesn't include file extension your_scene_name.ttt)|
| startScene         | true/false | Set if the scene is loaded via Preser|

Section *robot*:

| parameter | value | content |
| ----- | --- | ------ |
| name      | string | Set the name of the robot used in the simulation. The robot name reflect the real robot name|

## Command ##
Once the system is setted up, the simulation driver is wrapped in a workspace variable called **hilas** with a couple of usefull functions:

* `hilas.suspend`: Suspend the execution of the runner thread.
* `hilas.resume`: Resume the execution of the runner thread.
* `hilas.stopRun`: Stop the execution of the runner thread, stop the 
simulator and exit the loop (the command **doesn't** clean the Matlab workspace).

### API ###
The **General API** have a couple of functions usefull for plotting data:

* `plotRobotKine()`: Plot the kinematic chain.
* `plotRobotKineWorkspace(cube_size)`: Plot the kinematic chain with a workspace dimention ( needed for prismatic joint).
* `plotLaserPoints(laser)`: Plot the 2D laser points in the laser reference frame.
* `plotMap(slam, robot, amcl)`: Plot the occupacy grid from slam as an image. Slam, amcl and robot pose are displayed in overlay with circles of different colour (red = slam correction, green = amcl correction, blue = robot odometry).
* `plotStaticMap(static_map, robot, amcl)`: Plot the occupacy grid from a static map. Amcl and robot pose are displayed in overlay with circles of different colour (green = amcl correction, blue = robot odometry).
* `saveMap(slam)`: Save the occupacy grid in .mat file. Filename is map_yymmddHHMMSSFFF of your system.
* `loadMap(filename)`: Load the occupacy grid from a file and store the grid in a workspace variable called static_map.

With **the Robot API** is possible to modify the robot control loop parameters and joint target:

* `armSetCtrlModes(index, k)`: Set the specified control mode for the arm service.
* `armSetPos(index,joint_pos)`: Set the control mode to *PLANE_ANGLE* and set the target positions.
* `armSetVel(index,joint_vel)`: Set the control mode to *ANGULAR_VELOCITY* and set the target velocities.
* `armSetTor(index,joint_tor)`: Set the control mode to *TORQUE* and set the target torques (**not yet implemented**).
* `baseSetCtrlModes(index, k)`: Set the control mode for the base service.
* `baseSetPos(index,joint_pos)`: Set the control mode to *PLANE_ANGLE* and set the target positions (**deprecated**).
* `baseSetVel(index,joint_vel)`: Set the control mode to *ANGULAR_VELOCITY* and set the target velocities.
* `baseSetTor(index,joint_tor)`: Set the control mode to *TORQUE* and set the target torques (**not yet implemented**).
* `baseSetTwist(index,twist)`: Set the control mode to *TWIST* and set the specified twist command.
* `gripperSetCtrlModes(index, k)`: Set the specified control mode for the gripper service.
* `gripperSetStat(index,isToBeOpened)`: Set the specified target state for the gripper.

The control modes parameter set the type of loop controller. I.e Setting the control mode to *PLANE_ANGLE* the control loop will **only apply** the joint position target. The *MOTOR_STOP* mode is different for the other ones. Specifying it a joint position controller is activated which keeps fixed the instantaneous joints position. The control modes can be specified via enumeration or integer values:
```
PLANE_ANGLE (1)
ANGULAR_VELOCITY (2)
TORQUE (3)
MOTOR_STOP (4)
TWIST (5)
```
i.e `armSetCtrlModes(1,MOTOR_STOP)` is equal as `armSetCtrlModes(1,5)`.

## Remarks ##
Due to concurrent workspace access sometimes (mainly for long take time functions) is possible to have error messages in the console.

The simulation is driven by Matlab via the runner java thread, so the simulation time increase only when the runner is executed.
