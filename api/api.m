classdef api
	methods(Static)
		% -----------------------
		% General api
		% -----------------------
		function plotRobotKine()
			global robot_kine;
			robot_kine.robot_chain.plot(robot_kine.robotChain2kinematicChain());
		end
		
		function plotRobotKineWorkspace(cube_size)
			global robot_kine;
			robot_kine.robot_chain.plot(robot_kine.robotChain2kinematicChain(),'workspace',cube_size*[-1 1 -1 1 -1 1]);
		end
		
		function plotLaserPoints(laser)
			points = laser.out_sensed_points;
			rot_cumuate = bsxfun(@plus, zeros(4,size(points,2)), quatinv(struct2array(laser.out_sensor_pose.orientation))');
			points = quatrotate(rot_cumuate',points')';
			points = bsxfun(@plus, points, struct2array(laser.out_sensor_pose.position)');
			plot(-points(2,:),points(1,:),'o');
		end
		
		function plotStaticMap(static_map,amcl,robot)
				imshow(static_map.data,[0 100]);
				overlayLocalizationData(amcl,robot);
		end
		
		function overlayRobotData(static_map,robot)
				hold on
				scale = static_map.resolution;
				y_pix = fix(robot.Base(1).out_odometry.pose.pose.position.x/scale);
				x_pix = fix(robot.Base(1).out_odometry.pose.pose.position.y/scale);

				plot(size(static_map.data,1)/2+x_pix,size(static_map.data,2)/2+y_pix,'bo');				
		end
		
		function overlayLocalizationData(static_map,amcl,robot)
				hold on
				scale = static_map.resolution;
				y_pix = fix(robot.Base(1).out_odometry.pose.pose.position.x/scale);
				x_pix = fix(robot.Base(1).out_odometry.pose.pose.position.y/scale);
								
				y_pix_cor_a = fix(amcl.out_odometry.pose.pose.position.x/scale);
				x_pix_cor_a = fix(amcl.out_odometry.pose.pose.position.y/scale);

				plot(size(static_map.data,1)/2+x_pix,size(static_map.data,2)/2+y_pix,'bo');				
				plot(size(static_map.data,1)/2+x_pix_cor_a,size(static_map.data,2)/2+y_pix_cor_a,'go');			
		end
		
		function overlaySlamData(slam,robot,amcl)
			if nargin == 2
				hold on
				scale = slam.out_occupacy_grid.resolution;
				y_pix = fix(robot.Base(1).out_odometry.pose.pose.position.x/scale);
				x_pix = fix(robot.Base(1).out_odometry.pose.pose.position.y/scale);
				
				y_pix_cor = fix(slam.out_odometry.pose.pose.position.x/scale);
				x_pix_cor = fix(slam.out_odometry.pose.pose.position.y/scale);
				
				plot(size(slam.out_occupacy_grid.data,1)/2+x_pix_cor,size(slam.out_occupacy_grid.data,2)/2+y_pix_cor,'ro');
				plot(size(slam.out_occupacy_grid.data,1)/2+x_pix,size(slam.out_occupacy_grid.data,2)/2+y_pix,'bo');
			end
			
			if nargin == 3
				hold on
				scale = slam.out_occupacy_grid.resolution;
				y_pix = fix(robot.Base(1).out_odometry.pose.pose.position.x/scale);
				x_pix = fix(robot.Base(1).out_odometry.pose.pose.position.y/scale);
				
				y_pix_cor = fix(slam.out_odometry.pose.pose.position.x/scale);
				x_pix_cor = fix(slam.out_odometry.pose.pose.position.y/scale);
				
				y_pix_cor_a = fix(amcl.out_odometry.pose.pose.position.x/scale);
				x_pix_cor_a = fix(amcl.out_odometry.pose.pose.position.y/scale);

				plot(size(slam.out_occupacy_grid.data,1)/2+x_pix_cor,size(slam.out_occupacy_grid.data,2)/2+y_pix_cor,'ro');
				plot(size(slam.out_occupacy_grid.data,1)/2+x_pix,size(slam.out_occupacy_grid.data,2)/2+y_pix,'bo');				
				plot(size(slam.out_occupacy_grid.data,1)/2+x_pix_cor_a,size(slam.out_occupacy_grid.data,2)/2+y_pix_cor_a,'go');
			end			
		end
		
		function plotMap(slam,robot,amcl)
			imshow(slam.out_occupacy_grid.data,[0 100]);
			if nargin == 2
				api.overlaySlamData(slam,robot);
			end
			if nargin == 3
				api.overlaySlamData(slam,robot,amcl);
			end
		end

		function saveMap(slam,filename)
			if ~exist('saved','dir')
				mkdir('saved')
			end
			
			grid = slam.out_occupacy_grid;
			
			if(nargin == 2)
				save(strcat('saved/',filename),'grid');
			else
				save(strcat('saved/map_',datestr(now,'yymmddHHMMSSFFF')),'grid');
			end
		end

		function loadMap(filename)
			map = load(strcat('saved/',filename));
			assignin('base','static_map', map.grid);
		end
		
		function logic = to_logic_string(value)
			if(value >= 1)
				logic = 'true';
			else
				logic = 'false';
			end
		end
		
		function saveConfigToFile()
			evalin('base','ini.SetValues(''hilas'',''matlab_warning'', api.to_logic_string(matlab_warning));');
			evalin('base','ini.SetValues(''hilas'',''java_log'', api.to_logic_string(java_log));');
			evalin('base','ini.SetValues(''hilas'',''matlabcontrol_log'', api.to_logic_string(matlabcontrol_log));');
			evalin('base','ini.SetValues(''hilas'',''start_thread'', api.to_logic_string(start_thread));');
			evalin('base','ini.SetValues(''hilas'',''thread_sleep'', thread_sleep);');
			evalin('base','ini.SetValues(''hilas'',''init_procedure'', init_proc_name);');
			evalin('base','ini.SetValues(''hilas'',''procedure'', proc_name);');
			
			evalin('base','ini.SetValues(''robot'',''name'', robot_name);');
			
			evalin('base','ini.SetValues(''simulator'',''socketaddr'', sim_address);');
			evalin('base','ini.SetValues(''simulator'',''socketport'', sim_port);');
			evalin('base','ini.SetValues(''simulator'',''scene'', scene);');
			evalin('base','ini.SetValues(''simulator'',''start_scene'', api.to_logic_string(start_scene));');			
			evalin('base','ini.WriteFile(strcat(strcat(''config'',filesep),''Preser.ini''));');
		end
		
		function readConfigFromFile()
			evalin('base','ini.ReadFile(strcat(strcat(''config'',filesep),''Preser.ini''));');
			evalin('base','matlab_warning = strcmp(''true'',ini.GetValues(''hilas'',''matlab_warning''));');
			evalin('base','java_log = strcmp(''true'',ini.GetValues(''hilas'',''java_log''));');
			evalin('base','matlabcontrol_log = strcmp(''true'',ini.GetValues(''hilas'',''matlabcontrol_log''));');
			evalin('base','start_thread = strcmp(''true'',ini.GetValues(''hilas'',''start_thread''));');
			evalin('base','thread_sleep = ini.GetValues(''hilas'',''thread_sleep'');');
			evalin('base','init_proc_name = ini.GetValues(''hilas'', ''init_procedure'');');
			evalin('base','proc_name = ini.GetValues(''hilas'', ''procedure'');');
			evalin('base','robot_name = ini.GetValues(''robot'', ''name'');');
			evalin('base','sim_address = ini.GetValues(''simulator'', ''socketaddr'');');
			evalin('base','sim_port = ini.GetValues(''simulator'', ''socketport'');');
			evalin('base','scene = ini.GetValues(''simulator'', ''scene'');');
			evalin('base','start_scene = strcmp(''true'',ini.GetValues(''simulator'', ''start_scene''));');
		end
		
		% -----------------------
		% Robot command api
		% -----------------------
		function armSetCtrlModes(index, k)
			global robot;
			robot.Arm(index).setControlModesAll(ctrl_modes(k));
		end
		
		function armSetPos(index,joint_pos)
			global robot;
			api.armSetCtrlModes(index,ctrl_modes.PLANE_ANGLE);
			robot.Arm(index).in_positions_cmd = joint_pos;
		end
		
		function armSetVel(index,joint_vel)
			global robot;
			api.armSetCtrlModes(index,ctrl_modes.ANGULAR_VELOCITY);
			robot.Arm(index).in_velocities_cmd = joint_vel;
		end
		
		function armSetTor(index,joint_tor)
			global robot;
			api.armSetCtrlModes(index,ctrl_modes.TORQUE);
			robot.Arm(index).in_efforts_cmd = joint_tor;
		end
		
		function baseSetCtrlModes(index, k)
			global robot;
			robot.Base(index).setControlModesAll(ctrl_modes(k));
		end
		
		function baseSetPos(index,joint_pos)
			global robot;
			api.baseSetCtrlModes(index,ctrl_modes.PLANE_ANGLE);
			robot.Base(index).in_positions_cmd = joint_pos;
		end
		
		function baseSetVel(index,joint_vel)
			global robot;
			api.baseSetCtrlModes(index,ctrl_modes.ANGULAR_VELOCITY);
			robot.Base(index).in_velocities_cmd = joint_vel;
		end
		
		function baseSetTor(index,joint_tor)
			global robot;
			api.baseSetCtrlModes(index,ctrl_modes.TORQUE);
			robot.Base(index).in_efforts_cmd = joint_tor;
		end
		
		function baseSetTwist(index,twist)
			global robot;
			api.baseSetCtrlModes(index,ctrl_modes.TWIST);
			robot.Base(index).in_twist_cmd = twist;
		end
		
		function gripperSetCtrlModes(index, k)
			global robot;
			robot.Gripper(index).setControlModesAll(ctrl_modes(k));
		end
		
		function gripperSetStat(index,isToBeOpened)
			global robot;
			api.gripperSetCtrlModes(index,ctrl_modes.PLANE_ANGLE);
			if(isToBeOpened)
				robot.Gripper(index).in_positions_cmd = [0.0415 0.0415];
			else
				robot.Gripper(index).in_positions_cmd = [-0.0100 -0.0100];
			end
		end
		
	end
end