classdef ColorSegmentation < IImageSegmentation
    
    properties(Access = private)
        % Color Table
        % Color_Name  Hue_Cntr  Hue_BW  Sat_Cntr Sat_BW  Val_Cntr  Val_BW
        Color_Table = {
            'White'     0   50  0   50  255 110
            'Silver'    0   50  0   50  191 110
            'Gray'      0   50  0   50  128 110
            'Black'     128 300 128 300 0   50
            'Red'       0   30  255 120 230 150
            'Maroon'    0   30  245 120 100 100
            'Yellow'    43  25  245 120 230 150
            'Olive'     43  25  245 120 100 75
            'Lime'      84  25  245 120 230 150
            'Green'     90  50  210 180 50  120
            'Aqua'      127 10  185 200 170 120
            'Teal'      127 10  245 80  85  75
            'Blue'      170 30  160 200 150 220
            'Navy'      170 30  70  220 70  180
            'Magenta'   213 50  200 175 220 100
            'Purple'    212 260 100 220 70  180
            'Orange'    22  25  160 200 150 220
            };
        
    end
        
    methods
        function obj = ColorSegmentation(image)
            obj = obj@IImageSegmentation(image);
                      
        end
        
        
        function seg = getSegmentedRegion(obj,colorSelected)
            
            % Filter TF Builder. X = domain; C = center; BW = bandwidth
            % Cutoff points will be C + BW/2 and C - BW/2
            G = @(x,c,BW)  sqrt(1./(1+exp(-(1/2)*(x-(c-BW/2))))) + ...
                sqrt(1./(1+exp((1/2)*(x-(c+BW/2))))) - 1;
            
            % load image and create filters for the HSV channels
            img_HSV = uint8(rgb2hsv(obj.original_image)*255);
            x = 0:256;
            clrIdx = find(strcmpi(obj.Color_Table(:,1),colorSelected));
            if (isempty(clrIdx))
                disp('Color Not Recognized');
                return;
            end
            H_hue = G(x,obj.Color_Table{clrIdx,2},obj.Color_Table{clrIdx,3});
            H_sat = G(x,obj.Color_Table{clrIdx,4},obj.Color_Table{clrIdx,5});
            H_val = G(x,obj.Color_Table{clrIdx,6},obj.Color_Table{clrIdx,7});
            
            % apply filter and show the filtered images
            img_Hue_filtered = H_hue(img_HSV(:,:,1)+1);
            img_Sat_filtered = H_sat(img_HSV(:,:,2)+1);
            img_Val_filtered = H_val(img_HSV(:,:,3)+1);
            Combined = img_Hue_filtered.*img_Sat_filtered.*img_Val_filtered;
            
            %             figure, imshow(img_Hue_filtered)
            regionSeg = img_Hue_filtered;
            
            seg = data_centroids(regionSeg);
            obj.centroids = seg.centroids ;
            obj.regionSeg = seg.segmented_image;
        end
        
        function  colorInfo = getColorInfo(obj,colorSpace)
            switch colorSpace
                case 'rgb'
                    %                     figure,imshow(obj.original_image);
                    title('RGB Image');
                    [x,y] = ginput(1);
                    % Put a cross where they clicked.
                    hold on;
                    plot(x, y, 'w+', 'MarkerSize', 20);
                    % Get the location they click on.
                    row = int32(y);
                    column = int32(x);
                    colorInfo(1,1) =  obj.original_image(row, column, 1);
                    colorInfo(1,2) =  obj.original_image(row, column, 2);
                    colorInfo(1,3) =  obj.original_image(row, column, 3);
                    
                    obj.mod_image = obj.original_image;
                    
                    
                    
                case 'hsv'
                    hsvImage = rgb2hsv(obj.original_image);
                    % Display the original color image.
                    %                     figure,imshow(hsvImage);
                    title('HSV Image');
                    [x,y] = ginput(1);
                    % Put a cross where they clicked.
                    hold on;
                    plot(x, y, 'w+', 'MarkerSize', 20);
                    % Get the location they click on.
                    row = int32(y);
                    column = int32(x);
                    colorInfo(1,1) =  hsvImage(row, column, 1);
                    colorInfo(1,2) =  hsvImage(row, column, 2);
                    colorInfo(1,3) =  hsvImage(row, column, 3);
                    
                    obj.mod_image = hsvImage;
                    
                    
                    obj.regionSeg = obj.mod_image(:,:,1) > colorInfo(1,1) - 0.1 & obj.mod_image(:,:,1) < colorInfo(1,1) + 0.1
            end
            
        end
        
        
        function getPlot(obj,axes_handle)
            
            imshow(obj.regionSeg,'Parent',axes_handle)
            hold(axes_handle,'on')
            plot(axes_handle,obj.centroids(:,1),obj.centroids(:,2), 'r*')
            hold(axes_handle,'off')
            
        end
        
    end
end