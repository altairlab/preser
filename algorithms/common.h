#include <string>

typedef struct stime_s
{
	int32_t sec;
	int32_t nsec;

	stime_s operator-(stime_s a)
	{
		return{sec -  (int32_t)a.sec, nsec - (int32_t)a.nsec};
	}
}stamp_t;

typedef struct LaserScan_s
{
	double angle_min;
	double angle_max;
	double angle_increment;
	double time_increment;
	stamp_t stamp;
	double range_min;
	double range_max;
	std::vector<double> ranges;
	std::vector<double> intensities;
	double x;
	double y;
}LaserScan;

typedef struct Odometry_s
{
	double x;
	double y;
	double yaw;
}Odometry;

typedef struct Map_s
{
	double resolution;
	double width;
	double height;
	double position_x, position_y;
	std::vector<double> data;
	Odometry map_odom;
}Map;

typedef struct param_amcl_s
{
  bool first_map_only;
  double update_min_d;
  double update_min_a;
  int resample_interval;
  double laser_min_range;
  double laser_max_range;
  int laser_max_beams;
  double odom_alpha1;
  double odom_alpha2;
  double odom_alpha3;
  double odom_alpha4;
  double odom_alpha5;
  double laser_z_hit;
  double laser_z_short;
  double laser_z_max;
  double laser_z_rand;
  double laser_sigma_hit;
  double laser_lambda_short;
  double laser_likelihood_max_dist;
  std::string laser_model_type;
  std::string odom_model_type;
  double min_particles;
  double max_particles;
  double recovery_alpha_slow;
  double recovery_alpha_fast;
  double do_beamskip;
  double beam_skip_distance;
  double beam_skip_threshold;
  double kld_err;
  double kld_z;
}Parameters_amcl;

typedef struct param_gmapping_s
{
  double throttle_scans;
  int32_t map_update_interval;
  double minimum_score;
  double sigma;
  int kernelSize;
  double lstep;
  double astep;
  int iterations;
  double lsigma;
  double ogain;
  int lskip;
  double srr;
  double srt;
  double str;
  double stt;
  double linearUpdate;
  double angularUpdate;
  double temporalUpdate;
  double resampleThreshold;
  int particles;
  double xmin;
  double ymin;
  double xmax;
  double ymax;
  double delta;
  double occ_thresh;
  double llsamplerange;
  double llsamplestep;
  double lasamplerange;
  double lasamplestep;
}Parameters_gmapping;


inline int32_t fromSec(double d)
{
	int32_t sec = (int32_t)floor(d);
	return (int32_t)((d - (double)sec)*1e9);	//nanosec
}

inline double toSec(int32_t sec, int32_t nsec)
{
	return (double)sec + 1e-9*(double)nsec;
}

inline double toSec(stamp_t stamp)
{
	return toSec(stamp.sec,stamp.nsec);
}