/* Author: Nicola Piccinelli */

#include <iostream>
#include <time.h>
#include <cfloat>


#ifdef _WIN32 
	#include <cstdint>
#endif

#include <gmapping/gridfastslam/gridslamprocessor.h>
#include <gmapping/sensor/sensor_base/sensor.h>
#include <gmapping/sensor/sensor_range/rangesensor.h>
#include <gmapping/sensor/sensor_odometry/odometrysensor.h>
#include <common.h>

class SlamGMapping
{

public:
	SlamGMapping();
	SlamGMapping(unsigned long int seed);
	~SlamGMapping();

	void init(const Parameters_gmapping& param);
	void doSLAM(const LaserScan& scan, const Odometry& odom);
	inline Map getMap(){return map;};

private:
 	LaserScan scan;
 	Odometry odom_pose;
 	Map map;

	GMapping::GridSlamProcessor* gsp_;
	GMapping::RangeSensor* gsp_laser_;
	double gsp_laser_angle_increment_;
	double angle_min_;
	double angle_max_;
	unsigned int gsp_laser_beam_count_;
	GMapping::OdometrySensor* gsp_odom_;

	bool got_first_scan_;
	bool got_map_;

	int32_t map_update_interval_;
	stamp_t last_map_update;
	int laser_count_;
	int throttle_scans_;

	std::string base_frame_;
	std::string laser_frame_;
	std::string map_frame_;
	std::string odom_frame_;

	void laserCallback();
	void updateMap(const LaserScan& scan);
	bool getOdomPose(GMapping::OrientedPoint& gmap_pose);
	bool initMapper();
	bool addScan(GMapping::OrientedPoint& gmap_pose);
	double computePoseEntropy();
	
	// Parameters used by GMapping
	double maxRange_;
	double maxUrange_;
	double maxrange_;
	double minimum_score_;
	double sigma_;
	int kernelSize_;
	double lstep_;
	double astep_;
	int iterations_;
	double lsigma_;
	double ogain_;
	int lskip_;
	double srr_;
	double srt_;
	double str_;
	double stt_;
	double linearUpdate_;
	double angularUpdate_;
	double temporalUpdate_;
	double resampleThreshold_;
	int particles_;
	double xmin_;
	double ymin_;
	double xmax_;
	double ymax_;
	double delta_;
	double occ_thresh_;
	double llsamplerange_;
	double llsamplestep_;
	double lasamplerange_;
	double lasamplestep_;
	
	unsigned long int seed_;
};