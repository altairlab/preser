/* Author: Nicola Piccinelli */

#include "slam_gmapping.h"
// compute linear index for given map coords
#define MAP_IDX(sx, i, j) ((sx) * (j) + (i))

SlamGMapping::SlamGMapping():laser_count_(0)
{
  seed_ = time(NULL);
}

SlamGMapping::SlamGMapping(long unsigned int seed):laser_count_(0), seed_(seed){}

SlamGMapping::~SlamGMapping()
{
  delete gsp_;
  if(gsp_laser_)
    delete gsp_laser_;
  if(gsp_odom_)
    delete gsp_odom_;
}

void SlamGMapping::init(const Parameters_gmapping& param)
{
  this->scan = {0,0,0,0,{0,0},0,0,std::vector<double>(0),std::vector<double>(0)};
  this->odom_pose = {0,0,0};
  this->map = {0.5,0,0,0,0,std::vector<double>(0),{0,0,0}};

  // The library is pretty chatty
  //gsp_ = new GMapping::GridSlamProcessor(std::cerr);
  gsp_ = new GMapping::GridSlamProcessor();

  gsp_laser_ = NULL;
  gsp_laser_angle_increment_ = 0.0;
  gsp_odom_ = NULL;

  got_first_scan_ = false;
  got_map_ = false;
  
  // Parameters used by our GMapping wrapper
  throttle_scans_ = param.throttle_scans;
  base_frame_ = "base_link";
  map_frame_ = "map";
  odom_frame_ = "odom";
  map_update_interval_ = param.map_update_interval;
  
  // Parameters used by GMapping itself
  maxUrange_ = 0.0;
  maxRange_ = 0.0; // preliminary default, will be set in initMapper()
  minimum_score_ = param.minimum_score;
  sigma_ = param.sigma;
  kernelSize_ = param.kernelSize;
  lstep_ = param.lstep;
  astep_ = param.astep;
  iterations_ = param.iterations;
  lsigma_ = param.lsigma;
  ogain_ = param.ogain;
  lskip_ = param.lskip;
  srr_ = param.srr;
  srt_ = param.srt;
  str_ = param.str;
  stt_ = param.stt;
  linearUpdate_ = param.linearUpdate;
  angularUpdate_ = param.angularUpdate;
  temporalUpdate_ = param.temporalUpdate;
  resampleThreshold_ = param.resampleThreshold;
  particles_ = param.particles;
  xmin_ = param.xmin;
  ymin_ = param.ymin;
  xmax_ = param.xmax;
  ymax_ = param.ymax;
  delta_ = param.delta;
  occ_thresh_ = param.occ_thresh;
  llsamplerange_ = param.llsamplerange;
  llsamplestep_ = param.llsamplestep;
  lasamplerange_ = param.lasamplerange;
  lasamplestep_ = param.lasamplestep;
}

void SlamGMapping::doSLAM(const LaserScan& scan, const Odometry& odom)
{
  this->scan = scan;
  this->odom_pose = odom;
  laserCallback();
}

bool SlamGMapping::getOdomPose(GMapping::OrientedPoint& gmap_pose)
{
  // hardcoded offset (X/Y/YAW)
  double laser_x = this->scan.x;
  double laser_y = this->scan.y;
  double laser_theta = odom_pose.yaw;

  double laser_dist = sqrt(laser_x*laser_x + laser_y*laser_y);

  gmap_pose = GMapping::OrientedPoint(odom_pose.x + (laser_dist*cos(laser_theta)), odom_pose.y + (laser_dist*sin(laser_theta)), odom_pose.yaw);
  //gmap_pose = GMapping::OrientedPoint(this->odom_pose.x, this->odom_pose.y, this->odom_pose.yaw);

  return true;
}

bool SlamGMapping::initMapper()
{
  // Check that laserscan is from -x to x in angles:
  if (fabs(fabs(scan.angle_min) - fabs(scan.angle_max)) > FLT_EPSILON)
  {
    return false;
  }

  gsp_laser_beam_count_ = scan.ranges.size();
  int orientationFactor = 1;
  angle_min_ = orientationFactor * scan.angle_min;
  angle_max_ = orientationFactor * scan.angle_max;
  gsp_laser_angle_increment_ = orientationFactor * scan.angle_increment;
  
  maxRange_ = scan.range_max - 0.01;
  maxUrange_ = maxRange_;

  GMapping::OrientedPoint gmap_pose(0.0, 0.0, 0.0);

  // The laser must be called "FLASER".
  // We pass in the absolute value of the computed angle increment, on the
  // assumption that GMapping requires a positive angle increment.  If the
  // actual increment is negative, we'll swap the order of ranges before
  // feeding each scan to GMapping.
  gsp_laser_ = new GMapping::RangeSensor("FLASER",
                                         gsp_laser_beam_count_,
                                         fabs(gsp_laser_angle_increment_),
                                         gmap_pose,
                                         0.0,
                                         maxRange_);

  GMapping::SensorMap smap;
  smap.insert(make_pair(gsp_laser_->getName(), gsp_laser_));
  gsp_->setSensorMap(smap);
  gsp_odom_ = new GMapping::OdometrySensor(odom_frame_);

  /// @todo Expose setting an initial pose
  GMapping::OrientedPoint initialPose;
  if(!getOdomPose(initialPose))//, scan.header.stamp))
  {
    initialPose = GMapping::OrientedPoint(0.0, 0.0, 0.0);
  }

  gsp_->setMatchingParameters(maxUrange_, maxRange_, sigma_,
                              kernelSize_, lstep_, astep_, iterations_,
                              lsigma_, ogain_, lskip_);

  gsp_->setMotionModelParameters(srr_, srt_, str_, stt_);
  gsp_->setUpdateDistances(linearUpdate_, angularUpdate_, resampleThreshold_);
  gsp_->setUpdatePeriod(temporalUpdate_);
  gsp_->setgenerateMap(false);
  gsp_->GridSlamProcessor::init(particles_, xmin_, ymin_, xmax_, ymax_,
                                delta_, initialPose);
  gsp_->setllsamplerange(llsamplerange_);
  gsp_->setllsamplestep(llsamplestep_);
  gsp_->setlasamplerange(lasamplerange_);
  gsp_->setlasamplestep(lasamplestep_);
  gsp_->setminimumScore(minimum_score_);

  // Call the sampling function once to set the seed.
  GMapping::sampleGaussian(1,seed_);
  return true;
}

bool SlamGMapping::addScan(GMapping::OrientedPoint& gmap_pose)
{
  if(!getOdomPose(gmap_pose))
     return false;
  
  if(scan.ranges.size() != gsp_laser_beam_count_)
    return false;

  // GMapping wants an array of doubles...
  double* ranges_double = new double[scan.ranges.size()];

  // If the angle increment is negative, we have to invert the order of the readings.
  if (gsp_laser_angle_increment_ < 0)
  {
    int num_ranges = scan.ranges.size();
    for(int i=0; i < num_ranges; i++)
    {
      // Must filter out short readings, because the mapper won't
      if(scan.ranges[i] < scan.range_min)
        ranges_double[i] = (double)scan.range_max;
      else
        ranges_double[i] = (double)scan.ranges[num_ranges - i - 1];
    }
  } else 
  {
    for(unsigned int i=0; i < scan.ranges.size(); i++)
    {
      // Must filter out short readings, because the mapper won't
      if(scan.ranges[i] < scan.range_min)
        ranges_double[i] = (double)scan.range_max;
      else
        ranges_double[i] = (double)scan.ranges[i];
    }
  }

  GMapping::RangeReading reading(scan.ranges.size(),
                                 ranges_double,
                                 gsp_laser_,
                                 toSec(scan.stamp.sec,scan.stamp.nsec));

  // ...but it deep copies them in RangeReading constructor, so we don't
  // need to keep our array around.
  delete[] ranges_double;

  reading.setPose(gmap_pose);
  return gsp_->processScan(reading);
}

void SlamGMapping::laserCallback()
{
  laser_count_++;
  if ((laser_count_ % throttle_scans_) != 0)
    return;

  // We can't initialize the mapper until we've got the first scan
  if(!got_first_scan_)
  {
    if(!initMapper())
      return;
    got_first_scan_ = true;
  }

  GMapping::OrientedPoint r_pose;
  if(addScan(r_pose))
  {
    GMapping::OrientedPoint mpose = gsp_->getParticles()[gsp_->getBestParticleIndex()].pose;

    std::cout << "new best pose: " << mpose.x << " - " << mpose.y << " - " << mpose.theta << std::endl;
    std::cout << "odom pose: " << r_pose.x << " - " << r_pose.y << " - " << r_pose.theta << std::endl;
    std::cout << "correction: " << mpose.x - r_pose.x << " - " << mpose.y - r_pose.y << " - " << mpose.theta - r_pose.theta << std::endl;

    if(!got_map_ || toSec(scan.stamp - last_map_update) > map_update_interval_)
    {
      updateMap(scan);
      this->map.map_odom.x = mpose.x;
      this->map.map_odom.y = mpose.y;
      this->map.map_odom.yaw = mpose.theta;     

      last_map_update = scan.stamp;
    }
  }
}

double SlamGMapping::computePoseEntropy()
{
  double weight_total=0.0;
  for(std::vector<GMapping::GridSlamProcessor::Particle>::const_iterator it = gsp_->getParticles().begin();
      it != gsp_->getParticles().end();
      ++it)
  {
    weight_total += it->weight;
  }
  double entropy = 0.0;
  for(std::vector<GMapping::GridSlamProcessor::Particle>::const_iterator it = gsp_->getParticles().begin();
      it != gsp_->getParticles().end();
      ++it)
  {
    if(it->weight/weight_total > 0.0)
      entropy += it->weight/weight_total * log(it->weight/weight_total);
  }
  return -entropy;
}

void SlamGMapping::updateMap(const LaserScan& scan)
{
  GMapping::ScanMatcher matcher;
  double* laser_angles = new double[scan.ranges.size()];
  double theta = angle_min_;

  for(unsigned int i=0; i<scan.ranges.size(); i++)
  {
    if (gsp_laser_angle_increment_ < 0)
        laser_angles[scan.ranges.size()-i-1]=theta;
    else
        laser_angles[i]=theta;
    theta += gsp_laser_angle_increment_;
  }

  matcher.setLaserParameters(scan.ranges.size(), laser_angles, gsp_laser_->getPose());

  delete[] laser_angles;
  matcher.setlaserMaxRange(maxRange_);
  matcher.setusableRange(maxUrange_);
  matcher.setgenerateMap(true);

  GMapping::GridSlamProcessor::Particle best = gsp_->getParticles()[gsp_->getBestParticleIndex()];
  GMapping::Point center;
  center.x=(xmin_ + xmax_) / 2.0;
  center.y=(ymin_ + ymax_) / 2.0;

  GMapping::ScanMatcherMap smap(center, xmin_, ymin_, xmax_, ymax_, delta_);
  for(GMapping::GridSlamProcessor::TNode* n = best.node; n; n = n->parent)
  {
    if(!n->reading)
    {
      continue;
    }
    matcher.invalidateActiveArea();
    matcher.computeActiveArea(smap, n->pose, &((*n->reading)[0]));
    matcher.registerScan(smap, n->pose, &((*n->reading)[0]));
  }

  // the map may have expanded, so resize ros message as well
  if(map.width != (unsigned int) smap.getMapSizeX() || map.height != (unsigned int) smap.getMapSizeY())
  {
    // NOTE: The results of ScanMatcherMap::getSize() are different from the parameters given to the constructor
    //       so we must obtain the bounding box in a different way
    GMapping::Point wmin = smap.map2world(GMapping::IntPoint(0, 0));
    GMapping::Point wmax = smap.map2world(GMapping::IntPoint(smap.getMapSizeX(), smap.getMapSizeY()));
    xmin_ = wmin.x; ymin_ = wmin.y;
    xmax_ = wmax.x; ymax_ = wmax.y;

    map.width = smap.getMapSizeX();
    map.height = smap.getMapSizeY();
    map.position_x = xmin_;
    map.position_y = ymin_;
    map.data.resize(map.width * map.height);
  }
  
  for(int x=0; x < smap.getMapSizeX(); x++)
  {
    for(int y=0; y < smap.getMapSizeY(); y++)
    {
      /// @todo Sort out the unknown vs. free vs. obstacle thresholding
      GMapping::IntPoint p(x, y);
      double occ = smap.cell(p);
      assert(occ <= 1.0);
      if(occ < 0)
        map.data[MAP_IDX(map.width, x, y)] = -1;
      else if(occ > occ_thresh_)
      {
        map.data[MAP_IDX(map.width, x, y)] = 100;
      }
      else
        map.data[MAP_IDX(map.width, x, y)] = 0;
    }
  }
}