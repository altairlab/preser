% Importing all algos
addpath(genpath('algorithms'));

% Compile mex
% --> remember to add a space at the end of every filepath
g_mapping_source = ['slam_gmapping.cpp ' 'gmapping/utils/stat.cpp ' 'gmapping/gridfastslam/motionmodel.cpp ' ...
'gmapping/gridfastslam/gridslamprocessor.cpp ' 'gmapping/gridfastslam/gridslamprocessor_tree.cpp ' ...
'gmapping/sensor/sensor_range/rangesensor.cpp ' 'gmapping/sensor/sensor_range/rangereading.cpp ' ...
'gmapping/sensor/sensor_odometry/odometrysensor.cpp ' 'gmapping/sensor/sensor_base/sensorreading.cpp ' ...
'gmapping/sensor/sensor_base/sensor.cpp ' 'gmapping/scanmatcher/scanmatcher.cpp '];
include_dir = '-Igmapping/include -I../';
main_file = 'gmapping_mex.cpp';
output_name = 'GMappingMex';

eval(['mex CXXFLAGS="$CXXFLAGS -Wall -std=c++11" -output ' output_name ' ' include_dir ' ' main_file ' ' g_mapping_source]);