% Parameters used by our GMapping wrapper:
% 
% throttle_scans : [int] throw away every nth laser scan
% map_update_interval : [int32_t] time in nanoseconds between two recalculations of the map
% 
% Parameters used by GMapping itself:
% 
% Laser Parameters:
% sigma : [double] standard deviation for the scan matching process (cell)
% kernelSize : [double] search window for the scan matching process
% lstep : [double] initial search step for scan matching (linear)
% astep : [double:] initial search step for scan matching (angular)
% iterations : [double] number of refinement steps in the scan matching. The final "precision" for the match is lstep*2^(-iterations) or astep*2^(-iterations), respectively.
% lsigma : [double] standard deviation for the scan matching process (single laser beam)
% ogain : [double] gain for smoothing the likelihood
% lskip : [int] take only every (n+1)th laser ray for computing a match (0 = take all rays)
% minimumScore : [double] minimum score for considering the outcome of the scanmatching good. Can avoid 'jumping' pose estimates in large open spaces when using laser scanners with limited range (e.g. 5m). (0 = default. Scores go up to 600+, try 50 for example when experiencing 'jumping' estimate issues)
% 
% Motion Model Parameters (all standard deviations of a gaussian noise model)
% srr : [double] linear noise component (x and y)
% stt : [double] angular noise component (theta)
% srt : [double] linear -> angular noise component
% str : [double] angular -> linear noise component
% 
% Others:
% linearUpdate : [double] the robot only processes new measurements if the robot has moved at least this many meters
% angularUpdate : [double] the robot only processes new measurements if the robot has turned at least this many rads
% resampleThreshold : [double] threshold at which the particles get resampled. Higher means more frequent resampling.
% particles : [int] (fixed) number of particles. Each particle represents a possible trajectory that the robot has traveled
% 
% Likelihood sampling (used in scan matching)
% llsamplerange : [double] linear range
% lasamplerange : [double] linear step size
% llsamplestep : [double] linear range
% lasamplestep : [double] angular step size
% 
% Initial map dimensions and resolution:
% xmin : [double] minimum x position in the map [m]
% ymin : [double] minimum y position in the map [m]
% xmax : [double] maximum x position in the map [m]
% ymax : [double] maximum y position in the map [m]
% delta : [double] size of one pixel [m]
%
% The map data, in row-major order, starting with (0,0).
% Occupancy probabilities are in the range [0,100], unknown is -1.
classdef GMapping < handle
	properties(Access = private)
		map
		parameters
	end

	properties(Access = public)
		in_laser_data
		in_odometry
		out_occupacy_grid
		out_odometry
	end

	methods
		function obj = GMapping()
			obj.in_laser_data = data_laser();
			obj.in_odometry = data_odometry();
			obj.out_occupacy_grid = data_occupacygrid();
			obj.out_odometry = data_odometry();
			
			obj.parameters = struct('throttle_scans',1,'map_update_interval',5000000000, 'minimumScore',0.0, ...
			'sigma',0.05,'kernelSize',1,'lstep',0.05,'astep',0.05,'iterations',5,'lsigma',0.05, 'ogain',3.0,'lskip',0,'srr',0.1,'srt',0.2, ...
			'str',0.1,'stt',0.2,'linearUpdate',1.0,'angularUpdate',0.5, 'temporalUpdate',-1.0, 'resampleThreshold',0.5,'particles',30, ...
			'xmin',-10.0,'ymin',-10.0,'xmax',10.0,'ymax',10.0,'delta',0.05,'occ_thresh',0.25, 'llsamplerange',0.01,'llsamplestep',0.01, ...
			'lasamplerange',0.005,'lasamplestep',0.005);
		end
		
		function obj = configure(obj)
		end
	
		function obj = start(obj)
		end

		function obj = update(obj)
			if obj.in_laser_data.ranges ~= -1
				laser_data = struct('angle_min',obj.in_laser_data.angle_min,'angle_max',obj.in_laser_data.angle_max,'angle_increment', ...
				obj.in_laser_data.angle_increment,'time_increment',obj.in_laser_data.time_increment,'stamp',obj.in_laser_data.stamp, ...
				'range_min',obj.in_laser_data.range_min,'range_max',obj.in_laser_data.range_max,'ranges',obj.in_laser_data.ranges, ...	
				'intensities',obj.in_laser_data.intensities,'x',0.3180,'y',0.00036);

				[~,~,y] = obj.in_odometry.pose.pose.getRPY();
				odometry_data = struct('x',obj.in_odometry.pose.pose.position.x,'y',obj.in_odometry.pose.pose.position.y,'yaw',y);

				[obj.map, odom] = GMappingMex(laser_data, odometry_data, obj.parameters);

				obj.out_occupacy_grid.data = reshape(obj.map.data,[obj.map.width,obj.map.height]);
				obj.out_occupacy_grid.resolution = obj.parameters.delta;
				
				obj.out_odometry.pose.pose.position.x = odom.x - 0.3180;
				obj.out_odometry.pose.pose.position.y = odom.y - 0.00036;
				obj.out_odometry.pose.pose = obj.out_odometry.pose.pose.setOrientation(a2quat(0,0,odom.yaw,'XYZ')); % angle2quat -> Aerospace Toolbox
			end
		end
	end
end