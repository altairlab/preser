#include "slam_gmapping.h"
#include "mex.h"

static const char* fields[] = {"width","height","position_x","position_y","data"};
static const char* fields_odom[] = {"x","y","yaw"};

class GMappingMex
{
private:
	SlamGMapping gn;

public:
	GMappingMex(){};
	~GMappingMex(){};

	inline void mex2GMapping(const LaserScan& scan, const Odometry& odom)
	{
		gn.doSLAM(scan, odom);
	}

	inline void initSystem(const Parameters_gmapping& param)
	{
		gn.init(param);
	}

	inline mxArray* getMap()
	{
		Map m = gn.getMap();
		mwSize dims[2] = {1, 1};

		mxArray* map_data = mxCreateDoubleMatrix(1,m.data.size(),mxREAL);
		mxArray* strct= mxCreateStructArray(1,dims,5,fields);
		
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"width"),mxCreateDoubleScalar(m.width));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"height"),mxCreateDoubleScalar(m.height));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"position_x"),mxCreateDoubleScalar(m.position_x));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"position_y"),mxCreateDoubleScalar(m.position_y));

		double* dataptr = mxGetPr(map_data);
		for(int i=0; i<m.data.size(); ++i)
		{
			dataptr[i] = m.data[i];
		}
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"data"),map_data);
		return strct;
	}

	inline mxArray* getMapOdom()
	{
		Map m = gn.getMap();
		mwSize dims[2] = {1, 1};
		mxArray* strct= mxCreateStructArray(1,dims,3,fields_odom);
		
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"x"),mxCreateDoubleScalar(m.map_odom.x));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"y"),mxCreateDoubleScalar(m.map_odom.y));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"yaw"),mxCreateDoubleScalar(m.map_odom.yaw));
		return strct;
	}
};

static GMappingMex mex_class;
static bool isFirst = true;
/*
	1st arg: laser scan structure
	2nd arg: odometry structure
	return: occupacy grid structure
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	Odometry odom;
	LaserScan scan;
	Parameters_gmapping param;

	const mxArray* in_data;

	// laser
	in_data = prhs[0];

	double* stamp = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"stamp")));
	double* ranges = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ranges")));
	double* intensities = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"intensities")));				

	scan = LaserScan
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_min"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_max"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_increment"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"time_increment"))),
		{(int32_t)stamp[0],(int32_t)stamp[1]},
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"range_min"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"range_max"))),
		std::vector<double>(ranges,
			ranges + mxGetN(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ranges")))),
		std::vector<double>(intensities,
			intensities + mxGetN(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"intensities")))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"x"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"y"))),
	};

	// odometry
	in_data = prhs[1];

	odom = Odometry
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"x"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"y"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"yaw")))
	};

	// parameters
	in_data = prhs[2];

	param = Parameters_gmapping
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"throttle_scans"))),
		(int32_t)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"map_update_interval"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"minimumScore"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"sigma"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"kernelSize"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"lstep"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"astep"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"iterations"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"lsigma"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ogain"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"lskip"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"srr"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"srt"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"str"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"stt"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"linearUpdate"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angularUpdate"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"temporalUpdate"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"resampleThreshold"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"particles"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"xmin"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ymin"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"xmax"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ymax"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"delta"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"occ_thresh"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"llsamplerange"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"llsamplestep"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"lasamplerange"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"lasamplestep")))
	};

	if(isFirst)
	{
		std::cout << "Initializing gmapping ..." << std::endl;
		mex_class.initSystem(param);
		std::cout << "Initialization done" << std::endl;		
		isFirst = false;
	}

	mex_class.mex2GMapping(scan,odom);
	plhs[0] = mex_class.getMap();
	plhs[1] = mex_class.getMapOdom();
}