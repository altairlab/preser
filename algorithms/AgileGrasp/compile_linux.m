% % Importing all algos
addpath(genpath('algorithms'));

% Compile mex
% --> remember to add a space at the end of every filepath



IPath_pcl  =['-I/usr/include/pcl-1.7'];
LPath_1_pcl = ['-L/usr/lib'];  

IPath_eigen  =['-I/usr/include/eigen3'];
vtk = ['-L/usr/lib/vtk-5.8/tcl/vtk'];


AgileGrasp_source = ['src/localization.cpp ' 'src/learning.cpp ' 'src/antipodal.cpp ' ...
'src/finger_hand.cpp ' 'src/grasp_hypothesis.cpp ' ...
'src/grasp_localizer.cpp ' 'src/hand_search.cpp ' ...
'src/handle.cpp ' 'src/handle_search.cpp ' ...
'src/plot.cpp ' 'src/quadric.cpp ' 'src/rotating_hand.cpp '];

include_dir = '-Iinclude ';
main_file = 'grasp_pcd_mex.cpp';
output_name = 'GraspPcd_Mex';



eval(['mex -v CXXFLAGS="$CXXFLAGS -Wall -std=c++11" -output ' output_name ' ' vtk ' ' IPath_eigen ' ' include_dir ' ' IPath_pcl ' ' LPath_1_pcl ' ' main_file ]);