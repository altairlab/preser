// #include "agile_grasp.h"
// #define _CRT_SECURE_NO_DEPRECATE
// #define NDEBUG
#include "mex.h"
#include "lapack.h"
// 
// #ifdef _WIN32
// #include <cstdint>
// #endif

#include <agile_grasp/learning.h>
#include <agile_grasp/localization.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    char* file_name;
    file_name = mxArrayToString(prhs[0]); // prendo in input il file .pcd
    
    std::string pcd_file_name;
    std::string file_name_left;
    std::string file_name_right;
    
    pcd_file_name = std::string(file_name);
    
    if (pcd_file_name.find(".pcd") == std::string::npos)
    {
        file_name_left = pcd_file_name + "l_reg.pcd";
        file_name_right = pcd_file_name + "r_reg.pcd";
    }
    else
    {
        file_name_left = pcd_file_name;
        file_name_right = "";
    }
    
    mexPrintf("pcd_file_name %s \n" , pcd_file_name);
    // read SVM filename from command line
    std::string svm_file_name = mxArrayToString(prhs[1]);
    
    mexPrintf("svm_file_name %s \n" , svm_file_name);
    
    /* read number of samples, number of threads and min handle inliers
     * from command line */
    double *num_samples; // 400 default
    num_samples = mxGetPr(prhs[2]);
    
    double *num_threads; // 1 default
    num_threads = mxGetPr(prhs[3]);
    
    double *min_inliers; // 3 default
    min_inliers = mxGetPr(prhs[4]);
    
    mexPrintf("num_samples %f -  num_threads %f - min_inliers %f \n" , *num_samples, *num_threads, *min_inliers);
    
    double taubin_radius = 0.03;
    double hand_radius = 0.08;
    
    
    double *A, *B;    /* pointers to input matrices */
    double *T, *T2;  /* in/out arguments to DGESV*/
    size_t m,n,p;     /* matrix dimensions */
    mxArray *Awork, *Bwork;
    A = mxGetPr(prhs[5]); /* pointer to first input matrix */
    B = mxGetPr(prhs[6]); /* pointer to second input matrix */
    /* dimensions of input matrices */
    m = mxGetM(prhs[5]);
    p = mxGetN(prhs[5]);
    n = mxGetN(prhs[6]);
    
    
    Awork = mxCreateDoubleMatrix(m, p, mxREAL);
    T = mxGetPr(Awork);
    Bwork = mxCreateDoubleMatrix(p, n, mxREAL);
    T2 = mxGetPr(Bwork);
    memcpy(T, A, m*p*mxGetElementSize(prhs[5]));
    memcpy(T2,B,m*p*mxGetElementSize(prhs[6]));
    
    
    mexPrintf(" %f %f %f %f \n %f %f %f %f \n %f %f %f %f \n %f %f %f %f  \n\n",T[0],T[4],T[8],T[12],T[1],T[5],T[9],T[13],T[2],T[6],T[10],T[14],T[3],T[7],T[11],T[15]);
    mexPrintf(" %f %f %f %f \n %f %f %f %f \n %f %f %f %f \n %f %f %f %f  \n\n",T2[0],T2[4],T2[8],T2[12],T2[1],T2[5],T2[9],T2[13],T2[2],T2[6],T2[10],T2[14],T2[3],T2[7],T2[11],T2[15]);
    
//
    // camera poses for 2-camera Baxter setup
    Eigen::Matrix4d base_tf, sqrt_tf;
    
//     base_tf << 0, 0.445417, 0.895323, 0.215,
//             1, 0, 0, -0.015,
//             0, 0.895323, -0.445417, 0.23,
//             0, 0, 0, 1;
//
//     sqrt_tf <<   0.9366,  -0.0162,  0.3500, -0.2863,
//             0.0151,   0.9999,   0.0058,   0.0058,
//             -0.3501, -0.0002, 0.9367, 0.0554,
//             0,        0,      0,      1;
//
    base_tf << T[0],T[4],T[8],T[12],T[1],T[5],T[9],T[13],T[2],T[6],T[10],T[14],T[3],T[7],T[11],T[15];
    sqrt_tf << T2[0],T2[4],T2[8],T2[12],T2[1],T2[5],T2[9],T2[13],T2[2],T2[6],T2[10],T2[14],T2[3],T2[7],T2[11],T2[15];
    
    // workspace dimensions
    Eigen::VectorXd workspace(6);
    workspace << -10, 10, -10, 10, -10, 10;
    //workspace << 0.4, 0.7, -0.02, 0.06, -0.2, 10;
    //workspace << 0.4, 1.0, -0.3, 0.3, -0.2, 2;
    //workspace << 0.55, 0.9, -0.35, 0.2, -0.2, 2;
    //workspace << 0.6, 0.8, -0.25, 0.1, -0.3, 2;
    // workspace << 0.55, 0.95, -0.25, 0.07, -0.3, 1;
    // workspace << -10, 10, -10, 10, 0.55, 0.95;
    
//     mexPrintf(" Converting double to int : num_threads  %d \n " , (int)*num_threads);
    // set-up parameters for the hand search
    Localization loc( (int)*num_threads, true, true);
    loc.setCameraTransforms(base_tf * sqrt_tf.inverse(), base_tf * sqrt_tf);
    loc.setWorkspace(workspace);
    loc.setNumSamples((int)*num_samples);
    loc.setNeighborhoodRadiusTaubin(taubin_radius);
    loc.setNeighborhoodRadiusHands(hand_radius);
    loc.setFingerWidth(0.01);
    loc.setHandOuterDiameter(0.09);
    loc.setHandDepth(0.06);
    loc.setInitBite(0.01);
    loc.setHandHeight(0.02);
    mexPrintf("Localizing hands ...\n");

    // test with fixed set of indices
//    std::vector<int> indices(5);
//    indices[0] = 731;
//    indices[1] = 4507;
//    indices[2] = 4445;
//    indices[3] = 2254;
//    indices[4] = 3716;
//    std::vector<RotatingHand> hand_list = loc.localizeHands(file_name_left, file_name_right, indices, svm_file_name);
    
   loc.leggo();
   
//    // test with randomly sampled indices
    std::vector<GraspHypothesis> hands = loc.localizeHands(file_name_left, file_name_right); 
  //  std::vector<GraspHypothesis> antipodal_hands = loc.predictAntipodalHands(hands, svm_file_name);
  //  std::vector<Handle> handles = loc.findHandles(antipodal_hands, (int)*min_inliers, 0.005); 

    
//     return 0;

// std::cout << "No PCD filename given!\n";
// std::cout << "Usage: test_svm pcd_filename svm_filename [num_samples] [num_threads] [min_handle_inliers]\n";
// std::cout << "Localize grasps in a *.pcd file using a trained SVM.\n";

       
    
}