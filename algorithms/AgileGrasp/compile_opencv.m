% OPENCV 

OPENCV_ROOT = getenv('OPENCV_ROOT_3.0');
IPath_opencv  =['-I',OPENCV_ROOT,'/build/include']; 
LPath1 = ['-L',fullfile(OPENCV_ROOT, '/build/x64/vc12/lib')];
LPath2 = ['-L',fullfile(OPENCV_ROOT, '/build/x64/vc12/staticlib')];


lPath_opencv_0 =  ['-l','opencv_world300.lib'];
lPath_opencv_1 =  ['-l','opencv_ts300.lib'];
lPath_opencv_2 =  ['-l','opencv_calib3d300.lib'];
lPath_opencv_3  =  ['-l','opencv_core300.lib'];
lPath_opencv_4  =  ['-l','opencv_features2d300.lib'];
lPath_opencv_5  =  ['-l','opencv_flann300.lib'];
lPath_opencv_6  =  ['-l','opencv_highgui300.lib'];
lPath_opencv_7  =  ['-l','opencv_imgcodecs300.lib'];
lPath_opencv_8  =  ['-l','opencv_imgproc300.lib'];
lPath_opencv_9  =  ['-l','opencv_ml300.lib'];
lPath_opencv_10  =  ['-l','opencv_objdetect300.lib'];
lPath_opencv_11  =  ['-l','opencv_photo300.lib'];
lPath_opencv_12  =  ['-l','opencv_shape300.lib'];
lPath_opencv_13  =  ['-l','opencv_stitching300.lib'];
lPath_opencv_14  =  ['-l','opencv_superres300.lib'];
lPath_opencv_15  =  ['-l','opencv_video300.lib'];
lPath_opencv_16  =  ['-l','opencv_videoio300.lib'];
lPath_opencv_17  =  ['-l','opencv_videostab300.lib'];


% 
% lPath_opencv_0 =  fullfile(LPath1,'opencv_world300.lib');
% lPath_opencv_1 =  fullfile(LPath1,'opencv_ts300.lib');
% lPath_opencv_2 =  fullfile(LPath2,'opencv_calib3d300.lib');
% lPath_opencv_3  =  fullfile(LPath2,'opencv_core300.lib');
% lPath_opencv_4  =  fullfile(LPath2,'opencv_features2d300.lib');
% lPath_opencv_5  =  fullfile(LPath2,'opencv_flann300.lib');
% lPath_opencv_6  =  fullfile(LPath2,'opencv_highgui300.lib');
% lPath_opencv_7  =  fullfile(LPath2,'opencv_imgcodecs300.lib');
% lPath_opencv_8  =  fullfile(LPath2,'opencv_imgproc300.lib');
% lPath_opencv_9  =  fullfile(LPath2,'opencv_ml300.lib');
% lPath_opencv_10  =  fullfile(LPath2,'opencv_objdetect300.lib');
% lPath_opencv_11  =  fullfile(LPath2,'opencv_photo300.lib');
% lPath_opencv_12  =  fullfile(LPath2,'opencv_shape300.lib');
% lPath_opencv_13  =  fullfile(LPath2,'opencv_stitching300.lib');
% lPath_opencv_14  =  fullfile(LPath2,'opencv_superres300.lib');
% lPath_opencv_15  =  fullfile(LPath2,'opencv_video300.lib');
% lPath_opencv_16  =  fullfile(LPath2,'opencv_videoio300.lib');
% lPath_opencv_17  =  fullfile(LPath2,'opencv_videostab300.lib');

mex('-largeArrayDims','opencv_test.cpp', 'CXXFLAGS="$CXXFLAGS -Wall -std=c++11"','-v','-g' ,...
                                IPath_opencv,LPath1,LPath2 , ...
                                lPath_opencv_0,lPath_opencv_1,lPath_opencv_2,lPath_opencv_3,lPath_opencv_4, lPath_opencv_5,...
                                lPath_opencv_6,lPath_opencv_7,lPath_opencv_8,lPath_opencv_9,lPath_opencv_10, lPath_opencv_11,...
                                lPath_opencv_12,lPath_opencv_13,lPath_opencv_14,lPath_opencv_15,lPath_opencv_16, lPath_opencv_17 );

