% Compile Agile Grasp

% ------------------------------------------------
%                   OPENCV
% ------------------------------------------------

OPENCV_ROOT = getenv('OPENCV_ROOT_3.0');
IPath_opencv  =['-I',OPENCV_ROOT,'/build/include'];
LPath1 = ['-L',fullfile(OPENCV_ROOT, '/build/x64/vc12/lib')];
LPath2 = ['-L',fullfile(OPENCV_ROOT, '/build/x64/vc12/staticlib')];

lPath_opencv_0 =  ['-l','opencv_world300.lib'];
lPath_opencv_1 =  ['-l','opencv_ts300.lib'];
lPath_opencv_2 =  ['-l','opencv_calib3d300.lib'];
lPath_opencv_3  =  ['-l','opencv_core300.lib'];
lPath_opencv_4  =  ['-l','opencv_features2d300.lib'];
lPath_opencv_5  =  ['-l','opencv_flann300.lib'];
lPath_opencv_6  =  ['-l','opencv_highgui300.lib'];
lPath_opencv_7  =  ['-l','opencv_imgcodecs300.lib'];
lPath_opencv_8  =  ['-l','opencv_imgproc300.lib'];
lPath_opencv_9  =  ['-l','opencv_ml300.lib'];
lPath_opencv_10  =  ['-l','opencv_objdetect300.lib'];
lPath_opencv_11  =  ['-l','opencv_photo300.lib'];
lPath_opencv_12  =  ['-l','opencv_shape300.lib'];
lPath_opencv_13  =  ['-l','opencv_stitching300.lib'];
lPath_opencv_14  =  ['-l','opencv_superres300.lib'];
lPath_opencv_15  =  ['-l','opencv_video300.lib'];
lPath_opencv_16  =  ['-l','opencv_videoio300.lib'];
lPath_opencv_17  =  ['-l','opencv_videostab300.lib'];
lPath_opencv_18  =  ['-l','IlmImf.lib'];
lPath_opencv_19  =  ['-l','ippicvmt.lib'];
lPath_opencv_20  =  ['-l','libjasper.lib'];
lPath_opencv_21  =  ['-l','libjpeg.lib'];
lPath_opencv_22  =  ['-l','libpng.lib'];
lPath_opencv_23  =  ['-l','libtiff.lib'];
lPath_opencv_24  =  ['-l','zlib.lib'];
lPath_opencv_25 =   ['-l','libwebp.lib'];
lPath_opencv_26 =   ['-l','opencv_hal300.lib'];
% ------------------------------------------------
%                   PCL
% ------------------------------------------------

PCL_ROOT = getenv('PCL_ROOT');

IPath_pcl  =['-I',PCL_ROOT,'/include/pcl-1.7'];
IPath_boost=['-I',PCL_ROOT,'/3rdParty/Boost/include/boost-1_57'];
IPath_Eigen=['-I',PCL_ROOT,'/3rdParty/Eigen/eigen3'];
IPath_FLANN=['-I',PCL_ROOT,'/3rdParty/FLANN/include'];
IPath_vtw  =['-I',PCL_ROOT,'/3rdParty/VTK/include/vtk-6.2'];
IPath_Qhull=['-I',PCL_ROOT,'/3rdParty/Qhull/include'];

%LIB
LPath_PCL = ['-L',fullfile(PCL_ROOT, 'lib')];
LPath_Boost = ['-L',PCL_ROOT,'/3rdParty/Boost/lib'];
LPath_FLANN = ['-L',PCL_ROOT,'/3rdParty/FLANN/lib'];
LPath_QHull = ['-L',PCL_ROOT,'/3rdParty/Qhull/lib'];
LPath_VTK = ['-L',PCL_ROOT,'/3rdParty/VTK/lib'];

lPath_pcl_1 = ['-l','pcl_features_release.lib'];
lPath_pcl_2 = ['-l','pcl_io_release.lib'];
lPath_pcl_3 = ['-l','pcl_common_release.lib'];
lPath_pcl_4 = ['-l','pcl_kdtree_release.lib'];
lPath_pcl_5 = ['-l','pcl_search_release.lib'];
lPath_pcl_6 = ['-l','pcl_visualization_release.lib'];
lPath_pcl_7 = ['-l','pcl_segmentation_release.lib'];
lPath_pcl_8 = ['-l','pcl_filters_release.lib'];
lPath_pcl_9 = ['-l','pcl_io_ply_release.lib'];
lPath_pcl_10 = ['-l','pcl_keypoints_release.lib'];
lPath_pcl_11 = ['-l','pcl_octree_release.lib'];
lPath_pcl_12 = ['-l','pcl_outofcore_release.lib'];
lPath_pcl_13 = ['-l','pcl_people_release.lib'];
lPath_pcl_14 = ['-l','pcl_recognition_release.lib'];
lPath_pcl_15 = ['-l','pcl_registration_release.lib'];
lPath_pcl_16 = ['-l','pcl_sample_consensus_release.lib'];
lPath_pcl_17 = ['-l','pcl_surface_release.lib'];
lPath_pcl_18 = ['-l','pcl_tracking_release.lib'];

lPath_FLANN_1 = ['-l','flann.lib'];
lPath_FLANN_2 = ['-l','flann_cpp_s.lib'];
lPath_FLANN_3 = ['-l','flann_s.lib'];

lPath_vtw_1 = ['-l','vtkChartsCore-6.2.lib'];
lPath_vtw_2 = ['-l','vtkCommonColor-6.2.lib'];
lPath_vtw_3 = ['-l','vtkCommonComputationalGeometry-6.2.lib'];
lPath_vtw_4 = ['-l','vtkCommonCore-6.2.lib'];
lPath_vtw_5 = ['-l','vtkCommonDataModel-6.2.lib'];
lPath_vtw_6 = ['-l','vtkCommonExecutionModel-6.2.lib'];
lPath_vtw_7 = ['-l','vtkCommonMath-6.2.lib'];
lPath_vtw_8 = ['-l','vtkCommonMisc-6.2.lib'];
lPath_vtw_9 = ['-l','vtkCommonSystem-6.2.lib'];
lPath_vtw_10 = ['-l','vtkCommonTransforms-6.2.lib'];
lPath_vtw_11 = ['-l','vtkDICOMParser-6.2.lib'];
lPath_vtw_12 = ['-l','vtkDomainsChemistry-6.2.lib'];
lPath_vtw_13 = ['-l','vtkFiltersAMR-6.2.lib'];
lPath_vtw_14 = ['-l','vtkFiltersCore-6.2.lib'];
lPath_vtw_15 = ['-l','vtkFiltersExtraction-6.2.lib'];
lPath_vtw_16 = ['-l','vtkFiltersFlowPaths-6.2.lib'];
lPath_vtw_17 = ['-l','vtkFiltersGeneral-6.2.lib'];
lPath_vtw_18 = ['-l','vtkFiltersGeneric-6.2.lib'];
lPath_vtw_19 = ['-l','vtkFiltersGeometry-6.2.lib'];
lPath_vtw_20 = ['-l','vtkFiltersHybrid-6.2.lib'];
lPath_vtw_21 = ['-l','vtkFiltersHyperTree-6.2.lib'];
lPath_vtw_22 = ['-l','vtkFiltersImaging-6.2.lib'];
lPath_vtw_23 = ['-l','vtkFiltersModeling-6.2.lib'];
lPath_vtw_24 = ['-l','vtkFiltersParallel-6.2.lib'];
lPath_vtw_25 = ['-l','vtkFiltersParallelImaging-6.2.lib'];
lPath_vtw_26 = ['-l','vtkFiltersProgrammable-6.2.lib'];
lPath_vtw_27 = ['-l','vtkFiltersSMP-6.2.lib'];
lPath_vtw_28 = ['-l','vtkFiltersSelection-6.2.lib'];
lPath_vtw_29 = ['-l','vtkFiltersSources-6.2.lib'];
lPath_vtw_30 = ['-l','vtkFiltersStatistics-6.2.lib'];
lPath_vtw_31 = ['-l','vtkFiltersTexture-6.2.lib'];
lPath_vtw_32 = ['-l','vtkFiltersVerdict-6.2.lib'];
lPath_vtw_33 = ['-l','vtkGeovisCore-6.2.lib'];
lPath_vtw_34 = ['-l','vtkIOAMR-6.2.lib'];
lPath_vtw_35 = ['-l','vtkIOCore-6.2.lib'];
lPath_vtw_36 = ['-l','vtkIOEnSight-6.2.lib'];
lPath_vtw_37 = ['-l','vtkIOExodus-6.2.lib'];
lPath_vtw_38 = ['-l','vtkIOExport-6.2.lib'];
lPath_vtw_39 = ['-l','vtkIOGeometry-6.2.lib'];
lPath_vtw_40 = ['-l','vtkIOImage-6.2.lib'];
lPath_vtw_41 = ['-l','vtkIOImport-6.2.lib'];
lPath_vtw_42 = ['-l','vtkIOInfovis-6.2.lib'];
lPath_vtw_43 = ['-l','vtkIOLSDyna-6.2.lib'];
lPath_vtw_44 = ['-l','vtkIOLegacy-6.2.lib'];
lPath_vtw_45 = ['-l','vtkIOMINC-6.2.lib'];
lPath_vtw_46 = ['-l','vtkIOMovie-6.2.lib'];
lPath_vtw_47 = ['-l','vtkIONetCDF-6.2.lib'];
lPath_vtw_48 = ['-l','vtkIOPLY-6.2.lib'];
lPath_vtw_49 = ['-l','vtkIOParallel-6.2.lib'];
lPath_vtw_50 = ['-l','vtkIOParallelXML-6.2.lib'];
lPath_vtw_51 = ['-l','vtkIOSQL-6.2.lib'];
lPath_vtw_52 = ['-l','vtkIOVideo-6.2.lib'];
lPath_vtw_53 = ['-l','vtkIOXML-6.2.lib'];
lPath_vtw_54 = ['-l','vtkIOXMLParser-6.2.lib'];
lPath_vtw_55 = ['-l','vtkImagingColor-6.2.lib'];
lPath_vtw_56 = ['-l','vtkImagingCore-6.2.lib'];
lPath_vtw_57 = ['-l','vtkImagingFourier-6.2.lib'];
lPath_vtw_58 = ['-l','vtkImagingGeneral-6.2.lib'];
lPath_vtw_59 = ['-l','vtkImagingHybrid-6.2.lib'];
lPath_vtw_60 = ['-l','vtkImagingMath-6.2.lib'];
lPath_vtw_61 = ['-l','vtkImagingMorphological-6.2.lib'];
lPath_vtw_62 = ['-l','vtkImagingSources-6.2.lib'];
lPath_vtw_63 = ['-l','vtkImagingStatistics-6.2.lib'];
lPath_vtw_64 = ['-l','vtkImagingStencil-6.2.lib'];
lPath_vtw_65 = ['-l','vtkInfovisCore-6.2.lib'];
lPath_vtw_66 = ['-l','vtkInfovisLayout-6.2.lib'];
lPath_vtw_67 = ['-l','vtkInteractionImage-6.2.lib'];
lPath_vtw_68 = ['-l','vtkInteractionStyle-6.2.lib'];
lPath_vtw_69 = ['-l','vtkInteractionWidgets-6.2.lib'];
lPath_vtw_70 = ['-l','vtkNetCDF-6.2.lib'];
lPath_vtw_71 = ['-l','vtkNetCDF_cxx-6.2.lib'];
lPath_vtw_72 = ['-l','vtkParallelCore-6.2.lib'];
lPath_vtw_73 = ['-l','vtktiff-6.2.lib'];
lPath_vtw_74 = ['-l','vtkverdict-6.2.lib'];
lPath_vtw_75 = ['-l','vtkzlib-6.2.lib'];
lPath_vtw_76 = ['-l','vtkRenderingAnnotation-6.2.lib'];
lPath_vtw_77 = ['-l','vtkRenderingContext2D-6.2.lib'];
lPath_vtw_78 = ['-l','vtkRenderingContextOpenGL-6.2.lib'];
lPath_vtw_79 = ['-l','vtkRenderingCore-6.2.lib'];
lPath_vtw_80 = ['-l','vtkRenderingFreeType-6.2.lib'];
lPath_vtw_81 = ['-l','vtkRenderingFreeTypeOpenGL-6.2.lib'];
lPath_vtw_82 = ['-l','vtkRenderingGL2PS-6.2.lib'];
lPath_vtw_83 = ['-l','vtkRenderingImage-6.2.lib'];
lPath_vtw_84 = ['-l','vtkRenderingLIC-6.2.lib'];
lPath_vtw_85 = ['-l','vtkRenderingLOD-6.2.lib'];
lPath_vtw_86 = ['-l','vtkRenderingLabel-6.2.lib'];
lPath_vtw_87 = ['-l','vtkRenderingOpenGL-6.2.lib'];
lPath_vtw_88 = ['-l','vtkRenderingVolume-6.2.lib'];
lPath_vtw_89 = ['-l','vtkRenderingVolumeOpenGL-6.2.lib'];
lPath_vtw_90 = ['-l','vtkViewsContext2D-6.2.lib'];
lPath_vtw_91 = ['-l','vtkViewsCore-6.2.lib'];
lPath_vtw_92 = ['-l','vtkViewsInfovis-6.2.lib'];
lPath_vtw_93 = ['-l','vtkalglib-6.2.lib'];
lPath_vtw_94 = ['-l','vtkexoIIc-6.2.lib'];
lPath_vtw_95 = ['-l','vtkexpat-6.2.lib'];
lPath_vtw_96 = ['-l','vtkfreetype-6.2.lib'];
lPath_vtw_97 = ['-l','vtkftgl-6.2.lib'];
lPath_vtw_98 = ['-l','vtkgl2ps-6.2.lib'];
lPath_vtw_99 = ['-l','vtkhdf5-6.2.lib'];
lPath_vtw_100 = ['-l','vtkhdf5_hl-6.2.lib'];
lPath_vtw_101 = ['-l','vtkjpeg-6.2.lib'];
lPath_vtw_102 = ['-l','vtkjsoncpp-6.2.lib'];
lPath_vtw_103 = ['-l','vtklibxml2-6.2.lib'];
lPath_vtw_104 = ['-l','vtkmetaio-6.2.lib'];
lPath_vtw_105 = ['-l','vtkoggtheora-6.2.lib'];
lPath_vtw_106 = ['-l','vtkpng-6.2.lib'];
lPath_vtw_107 = ['-l','vtkproj4-6.2.lib'];
lPath_vtw_108 = ['-l','vtksqlite-6.2.lib'];
lPath_vtw_109 = ['-l','vtksys-6.2.lib'];


lpath_boost_1 = ['-l','libboost_atomic-vc120-mt-1_57.lib'];
lpath_boost_3 = ['-l','libboost_chrono-vc120-mt-1_57.lib'];
lpath_boost_5 = ['-l','libboost_container-vc120-mt-1_57.lib'];
lpath_boost_7 = ['-l','libboost_context-vc120-mt-1_57.lib'];
lpath_boost_9 = ['-l','libboost_coroutine-vc120-mt-1_57.lib'];
lpath_boost_11 = ['-l','libboost_date_time-vc120-mt-1_57.lib'];
lpath_boost_13 = ['-l','libboost_exception-vc120-mt-1_57.lib'];
lpath_boost_15 = ['-l','libboost_filesystem-vc120-mt-1_57.lib'];
lpath_boost_17 = ['-l','libboost_graph-vc120-mt-1_57.lib'];
lpath_boost_19 = ['-l','libboost_iostreams-vc120-mt-1_57.lib'];
lpath_boost_21 = ['-l','libboost_locale-vc120-mt-1_57.lib'];
lpath_boost_23 = ['-l','libboost_log-vc120-mt-1_57.lib'];
lpath_boost_25 = ['-l','libboost_log_setup-vc120-mt-1_57.lib'];
lpath_boost_27 = ['-l','libboost_math_c99-vc120-mt-1_57.lib'];
lpath_boost_29 = ['-l','libboost_math_c99f-vc120-mt-1_57.lib'];
lpath_boost_31 = ['-l','libboost_math_c99l-vc120-mt-1_57.lib'];
lpath_boost_33 = ['-l','libboost_math_tr1-vc120-mt-1_57.lib'];
lpath_boost_35 = ['-l','libboost_math_tr1f-vc120-mt-1_57.lib'];
lpath_boost_37 = ['-l','libboost_math_tr1l-vc120-mt-1_57.lib'];
lpath_boost_39 = ['-l','libboost_mpi-vc120-mt-1_57.lib'];
lpath_boost_41 = ['-l','libboost_prg_exec_monitor-vc120-mt-1_57.lib'];
lpath_boost_43 = ['-l','libboost_program_options-vc120-mt-1_57.lib'];
lpath_boost_45 = ['-l','libboost_random-vc120-mt-1_57.lib'];
lpath_boost_47 = ['-l','libboost_regex-vc120-mt-1_57.lib'];
lpath_boost_49 = ['-l','libboost_serialization-vc120-mt-1_57.lib'];
lpath_boost_51 = ['-l','libboost_signals-vc120-mt-1_57.lib'];
lpath_boost_53 = ['-l','libboost_system-vc120-mt-1_57.lib'];
lpath_boost_55 = ['-l','libboost_test_exec_monitor-vc120-mt-1_57.lib'];
lpath_boost_57 = ['-l','libboost_thread-vc120-mt-1_57.lib'];
lpath_boost_59 = ['-l','libboost_timer-vc120-mt-1_57.lib'];
lpath_boost_61 = ['-l','libboost_unit_test_framework-vc120-mt-1_57.lib'];
lpath_boost_63 = ['-l','libboost_wave-vc120-mt-1_57.lib'];
lpath_boost_65 = ['-l','libboost_wserialization-vc120-mt-1_57.lib'];


% ------------------------------------------------
%                   AgileGrasp
% ------------------------------------------------


AgileGrasp_learning = fullfile(pwd,'src/learning.cpp');
AgileGrasp_localization = fullfile(pwd,'src/localization.cpp');
AgileGrasp_antipodal = fullfile(pwd,'src/antipodal.cpp');
AgileGrasp_fingerHand= fullfile(pwd,'src/finger_hand.cpp');
AgileGrasp_graspHypothesis = fullfile(pwd,'src/grasp_hypothesis.cpp');
AgileGrasp_graspLocalizer = fullfile(pwd,'src/grasp_localizer.cpp');
AgileGrasp_handSearch = fullfile(pwd,'src/hand_search.cpp');
AgileGrasp_handleSearcg = fullfile(pwd,'src/handle_search.cpp');
AgileGrasp_handle = fullfile(pwd,'src/handle.cpp');
AgileGrasp_plot = fullfile(pwd,'src/plot.cpp');
AgileGrasp_quadric = fullfile(pwd,'src/quadric.cpp');
AgileGrasp_rotatingHand = fullfile(pwd,'src/rotating_hand.cpp');


L_lapack_path = ['-L',fullfile(matlabroot,'extern','lib',computer('arch'),'microsoft')];
lapacklib = ['-l','libmwlapack.lib'];
blaslib = ['-l','libmwblas.lib'];
% 
% L_lapack_path = ['-L' ,'C:\lapack-3.6.0\build\lib\Release'];
% lapacklib = ['-l','lapack.lib'];
% blaslib = ['-l','blas.lib'];

% L_lapack_path = ['-L' pwd];
% lapacklib = ['-l','liblapack.lib'];
% blaslib = ['-l','libblas.lib'];

include_dir = ['-I' fullfile(pwd,'include')];

main_file = 'grasp_pcd_mex.cpp';            % OK
% main_file = 'test/opencv_test.cpp';         % OK
% main_file = 'test/antipodal_test.cpp';      % OK
% main_file = 'test/hands_test.cpp';          % OK   
% main_file = 'test/taubin_test.cpp';          % OK 


%
% mex('-largeArrayDims',main_file, 'CXXFLAGS="$CXXFLAGS -Wall -std=c++11"','-v','-g' ,...
%                                 IPath_opencv,LPath1,LPath2 , ...
%                                 lPath_opencv_0,lPath_opencv_1,lPath_opencv_2,lPath_opencv_3,lPath_opencv_4, lPath_opencv_5,...
%                                 lPath_opencv_6,lPath_opencv_7,lPath_opencv_8,lPath_opencv_9,lPath_opencv_10, lPath_opencv_11,...
%                                 lPath_opencv_12,lPath_opencv_13,lPath_opencv_14,lPath_opencv_15,lPath_opencv_16, lPath_opencv_17 );
%
%
mex('-largeArrayDims',include_dir, main_file, 'CXXFLAGS="$CXXFLAGS -Wall -std=c++11"',... % '-v','-g',
    L_lapack_path,lapacklib,blaslib,...
    IPath_pcl,IPath_boost,IPath_Eigen,IPath_FLANN,IPath_vtw,IPath_Qhull, ...                    % PCL + 3rd
    LPath_PCL,LPath_Boost,LPath_FLANN,LPath_QHull,LPath_VTK,...
    lPath_pcl_1,lPath_pcl_2,lPath_pcl_3,lPath_pcl_4,lPath_pcl_5,lPath_pcl_6, ...
    lPath_pcl_7,lPath_pcl_8,lPath_pcl_9,lPath_pcl_10,lPath_pcl_11,lPath_pcl_12,...
    lPath_pcl_13,lPath_pcl_14,lPath_pcl_15,lPath_pcl_16,lPath_pcl_17,lPath_pcl_18,...
    lPath_FLANN_1,lPath_FLANN_2,lPath_FLANN_3,...
    lPath_vtw_1,lPath_vtw_2,lPath_vtw_3,lPath_vtw_4,lPath_vtw_5,lPath_vtw_6,lPath_vtw_7,lPath_vtw_8,lPath_vtw_9,...
    lPath_vtw_10,lPath_vtw_11,lPath_vtw_12,lPath_vtw_13,lPath_vtw_14,lPath_vtw_15,lPath_vtw_16,lPath_vtw_17,lPath_vtw_18,lPath_vtw_19,lPath_vtw_20,lPath_vtw_21,lPath_vtw_22,lPath_vtw_23,lPath_vtw_24,lPath_vtw_25,lPath_vtw_26,lPath_vtw_27,lPath_vtw_28,lPath_vtw_29,lPath_vtw_30,lPath_vtw_31,lPath_vtw_32,lPath_vtw_33,lPath_vtw_34,lPath_vtw_35,lPath_vtw_36,lPath_vtw_37,...
    lPath_vtw_38,lPath_vtw_39,lPath_vtw_40,lPath_vtw_41,lPath_vtw_42,lPath_vtw_43,lPath_vtw_44,lPath_vtw_45,lPath_vtw_46,lPath_vtw_47,lPath_vtw_48,lPath_vtw_49,lPath_vtw_50,lPath_vtw_51,lPath_vtw_52,lPath_vtw_53,lPath_vtw_54,lPath_vtw_55,lPath_vtw_56,lPath_vtw_57,lPath_vtw_58,lPath_vtw_59,lPath_vtw_60,lPath_vtw_61,lPath_vtw_62,lPath_vtw_63,lPath_vtw_64,lPath_vtw_65,...
    lPath_vtw_66,lPath_vtw_67,lPath_vtw_68,lPath_vtw_69,lPath_vtw_70,lPath_vtw_71,lPath_vtw_72,lPath_vtw_73,lPath_vtw_74,lPath_vtw_75,lPath_vtw_76,lPath_vtw_77,lPath_vtw_78,lPath_vtw_79,lPath_vtw_80,lPath_vtw_81,lPath_vtw_82,lPath_vtw_83,lPath_vtw_84,lPath_vtw_85,lPath_vtw_86,lPath_vtw_87,lPath_vtw_88,lPath_vtw_89,lPath_vtw_90,lPath_vtw_91,lPath_vtw_92,lPath_vtw_93,...
    lPath_vtw_94,lPath_vtw_95,lPath_vtw_96,lPath_vtw_97,lPath_vtw_98,lPath_vtw_99,lPath_vtw_100,lPath_vtw_101,lPath_vtw_102,lPath_vtw_103,lPath_vtw_104,lPath_vtw_105,lPath_vtw_106,lPath_vtw_107,lPath_vtw_108,lPath_vtw_109,...
    lpath_boost_1,lpath_boost_3,lpath_boost_5,lpath_boost_7,lpath_boost_9,lpath_boost_11,lpath_boost_13,lpath_boost_15,lpath_boost_17,lpath_boost_19,lpath_boost_21,...
    lpath_boost_23,lpath_boost_25,lpath_boost_27,lpath_boost_29,lpath_boost_31,lpath_boost_33,lpath_boost_35,lpath_boost_37,lpath_boost_39,lpath_boost_41,lpath_boost_43,...
    lpath_boost_45,lpath_boost_47,lpath_boost_49,lpath_boost_51,lpath_boost_53,lpath_boost_55,lpath_boost_57,lpath_boost_59,lpath_boost_61,lpath_boost_63,lpath_boost_65,...
    IPath_opencv,LPath1,LPath2 , ...                                                                % OPENCV
    lPath_opencv_0,lPath_opencv_1,lPath_opencv_2,lPath_opencv_4,...  % lPath_opencv_3 , lPath_opencv_5
    lPath_opencv_6,lPath_opencv_7,lPath_opencv_8,lPath_opencv_9,lPath_opencv_10, lPath_opencv_11,...
    lPath_opencv_12,lPath_opencv_14,lPath_opencv_15,lPath_opencv_16,... % lPath_opencv_13  lPath_opencv_17
    lPath_opencv_19,lPath_opencv_20,lPath_opencv_21,lPath_opencv_22,lPath_opencv_24,lPath_opencv_25); % lPath_opencv_18 lPath_opencv_26 lPath_opencv_23(tolto per antipodal)



% include
% CPPFLAGS = [ include_dir ' ' IPath_pcl ' ' IPath_boost ' ' IPath_Eigen ' ' lPath_FLANN ' ' lPath_vtw ' ' IPath_Qhull ' ' IPath_opencv ];
% CPPFLAGS = [' -largeArrayDims ' CPPFLAGS ];
% CXXFLAGS ='$CXXFLAGS -Wall -std=c++11';
% LDFLAGS  = [  LPath_PCL ' ' LPath_Boost ' ' LPath_FLANN ' ' LPath_QHull ' ' LPath_VTK ' ' LPath1 ' ' LPath2 ];
% LIBS     = [ lPath_pcl_1 ' ' lPath_pcl_2 ' ' lPath_pcl_3 ' ' lPath_pcl_4 ' ' lPath_pcl_5 ' ' lPath_pcl_6 ' ' lPath_pcl_7 ' ' lPath_pcl_8 ' ' lPath_pcl_9 ' ' lPath_pcl_10 ' ' lPath_pcl_11 ' ' lPath_pcl_12 ' ' lPath_pcl_13 ' ' lPath_pcl_14 ' ' lPath_pcl_15 ' ' lPath_pcl_16 ' ' lPath_pcl_17 ' ' lPath_pcl_18 ' ' lPath_opencv_0 ' ' lPath_opencv_1 ' ' lPath_opencv_2 ' ' lPath_opencv_3 ' ' lPath_opencv_4 ' '  lPath_opencv_5 ' ' lPath_opencv_6 ' ' lPath_opencv_7 ' ' lPath_opencv_8 ' ' lPath_opencv_9 ' ' lPath_opencv_10 ' '  lPath_opencv_11 ' ' lPath_opencv_12 ' ' lPath_opencv_13 ' ' lPath_opencv_14 ' ' lPath_opencv_15 ' ' lPath_opencv_16 ' '  lPath_opencv_17];
%
% mex(main_file, CPPFLAGS , LDFLAGS , LIBS);
