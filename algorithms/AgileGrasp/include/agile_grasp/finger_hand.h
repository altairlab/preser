/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FINGER_HAND_H
#define FINGER_HAND_H

#include <Eigen/Dense>
#include <iostream>
#include <vector>

/** FingerHand class
 *
 * \brief Calculate collision-free fingers
 * 
 * This class calculates a set of collision-free finger placements. The parameters are the dimensions of the 
 * robot hand and the desired "bite" that the grasp must have. The bite is how far the hand can be 
 * moved into the object.
 * 
*/
class FingerHand
{
public:

  /**
   * \brief Default constructor.
  */
  FingerHand()
  {
  }
	
	/**
   * \brief Constructor.
   * \param finger_width the width of the fingers
   * \param hand_outer_diameter the maximum aperture of the robot hand
   * \param hand_depth the length of the fingers
  */
//   FingerHand(double finger_width, double hand_outer_diameter, double hand_depth);
  FingerHand(double finger_width, double hand_outer_diameter,
		double hand_depth) :
		finger_width_(finger_width), hand_outer_diameter_(hand_outer_diameter), hand_depth_(
				hand_depth)
{
	int n = 10; // number of finger placements to consider over a single hand diameter

	Eigen::VectorXd fs_half;
	fs_half.setLinSpaced(n, 0.0, hand_outer_diameter - finger_width);
	finger_spacing_.resize(2 * fs_half.size());
	finger_spacing_
			<< (fs_half.array() - hand_outer_diameter_ + finger_width_).matrix(), fs_half;
	fingers_ = Eigen::Array<bool, 1, Eigen::Dynamic>::Constant(1, 2 * n, false);
//  std::cout << "fs_half: " << fs_half.transpose() << std::endl;
//  std::cout << "finger spacing: " << finger_spacing_.transpose() << std::endl;
}

  /**
   * \brief Find collision-free finger placements.
   * \param bite the minimum object height
  */
  void evaluateFingers(double bite)
  {
      back_of_hand_ = -1.0 * (hand_depth_ - bite);
//  std::cout << "back_of_hand_: " << back_of_hand_ << std::endl;

	fingers_.setConstant(false);

	// crop points at bite
	std::vector<int> cropped_indices;
	for (int i = 0; i < points_.cols(); i++)
	{
		if (points_(1, i) < bite)
		{
			cropped_indices.push_back(i);

			// Check that the hand would be able to extend by <bite> onto the object without causing back of hand
			// to collide with points.
			if (points_(1, i) < back_of_hand_)
			{
				return;
			}
		}
	}
//  std::cout << "     -A- " << " " << cropped_indices.size() << " " << points_.cols() << "\n";
	Eigen::MatrixXd cropped_points(points_.rows(), cropped_indices.size());
	for (int i = 0; i < cropped_indices.size(); i++)
	{
		cropped_points.col(i) = points_.col(cropped_indices[i]);
	}
//  std::cout << "     -C-\n";

	// identify free gaps
	int m = finger_spacing_.size();
//  std::cout << "m: " << m << ", finger_spacing_: " << finger_spacing_.transpose() << " " << finger_spacing_.size() << std::endl;
	for (int i = 0; i < m; i++)
	{
//    std::cout << "  i: " << i << std::endl;
		// int num_in_gap = (cropped_points.array() > finger_spacing_(i)
		// && cropped_points.array() < finger_spacing_(i) + finger_width_).count();
		int num_in_gap = 0;
		for (int j = 0; j < cropped_indices.size(); j++)
		{
			if (cropped_points(0, j) > finger_spacing_(i)
					&& cropped_points(0, j) < finger_spacing_(i) + finger_width_)
				num_in_gap++;
		}
//    std::cout << "i: " << i << ", numInGap: " << num_in_gap << " finger_width_: " << finger_width_ << " finger_spacing_: " << finger_spacing_(i) << std::endl;

		if (num_in_gap == 0)
		{
			int sum;

			if (i <= m / 2)
			{
//        std::cout << " i <= m/2 " << finger_spacing_(i) << "\n";
//        Eigen::Array<bool, 1, Eigen::Dynamic>
				sum = (cropped_points.row(0).array()
						> finger_spacing_(i) + finger_width_).count();
			}
			else
			{
//        std::cout << " i > m/2\n";
				sum = (cropped_points.row(0).array() < finger_spacing_(i)).count();
			}

//      std::cout << "                 i: " << i<< " sum: " << sum << " cropped_points.array(): " << cropped_points.array().rows() << " x " << cropped_points.array().cols() << std::endl;

			if (sum > 0)
			{
//        std::cout << "      sum larger 0\n";
				fingers_(i) = true;
//        std::cout << fingers_ << "\n";
			}
		}
//    std::cout << " -----\n";
	}
//  std::cout << "     -E-\n";
//  std::cout << " fingers_: " << fingers_ << std::endl;
  }
	
	/**
   * \brief Find robot hand configurations that fit the cloud.
  */
  void evaluateHand()
  {
      
//  std::cout << "evaluateHand()\n";
	int n = fingers_.size() / 2;
//  std::cout << " n: " << n;
	// hand_ = fingers_.block(0, 0, 1, n) && fingers_.block(0, n, 1, n);
	hand_.resize(1, n);
	for (int i = 0; i < n; i++)
	{
		if (fingers_(i) == 1 && fingers_(n + i) == 1)
			hand_(i) = 1;
		else
			hand_(i) = 0;
	}
//  std::cout << " hand_: " << hand_ << std::endl;
  }
	
	/**
   * \brief Set the grasp parameters.
   * 
   * The parameter @p bite is used to calculate the grasp width by only evaluating the width of 
   * the points below @p bite.
   * 
   * \param bite the minimum object height
  */
  void evaluateGraspParameters(double bite)
  {
      
//  double hor_pos = (hand_outer_diameter_ / 2.0)
//      + (finger_spacing_.block(0, 0, 1, hand_.cols()).array() * hand_.cast<double>()).sum() / hand_.sum();
//  std::cout << "hand_: " << hand_ << ", hand_.sum(): " << hand_.sum() << std::endl;
//  std::cout << "finger_spacing_: " << finger_spacing_.block(0, 0, 1, hand_.cols()).array() << std::endl;
//  std::cout << "hand_.cast<double>(): " << hand_.cast<double>() << std::endl;
//  std::cout << finger_spacing_.block(0, 0, 1, hand_.cols()).array() * hand_.cast<double>() << std::endl;
//  std::cout << (finger_spacing_.block(0, 0, 1, hand_.cols()).array() * hand_.cast<double>()).sum() << std::endl;

	double fs_sum = 0.0;
	for (int i = 0; i < hand_.size(); i++)
	{
		fs_sum += finger_spacing_(i) * hand_(i);
	}
	double hor_pos = (hand_outer_diameter_ / 2.0) + (fs_sum / hand_.sum());
//	std::cout << "fs_sum: " << fs_sum << ", hor_pos: " << hor_pos << ", hand_: "
//			<< hand_ << "\n";
	grasp_bottom << hor_pos, points_.row(1).maxCoeff();
	grasp_surface << hor_pos, points_.row(1).minCoeff();
//  std::cout << "  grasp_surface:\n" << grasp_surface << std::endl;
//  std::cout << "  grasp_bottom:\n" << grasp_bottom << std::endl;
//  std::cout << " EGP(1)\n";
//  std::cout << " hand_: " << hand_ << ", " << hand_.sum() << std::endl;

	// calculate handWidth. First find eroded hand. Then take points contained within two fingers of that hand.
	std::vector<int> hand_idx;
	for (int i = 0; i < hand_.cols(); i++)
	{
		if (hand_(i) == true)
			hand_idx.push_back(i);
	}
//  std::cout << hand_idx.size() << std::endl;
	int hand_eroded_idx = hand_idx[hand_idx.size() / 2];
//  std::cout << "hand_eroded_idx: " << hand_eroded_idx << std::endl;
	double left_finger_pos = finger_spacing_(hand_eroded_idx);
	double right_finger_pos = finger_spacing_(hand_.cols() + hand_eroded_idx);
	double max = -100000.0;
	double min = 100000.0;
//  std::cout << " EGP(2) left_finger_pos: " << left_finger_pos << " right_finger_pos: " << right_finger_pos << std::endl;
	for (int i = 0; i < points_.cols(); i++)
	{
		if (points_(1, i) < bite && points_(0, i) > left_finger_pos
				&& points_(0, i) < right_finger_pos)
		{
			if (points_(0, i) < min)
				min = points_(0, i);
			if (points_(0, i) > max)
				max = points_(0, i);
		}
	}
//  std::cout << " EGP(3) max: " << max << " min: " << min << std::endl;

	grasp_width_ = max - min;
  }
	
	/**
   * \brief Select center hand and try to extend it into the object as deeply as possible.
   * \param init_deepness the initial deepness (usually the minimum object height)
   * \param max_deepness the maximum allowed deepness (usually the finger length)
  */
  void deepenHand(double init_deepness, double max_deepness)
  {
      
//  std::cout << "deepenHand begin: " << hand_ << std::endl;
//	std::cout << "deepenHand begin. hand_: " << hand_ << ", fingers_: "
//			<< fingers_ << std::endl;
	std::vector<int> hand_idx;

	for (int i = 0; i < hand_.cols(); i++)
	{
		if (hand_(i) == true)
			hand_idx.push_back(i);
	}

	if (hand_idx.size() == 0)
		return;

	// choose middle hand
	int hand_eroded_idx = hand_idx[ceil(hand_idx.size() / 2.0) - 1]; // middle index
	Eigen::Array<bool, 1, Eigen::Dynamic> hand_eroded = Eigen::Array<bool, 1,	
		Eigen::Dynamic>::Constant(hand_.cols(), false);
	hand_eroded(hand_eroded_idx) = true;
//	std::cout << "hand_eroded_idx: " << hand_eroded_idx << ", hand_eroded: "
//			<< hand_eroded << ", hand_idx.size() / 2: " << hand_idx.size() / 2
//			<< std::endl;

	// attempt to deepen hand
	double deepen_step_size = 0.005;
	FingerHand new_hand = *this;
	FingerHand last_new_hand = new_hand;
//  std::cout << "new_hand.hand_: " << new_hand.getHand() << std::endl;
//  std::cout << "d = " << init_deepness + deepen_step_size << ", max_deepness: " << max_deepness << std::endl;
	for (double d = init_deepness + deepen_step_size; d <= max_deepness; d +=	deepen_step_size)
	{
//    std::cout << "d(): " << d << std::endl;
		new_hand.evaluateFingers(d);
		new_hand.evaluateHand();
//		std::cout << "d: " << d << ", new_hand.hand_: " << new_hand.getHand()
//				<< std::endl;
//		std::cout << "hand_eroded: " << hand_eroded << std::endl;
//    std::cout << " => last_new_hand.hand_: " << last_new_hand.getHand() << std::endl;
//     if ((new_hand.hand_ && hand_eroded).count() == 0) // hand is so deep that it does not exist any longer
//       break;
		bool is_too_deep = true;
		for (int i = 0; i < hand_eroded.cols(); i++)
		{
			if (new_hand.hand_(i) == 1 && hand_eroded(i) == 1) // hand is not so deep that it does not exist any longer
			{
				is_too_deep = false;
				break;
			}
		}
		if (is_too_deep)
			break;
		last_new_hand = new_hand;
//    std::cout << "d[]: " << d << ", last_new_hand.hand: " << last_new_hand.getHand() << ", new_hand.hand: " << new_hand.getHand() << std::endl;
	}
	*this = last_new_hand; // recover most deep hand
	hand_ = hand_eroded;
//	std::cout << "deepenHand done. hand_: " << hand_ << ", fingers_: " << fingers_
//			<< std::endl;
  }
	
	/**
	 * \brief Set the points.
	 * 
	 * The points contained in the matrix @p points are assumed to be already rotated into the robot 
	 * hand frame and offset to a point within the local neighborhood.
	 * 
	 * \param points the points to be set
	*/
  void setPoints(const Eigen::MatrixXd& points)
  {
    this->points_ = points;
  }
	
	/**
	 * \brief Return the depth of the hand.
	 * \return the hand depth
	*/
  double getHandDepth() const
  {
    return hand_depth_;
  }
	
	/**
	 * \brief Return the hand configuration evaluations.
	 * \return the hand configuration evaluations
	*/
  const Eigen::Array<bool, 1, Eigen::Dynamic>& getHand() const
  {
    return hand_;
  }
	
	/**
	 * \brief Return the finger placement evaluations.
	 * \return the hand configuration evaluations
	*/
  const Eigen::Array<bool, 1, Eigen::Dynamic>& getFingers() const
  {
    return fingers_;
  }
	
	/**
	 * \brief Return the grasp position between the end of the finger tips.
	 * \return the 2x1 grasp position between the end of the finger tips
	*/
  const Eigen::Vector2d& getGraspBottom() const
  {
    return grasp_bottom;
  }
	
	/**
	 * \brief Return the grasp position between the end of the finger tips projected onto the back of the hand.
	 * \return the 2x1 grasp position between the end of the finger tips projected onto the back of the hand
	*/
  const Eigen::Vector2d& getGraspSurface() const
  {
    return grasp_surface;
  }
	
	/**
	 * \brief Return the width of the object contained in the grasp.
	 * \return the width of the object contained in the grasp
	*/
  double getGraspWidth() const
  {
    return grasp_width_;
  }
	
	/**
	 * \brief Return where the back of the hand is.
	 * \return the back of the hand
	*/
  double getBackOfHand() const
  {
    return back_of_hand_;
  }

private:

  double finger_width_; ///< the width of the robot hand fingers
  double hand_outer_diameter_; ///< the maximum aperture of the robot hand
  double hand_depth_; ///< the hand depth (finger length)

  double back_of_hand_; ///< where the back of the hand is (distance from back to position between finger tip ends)

  double grasp_width_; ///< the width of the object contained in the grasp

  Eigen::VectorXd finger_spacing_; ///< the possible finger placements
  Eigen::Array<bool, 1, Eigen::Dynamic> fingers_; ///< indicates which finger placements are collision-free
  Eigen::Array<bool, 1, Eigen::Dynamic> hand_;///< indicates which hand configurations fit the point cloud
  Eigen::MatrixXd points_; ///< the points in the point neighborhood (see FingerHand::setPoints)
  Eigen::Vector2d grasp_bottom; ///< the grasp position between the end of the finger tips
  Eigen::Vector2d grasp_surface; ///< the position between the end of the finger tips projected to the back of the hand
};

#endif
