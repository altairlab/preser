/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PLOT_H
#define PLOT_H

#include "mex.h"

#include <pcl/visualization/pcl_visualizer.h>

// #include <geometry_msgs/Point.h>
// #include <ros/ros.h>
// #include <visualization_msgs/MarkerArray.h>

#include <agile_grasp/grasp_hypothesis.h>
#include <agile_grasp/handle.h>
#include <agile_grasp/quadric.h>


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
typedef pcl::PointCloud<pcl::PointNormal> PointCloudNormal;

/** Plot class
 *
 * This class provides a set of visualization methods that visualize the output of the algorithm and
 * some intermediate steps. The visualization is done using PCL Visualizer.
 *
 */
class Plot
{
public:
    
    Plot()
    {
        
    }
    
    /**
     * \brief Plot a set of grasp hypotheses.
     * \param hand_list the set of grasp hypotheses to be plotted
     * \param cloud the point cloud to be plotted
     * \param str the title of the visualization window
     * \param use_grasp_bottom whether the grasps plotted originate from the grasp bottom point
     */
    void plotHands(const std::vector<GraspHypothesis>& hand_list, const PointCloud::Ptr& cloud, std::string str,
            bool use_grasp_bottom = false)
    {
        PointCloudNormal::Ptr hands_cloud = createNormalsCloud(hand_list, false, false);
        PointCloudNormal::Ptr antipodal_hands_cloud = createNormalsCloud(hand_list, true, false);
        plotHandsHelper(hands_cloud, antipodal_hands_cloud, cloud, str, use_grasp_bottom);
    }
    
    /**
     * \brief Plot a set of grasp hypotheses and a set of antipodal grasps.
     * \param hand_list the set of grasp hypotheses to be plotted
     * \param antipodal_hand_list the set of antipodal grasps to be plotted
     * \param cloud the point cloud to be plotted
     * \param str the title of the visualization window
     * \param use_grasp_bottom whether the grasps plotted originate from the grasp bottom point
     */
    void plotHands(const std::vector<GraspHypothesis>& hand_list,
            const std::vector<GraspHypothesis>& antipodal_hand_list, const PointCloud::Ptr& cloud, std::string str,
            bool use_grasp_bottom = false)
    {
        PointCloudNormal::Ptr hands_cloud = createNormalsCloud(hand_list, false, false);
        PointCloudNormal::Ptr antipodal_hands_cloud = createNormalsCloud(antipodal_hand_list, true,
                false);
        plotHandsHelper(hands_cloud, antipodal_hands_cloud, cloud, str, use_grasp_bottom);
    }
    
    /**
     * \brief Plot a set of samples.
     * \param index_list the list of samples (indices into the point cloud)
     * \param cloud the point cloud to be plotted
     */
    void plotSamples(const std::vector<int>& index_list, const PointCloud::Ptr& cloud)
    {
        PointCloud::Ptr samples_cloud(new PointCloud);
        for (int i = 0; i < index_list.size(); i++)
            samples_cloud->points.push_back(cloud->points[index_list[i]]);
        
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = createViewer("Samples");
        viewer->addPointCloud<pcl::PointXYZ>(cloud, "registered point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
                "registered point cloud");
        viewer->addPointCloud<pcl::PointXYZ>(samples_cloud, "samples cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "samples cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0,
                "samples cloud");
        
        runViewer(viewer);
    }
    
    /**
     * \brief Plot a set of quadrics by plotting their local axes.
     * \param quadric_list the list of quadrics to be plotted
     * \param cloud the point cloud to be plotted
     */
    void plotLocalAxes(const std::vector<Quadric>& quadric_list, const PointCloud::Ptr& cloud)
    {
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = createViewer("Local Axes");
        viewer->addPointCloud<pcl::PointXYZ>(cloud, "registered point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
                "registered point cloud");
        
        for (int i = 0; i < quadric_list.size(); i++)
            quadric_list[i].plotAxes((void*) &viewer, i);
        
        runViewer(viewer);
    }
    
    /**
     * \brief Plot the camera source for each point in the point cloud.
     * \param pts_cam_source_in the camera source for each point in the point cloud
     * \param cloud the point cloud to be plotted
     */
    void plotCameraSource(const Eigen::VectorXi& pts_cam_source_in, const PointCloud::Ptr& cloud)
    {
        PointCloud::Ptr left_cloud(new PointCloud);
        PointCloud::Ptr right_cloud(new PointCloud);
        
        for (int i = 0; i < pts_cam_source_in.size(); i++)
        {
            if (pts_cam_source_in(i) == 0)
                left_cloud->points.push_back(cloud->points[i]);
            else if (pts_cam_source_in(i) == 1)
                right_cloud->points.push_back(cloud->points[i]);
        }
        
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = createViewer("Camera Sources");
        viewer->addPointCloud<pcl::PointXYZ>(left_cloud, "left point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
                "left point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0,
                "left point cloud");
        viewer->addPointCloud<pcl::PointXYZ>(right_cloud, "right point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
                "right point cloud");
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.0, 1.0, 0.0,
                "right point cloud");
        runViewer(viewer); 
    }
    
    /**
     * \brief Create publishers for handles and grasp hypotheses to visualize them in Rviz.
     * \param node the ROS node for which the publishers are advertised
     * \param marker_lifetime the lifetime of each visual marker
     */
// 		void createVisualPublishers(ros::NodeHandle& node, double marker_lifetime); // ROS
    
    /**
     * \brief Plot the grasp hypotheseses in Rviz.
     * \param hand_list the list of grasp hypotheses
     * \param frame the frame that the poses of the grasp hypotheses are relative to
     */
// 		void plotGraspsRviz(const std::vector<GraspHypothesis>& hand_list, const std::string& frame,
// 			bool is_antipodal = false);  // ROS
    
    /**
     * \brief Plot the handles in Rviz.
     * \param handle_list the list of handles
     * \param frame the frame that the poses of the handle grasps are relative to
     */
// 		void plotHandlesRviz(const std::vector<Handle>& handle_list, const std::string& frame);  // ROS
    
    /**
     * \brief Plot the handles.
     * \param handle_list the list of handles
     * \param frame the frame that the poses of the handle grasps are relative to
     * \param str the title of the PCL visualization window
     */
    void plotHandles(const std::vector<Handle>& handle_list, const PointCloud::Ptr& cloud, std::string str)
    {
        double colors[10][3] = { { 0, 0.4470, 0.7410 }, { 0.8500, 0.3250, 0.0980 }, { 0.9290, 0.6940, 0.1250 }, {
            0.4940, 0.1840, 0.5560 }, { 0.4660, 0.6740, 0.1880 }, { 0.3010, 0.7450, 0.9330 }, { 0.6350, 0.0780,
                    0.1840 }, { 0, 0.4470, 0.7410 }, { 0.8500, 0.3250, 0.0980 }, { 0.9290, 0.6940, 0.1250} };
                    
                    //	    0.4940    0.1840    0.5560
                    //	    0.4660    0.6740    0.1880
                    //	    0.3010    0.7450    0.9330
                    //	    0.6350    0.0780    0.1840
                    //	         0    0.4470    0.7410
                    //	    0.8500    0.3250    0.0980
                    //	    0.9290    0.6940    0.1250
                    //	    0.4940    0.1840    0.5560
                    //	    0.4660    0.6740    0.1880
                    //	    0.3010    0.7450    0.9330
                    //	    0.6350    0.0780    0.1840
                    
                    std::vector<pcl::PointCloud<pcl::PointNormal>::Ptr> clouds;
                    pcl::PointCloud<pcl::PointNormal>::Ptr handle_cloud(new pcl::PointCloud<pcl::PointNormal>());
                    
                    for (int i = 0; i < handle_list.size(); i++)
                    {
                        pcl::PointNormal p;
                        p.x = handle_list[i].getHandsCenter()(0);
                        p.y = handle_list[i].getHandsCenter()(1);
                        p.z = handle_list[i].getHandsCenter()(2);
                        p.normal[0] = -handle_list[i].getApproach()(0);
                        p.normal[1] = -handle_list[i].getApproach()(1);
                        p.normal[2] = -handle_list[i].getApproach()(2);
                        handle_cloud->points.push_back(p);
                        
                        const std::vector<int>& inliers = handle_list[i].getInliers();
                        const std::vector<GraspHypothesis>& hand_list = handle_list[i].getHandList();
                        pcl::PointCloud<pcl::PointNormal>::Ptr axis_cloud(new pcl::PointCloud<pcl::PointNormal>);
                        
                        for (int j = 0; j < inliers.size(); j++)
                        {
                            pcl::PointNormal p;
                            p.x = hand_list[inliers[j]].getGraspSurface()(0);
                            p.y = hand_list[inliers[j]].getGraspSurface()(1);
                            p.z = hand_list[inliers[j]].getGraspSurface()(2);
                            p.normal[0] = -hand_list[inliers[j]].getApproach()(0);
                            p.normal[1] = -hand_list[inliers[j]].getApproach()(1);
                            p.normal[2] = -hand_list[inliers[j]].getApproach()(2);
                            axis_cloud->points.push_back(p);
                        }
                        clouds.push_back(axis_cloud);
                    }
                    
                    std::string title = "Handles: " + str;
                    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer(title));
                    viewer->setBackgroundColor(1, 1, 1);
                    //viewer->setPosition(0, 0);
                    //viewer->setSize(640, 480);
                    viewer->addPointCloud<pcl::PointXYZ>(cloud, "registered point cloud");
                    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
                            "registered point cloud");
                    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.0, 1.0, 0.0,
                            "registered point cloud");
                    
                    for (int i = 0; i < clouds.size(); i++)
                    {
                        std::string name = "handle_" + boost::lexical_cast<std::string>(i);
                        int ci = i % 6;
//		std::cout << "ci: " << ci << "\n";
                        viewer->addPointCloud<pcl::PointNormal>(clouds[i], name);
                        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, colors[ci][0],
                                colors[ci][1], colors[ci][2], name);
                        std::string name2 = "approach_" + boost::lexical_cast<std::string>(i);
                        viewer->addPointCloudNormals<pcl::PointNormal>(clouds[i], 1, 0.04, name2);
                        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, name2);
                        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, colors[ci][0],
                                colors[ci][1], colors[ci][2], name2);
                    }
                    
                    viewer->addPointCloud<pcl::PointNormal>(handle_cloud, "handles");
                    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0, "handles");
                    viewer->addPointCloudNormals<pcl::PointNormal>(handle_cloud, 1, 0.08, "approach");
                    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 4, "approach");
                    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0, "approach");
                    
                    // viewer->addCoordinateSystem(1.0, "", 0);
                    viewer->initCameraParameters();
                    viewer->setPosition(0, 0);
                    viewer->setSize(640, 480);
                    
                    while (!viewer->wasStopped())
                    {
                        viewer->spinOnce(100);
                        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
                    }
                    
                    viewer->close();
    }
    
private:
    
    /**
     * \brief Create a point cloud with normals that represent the approach vectors for a set of
     * grasp hypotheses.
     * \param hand_list the set of grasp hypotheses
     * \param plots_only_antipodal whether only the approach vectors of antipodal grasps are
     * considered
     * \param plots_grasp_bottom whether the approach vectors plotted originate from the grasp
     * bottom point
     */
    PointCloudNormal::Ptr createNormalsCloud(const std::vector<GraspHypothesis>& hand_list,
            bool plots_only_antipodal, bool plots_grasp_bottom)
    {
        PointCloudNormal::Ptr cloud(new PointCloudNormal);

	for (int i = 0; i < hand_list.size(); i++)
	{
		Eigen::Matrix3Xd grasp_surface = hand_list[i].getGraspSurface();
		Eigen::Matrix3Xd grasp_bottom = hand_list[i].getGraspBottom();
		Eigen::Matrix3Xd hand_approach = hand_list[i].getApproach();

    if (!plots_only_antipodal || (plots_only_antipodal && hand_list[i].isFullAntipodal()))
    {
      pcl::PointNormal p;
      if (!plots_grasp_bottom)
      {
        p.x = grasp_surface(0);
        p.y = grasp_surface(1);
        p.z = grasp_surface(2);
      }
      else
      {
        p.x = grasp_bottom(0);
        p.y = grasp_bottom(1);
        p.z = grasp_bottom(2);
      }
      p.normal[0] = -hand_approach(0);
      p.normal[1] = -hand_approach(1);
      p.normal[2] = -hand_approach(2);
      cloud->points.push_back(p);
    }
	}

	return cloud;
    }
    
    /**
     * \brief Add a point cloud with normals to a PCL visualizer.
     * \param viewer the PCL visualizer that the cloud is added to
     * \param cloud the cloud to be added
     * \param line_width the line width for drawing normals
     * \param color_cloud the color that is used to draw the cloud
     * \param color_normals the color that is used to draw the normals
     * \param cloud_name an identifier string for the cloud
     * \param normals_name an identifier string for the normals
     */
    void addCloudNormalsToViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
            const PointCloudNormal::Ptr& cloud, double line_width, double* color_cloud, double* color_normals,
            const std::string& cloud_name, const std::string& normals_name)
    {
        viewer->addPointCloud<pcl::PointNormal>(cloud, cloud_name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, color_cloud[0],
		color_cloud[1], color_cloud[2], cloud_name);
	viewer->addPointCloudNormals<pcl::PointNormal>(cloud, 1, 0.04, normals_name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, line_width,
		normals_name);
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, color_normals[0],
		color_normals[1], color_normals[2], normals_name);
        
    }
    
    /**
     * \brief Plot two point clouds representing grasp hypotheses and antipodal grasps,
     * respectively.
     * \param hands_cloud the cloud that represents the grasp hypotheses
     * \param antipodal_hands_cloud the cloud that represents the antipodal grasps
     * \param cloud the point cloud to be plotted
     * \param str the title of the visualization window
     * \param use_grasp_bottom whether the grasps plotted originate from the grasp bottom point
     */
    void plotHandsHelper(const PointCloudNormal::Ptr& hands_cloud, const PointCloudNormal::Ptr& antipodal_hands_cloud,
            const PointCloud::Ptr& cloud,	std::string str, bool use_grasp_bottom)
    {
        std::cout << "Drawing " << hands_cloud->size() << " grasps of which " << antipodal_hands_cloud->size()
			<< " are antipodal grasps.\n";

	std::string title = "Hands: " + str;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = createViewer(title);  
	viewer->addPointCloud<pcl::PointXYZ>(cloud, "registered point cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1,
		"registered point cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.0, 0.8, 0.0,
		"registered point cloud");

	double green[3] = { 0.0, 1.0, 0.0 };
	double cyan[3] = { 0.0, 0.4, 0.8 };
	addCloudNormalsToViewer(viewer, hands_cloud, 2, green, cyan, std::string("hands"),
		std::string("approaches"));

	if (antipodal_hands_cloud->size() > 0)
	{
		double red[3] = { 1.0, 0.0, 0.0 };
		addCloudNormalsToViewer(viewer, antipodal_hands_cloud, 2, green, red, std::string("antipodal_hands"),
			std::string("antipodal_approaches"));
	}

	runViewer(viewer);
    }
    
    /**
     * \brief Run/show a PCL visualizer until an escape key is hit.
     * \param viewer the PCL visualizer to be shown
     */
    void runViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer)
    {
        // viewer->addCoordinateSystem(1.0, "", 0);
        viewer->initCameraParameters();
        viewer->setPosition(0, 0);
        viewer->setSize(640, 480);
        
        while (!viewer->wasStopped())
        {
            viewer->spinOnce(100);
            boost::this_thread::sleep(boost::posix_time::microseconds(100000));
        }
        
        viewer->close();
        
        //	std::vector<pcl::visualization::Camera> cam;
        //
        //	// print the position of the camera
        //	viewer->getCameras(cam);
        //	std::cout << "Cam: " << std::endl << " - pos: (" << cam[0].pos[0] << ", " << cam[0].pos[1] << ", "
        //			<< cam[0].pos[2] << ")" << std::endl << " - view: (" << cam[0].view[0] << ", " << cam[0].view[1] << ", "
        //			<< cam[0].view[2] << ")" << std::endl << " - focal: (" << cam[0].focal[0] << ", " << cam[0].focal[1]
        //			<< ", " << cam[0].focal[2] << ")" << std::endl;
    }
    
    /**
     * \brief Create a PCL visualizer.
     * \param title the title of the visualization window
     */
    boost::shared_ptr<pcl::visualization::PCLVisualizer> createViewer(std::string title)
    {
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer(title));
        viewer->setBackgroundColor(1, 1, 1);
        return viewer;
    }
    
    /**
     * \brief Create a visual marker for an approach vector in Rviz.
     * \param frame the frame that the marker's pose is relative to
     * \param center the position of the marker
     * \param approach the approach vector
     * \param id the identifier of the marker
     * \param color a 3-element array defining the markers color in RGB
     * \param alpha the transparency level of the marker
     * \param diam the diameter of the marker
     */
// 		visualization_msgs::Marker createApproachMarker(const std::string& frame, const geometry_msgs::Point& center,
// 			const Eigen::Vector3d& approach, int id, const double* color, double alpha, double diam); // ROS
    
    /**
     * Create a visual marker.
     * \param frame the frame that the marker's pose is relative to
     */
// 		visualization_msgs::Marker createMarker(const std::string& frame); // ROS
    
// 		ros::Publisher hypotheses_pub_; ///< ROS publisher for grasp hypotheses (Rviz) // ROS
// 		ros::Publisher antipodal_pub_; ///< ROS publisher for antipodal grasps (Rviz) // ROS
// 		ros::Publisher handles_pub_; ///< ROS publisher for handles (Rviz) // ROS
// 		double marker_lifetime_; ///< max time that markers are visualized in Rviz // ROS
};

#endif /* PLOT_H */
