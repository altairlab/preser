/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HAND_SEARCH_H
#define HAND_SEARCH_H

#include "mex.h"

#include <Eigen/Dense>

#include <pcl/filters/random_sample.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>

#include <agile_grasp/finger_hand.h>
#include <agile_grasp/grasp_hypothesis.h>
#include <agile_grasp/plot.h>
#include <agile_grasp/quadric.h>
#include <agile_grasp/rotating_hand.h>


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

/** HandSearch class
 *
 * \brief Search for grasp hypotheses.
 *
 * This class searches for grasp hypotheses in a point cloud by first fitting a quadratic surface
 * to a small, local point neighborhood, and then finding geometrically feasible grasp hypotheses for
 * a larger point neighborhood. It can also estimate whether the grasp is antipodal from the normals of
 * the point neighborhood.
 *
 */
class HandSearch
{
public:
    
    /**
     * \brief Constructor.
     * \param finger_width the width of the robot hand fingers
     * \param hand_outer_diameter the maximum robot hand aperture
     * \param hand_depth the hand depth (length of fingers)
     * \param hand_height the hand extends plus/minus this value along the hand axis
     * \param init_bite the minimum object height
     * \param num_threads the number of CPU threads to be used for the search
     * \param num_samples the number of samples drawn from the point cloud
     * \param plots_hands whether grasp hypotheses are plotted
     */
    HandSearch(double finger_width, double hand_outer_diameter,
            double hand_depth, double hand_height, double init_bite, int num_threads,
            int num_samples, bool plots_hands) : finger_width_(finger_width),
                    hand_outer_diameter_(hand_outer_diameter), hand_depth_(hand_depth),
                    hand_height_(hand_height), init_bite_(init_bite),
                    num_threads_(num_threads), num_samples_(num_samples),
                    plots_hands_(plots_hands), plots_samples_(false),
                    plots_local_axes_(false), uses_determinstic_normal_estimation_(false),
                    nn_radius_taubin_(0.03), nn_radius_hands_(0.08) { }
            
            /**
             * \brief Find grasp hypotheses in a point cloud.
             *
             * If the parameter @p indices is of size 0, then indices are randomly drawn from the point
             * cloud according to a uniform distribution.
             *
             * \param cloud the point cloud that is searched for grasp hypotheses
             * \param pts_cam_source the camera source for each point in the point cloud
             * \param indices the list of point cloud indices that are used in the search
             * \param cloud_plot the point cloud that is used for plotting
             * \param calculates_antipodal whether grasp hypotheses are estimated to be antipodal
             * \param uses_clustering whether clustering is used to divide the point cloud (useful for creating training data)
             * \return a list of grasp hypotheses
             */
            std::vector<GraspHypothesis> findHands(const PointCloud::Ptr cloud,
                    const Eigen::VectorXi& pts_cam_source, const std::vector<int>& indices,
                    const PointCloud::Ptr cloud_plot, bool calculates_antipodal,
                    bool uses_clustering)
            {
                
                // create KdTree for neighborhood search
                pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
                kdtree.setInputCloud(cloud);
                
                cloud_normals_.resize(3, cloud->size());
                cloud_normals_.setZero(3, cloud->size());
                
                // calculate normals for all points
                if (calculates_antipodal)
                {
                    std::cout << "Calculating normals for all points\n";
                    nn_radius_taubin_ = 0.01;
                    std::vector<int> indices_cloud(cloud->size());
                    for (int i = 0; i < indices_cloud.size(); i++)
                        indices_cloud[i] = i;
                    findQuadrics(cloud, pts_cam_source, kdtree, indices_cloud);
                    nn_radius_taubin_ = 0.03;
                }
                
                // draw samples from the point cloud uniformly
                std::vector<int> indices_rand;
                Eigen::VectorXi hands_cam_source;
                if (indices.size() == 0)
                {
                    double t_rand = omp_get_wtime();
                    std::cout << "Generating uniform random indices ...\n";
                    indices_rand.resize(num_samples_);
                    pcl::RandomSample<pcl::PointXYZ> random_sample;
                    random_sample.setInputCloud(cloud);
                    random_sample.setSample(num_samples_);
                    random_sample.filter(indices_rand);
                    hands_cam_source.resize(num_samples_);
                    for (int i = 0; i < num_samples_; i++)
                        hands_cam_source(i) = pts_cam_source(indices_rand[i]);
                    std::cout << " Done in " << omp_get_wtime() - t_rand << " sec\n";
                }
                else
                    indices_rand = indices;
                
                if (plots_samples_)
                    plot_.plotSamples(indices_rand, cloud);
                
                // find quadrics
                // std::cout << "Estimating local axes ...\n";
				mexPrintf("Estimating local axes ...\n ");
                std::vector<Quadric> quadric_list = findQuadrics(cloud, pts_cam_source, kdtree, indices_rand);
                if (plots_local_axes_)
                    plot_.plotLocalAxes(quadric_list, cloud_plot);
                
                // find hands
                // std::cout << "Finding hand poses ...\n";
				mexPrintf("Finding hand poses .. \n ");
                std::vector<GraspHypothesis> hand_list = findHands(cloud, pts_cam_source, quadric_list, hands_cam_source, kdtree);
                
                return hand_list;
            }
            
            
private:
    
    /**
     * \brief Find quadratic surfaces in a point cloud.
     * \param cloud the point cloud that is searched for quadrics
     * \param pts_cam_source the camera source for each point in the point cloud
     * \param kdtree the KD tree that is used for finding point neighborhoods
     * \param indices the list of point cloud indices that are used in the search
     * \return a list of quadratic surfaces
     */
    std::vector<Quadric> findQuadrics(const PointCloud::Ptr cloud, const Eigen::VectorXi& pts_cam_source,
            const pcl::KdTreeFLANN<pcl::PointXYZ>& kdtree, const std::vector<int>& indices)
    {
        double t1 = omp_get_wtime();
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;
        std::vector<Eigen::Matrix4d, Eigen::aligned_allocator<Eigen::Matrix4d> > T_cams;
        T_cams.push_back(cam_tf_left_);
        T_cams.push_back(cam_tf_right_);
        std::vector<Quadric> quadric_list(indices.size());
        
#ifdef _OPENMP // parallelization using OpenMP
#pragma omp parallel for private(nn_indices, nn_dists) num_threads(num_threads_)
#endif
        for (int i = 0; i < indices.size(); i++)
        {
            const pcl::PointXYZ& sample = cloud->points[indices[i]];
//    std::cout << "i: " << i << ", index: " << indices[i] << ", sample: " << sample << std::endl;
            
            if (kdtree.radiusSearch(sample, nn_radius_taubin_, nn_indices, nn_dists) > 0)
            {
                Eigen::VectorXi nn_cam_source(nn_indices.size());
//      std::cout << " Found " << nn_indices.size() << " neighbors.\n";
                
                for (int j = 0; j < nn_cam_source.size(); j++)
                {
                    nn_cam_source(j) = pts_cam_source(nn_indices[j]);
                }
                
                Eigen::Vector3d sample_eigen = sample.getVector3fMap().cast<double>();
                Quadric q(T_cams, cloud, sample_eigen, uses_determinstic_normal_estimation_);
                q.fitQuadric(nn_indices);
//      std::cout << " Fitted quadric\n";
                q.findTaubinNormalAxis(nn_indices, nn_cam_source);
//      std::cout << " Found local axes\n";
                quadric_list[i] = q;
                cloud_normals_.col(indices[i]) = q.getNormal();
            }
        }
        
        double t2 = omp_get_wtime();
        std::cout << "Fitted " << quadric_list.size() << " quadrics in " << t2 - t1 << " sec.\n";
        
//  quadric_list[0].print(); // debugging
        plot_.plotLocalAxes(quadric_list, cloud);
        
        return quadric_list;
    }
    
    /**
     * \brief Find grasp hypotheses in a point cloud given a list of quadratic surfaces.
     * \param cloud the point cloud that is searched for quadrics
     * \param pts_cam_source the camera source for each point in the point cloud
     * \param quadric_list the list of quadratic surfaces
     * \param hands_cam_source the camera source for each sample
     * \param kdtree the KD tree that is used for finding point neighborhoods
     * \return a list of grasp hypotheses
     */
    std::vector<GraspHypothesis> findHands(const PointCloud::Ptr cloud, const Eigen::VectorXi& pts_cam_source,
            const std::vector<Quadric>& quadric_list, const Eigen::VectorXi& hands_cam_source,
            const pcl::KdTreeFLANN<pcl::PointXYZ>& kdtree)
    {
        double t1 = omp_get_wtime();
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;
        Eigen::Matrix3Xd nn_normals(3, nn_indices.size());
        Eigen::VectorXi nn_cam_source(nn_indices.size());
        Eigen::Matrix3Xd centered_neighborhood(3, nn_indices.size());
        std::vector<RotatingHand> hand_list(quadric_list.size());
//  std::vector<RotatingHand> hand_list;
        double time_eval_hand = 0.0;
        double time_iter = 0.0;
        double time_nn = 0.0;
        double time_tf = 0.0;
        
        std::vector< std::vector<GraspHypothesis> > grasp_lists(quadric_list.size(), std::vector<GraspHypothesis>(0));
        
#ifdef _OPENMP // parallelization using OpenMP
#pragma omp parallel for private(nn_indices, nn_dists, nn_normals, nn_cam_source, centered_neighborhood) num_threads(num_threads_)
#endif
        for (std::size_t i = 0; i < quadric_list.size(); i++)
        {
            double timei = omp_get_wtime();
            pcl::PointXYZ sample;
            sample.x = quadric_list[i].getSample()(0);
            sample.y = quadric_list[i].getSample()(1);
            sample.z = quadric_list[i].getSample()(2);
//    std::cout << "i: " << i << ", sample: " << sample << std::endl;
            
            if (kdtree.radiusSearch(sample, nn_radius_hands_, nn_indices, nn_dists) > 0)
            {
                time_nn += omp_get_wtime() - timei;
                nn_normals.setZero(3, nn_indices.size());
                nn_cam_source.setZero(nn_indices.size());
                centered_neighborhood.setZero(3, nn_indices.size());
                
                for (int j = 0; j < nn_indices.size(); j++)
                {
                    nn_cam_source(j) = pts_cam_source(nn_indices[j]);
                    centered_neighborhood.col(j) = (cloud->points[nn_indices[j]].getVector3fMap()
                    - sample.getVector3fMap()).cast<double>();
                    nn_normals.col(j) = cloud_normals_.col(nn_indices[j]);
                }
                
                FingerHand finger_hand(finger_width_, hand_outer_diameter_, hand_depth_);
                
                Eigen::Vector3d sample_eig = sample.getVector3fMap().cast<double>();
                RotatingHand rotating_hand(cam_tf_left_.block<3, 1>(0, 3) - sample_eig,
                        cam_tf_right_.block<3, 1>(0, 3) - sample_eig, finger_hand, tolerant_antipodal_, hands_cam_source(i));
                const Quadric& q = quadric_list[i];
                double time_tf1 = omp_get_wtime();
                rotating_hand.transformPoints(centered_neighborhood, q.getNormal(), q.getCurvatureAxis(), nn_normals,
                        nn_cam_source, hand_height_);
                time_tf += omp_get_wtime() - time_tf1;
                double time_eval1 = omp_get_wtime();
                std::vector<GraspHypothesis> grasps = rotating_hand.evaluateHand(init_bite_, sample_eig, true);
                time_eval_hand += omp_get_wtime() - time_eval1;
                
                if (grasps.size() > 0)
                {
                    // grasp_list.insert(grasp_list.end(), grasps.begin(), grasps.end());
                    grasp_lists[i] = grasps;
                }
            }
            
            time_iter += omp_get_wtime() - timei;
        }
        time_eval_hand /= quadric_list.size();
        time_nn /= quadric_list.size();
        time_iter /= quadric_list.size();
        time_tf /= quadric_list.size();
        std::cout << " avg time for transforming point neighborhood: " << time_tf << " sec.\n";
        std::cout << " avg time for NN search: " << time_nn << " sec.\n";
        std::cout << " avg time for rotating_hand.evaluate(): " << time_eval_hand << " sec.\n";
        std::cout << " avg time per iteration: " << time_iter << " sec.\n";
        
        std::vector<GraspHypothesis> grasp_list;
        for (std::size_t i = 0; i < grasp_lists.size(); i++)
        {
            // std::cout << i << " " << grasp_lists[i].size() << "\n";
            if (grasp_lists[i].size() > 0)
                grasp_list.insert(grasp_list.end(), grasp_lists[i].begin(), grasp_lists[i].end());
        }
        
        double t2 = omp_get_wtime();
        std::cout << " Found " << grasp_list.size() << " robot hand poses in " << t2 - t1 << " sec.\n";
        
        return grasp_list;
    }
    
    Eigen::Matrix4d cam_tf_left_, cam_tf_right_; ///< camera poses
    
    /** hand search parameters */
    double finger_width_;
    double hand_outer_diameter_; ///< the maximum robot hand aperture
    double hand_depth_; ///< the finger length
    double hand_height_; ///< the hand extends plus/minus this value along the hand axis
    double init_bite_; ///< the minimum object height
    int num_threads_; ///< the number of threads used in the search
    int num_samples_; ///< the number of samples used in the search
    
    Eigen::Matrix3Xd cloud_normals_; ///< a 3xn matrix containing the normals for points in the point cloud
    Plot plot_; ///< plot object for visualization of search results
    
    bool uses_determinstic_normal_estimation_; ///< whether the normal estimation for the quadratic surface is deterministic (used for debugging)
    bool tolerant_antipodal_; ///< whether the antipodal testing uses "tolerant" thresholds
    
    bool plots_samples_; ///< whether the samples drawn from the point cloud are plotted
    bool plots_camera_sources_; ///< whether the camera source for each point in the point cloud is plotted
    bool plots_local_axes_; ///< whether the local axes estimated for each point neighborhood are plotted
    bool plots_hands_; ///< whether the grasp hypotheses are plotted
    
    double nn_radius_taubin_; ///< the radius for the neighborhood search for the quadratic surface fit
    double nn_radius_hands_; ///< the radius for the neighborhood search for the hand search
};

#endif /* HAND_SEARCH_H */
