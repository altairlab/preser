/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HANDLE_SEARCH_H_
#define HANDLE_SEARCH_H_

#include "mex.h"

#include <set>
#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>

// #include <ros/ros.h> // commented 01/05/2016

#include <agile_grasp/grasp_hypothesis.h>
#include <agile_grasp/handle.h>


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

/** HandleSearch class
 *
 * \brief Search handles given grasp hypotheses
 *
 * This class searches for handles, i.e., clusters of grasps that are geometrically aligned. It can
 * also plot the results of this search.
 *
 */
class HandleSearch
{
public:
    
    /**
     * \brief Search for handles given a list of grasp hypotheses.
     * \param hand_list the list of grasp hypotheses
     * \param min_inliers the minimum number of grasp hypothesis contained in a handle
     * \param min_length the minimum length of a handle
     * \return the list of handles found
     */
    std::vector<Handle> findHandles(const std::vector<GraspHypothesis>& hand_list, int min_inliers, double min_length)
    {
        double t0 = omp_get_wtime();
        std::vector<Handle> handle_list;
        std::vector<GraspHypothesis> reduced_hand_list(hand_list);
        
        for (int i = 0; i < hand_list.size(); i++)
        {
            if (reduced_hand_list[i].getGraspWidth() == -1)
                continue;
            
            Eigen::Vector3d iaxis = reduced_hand_list[i].getAxis();
            Eigen::Vector3d ipt = reduced_hand_list[i].getGraspBottom();
            Eigen::Vector3d inormal = reduced_hand_list[i].getApproach();
            std::vector<Eigen::Vector2d> inliers;
            
            for (int j = 0; j < hand_list.size(); j++)
            {
                if (reduced_hand_list[j].getGraspWidth() == -1)
                    continue;
                
                Eigen::Vector3d jaxis = reduced_hand_list[j].getAxis();
                Eigen::Vector3d jpt = reduced_hand_list[j].getGraspBottom();
                Eigen::Vector3d jnormal = reduced_hand_list[j].getApproach();
                
                // how good is this match?
                double dist_from_line = ((Eigen::Matrix3d::Identity(3, 3) - iaxis * iaxis.transpose()) * (jpt - ipt)).norm();
                double dist_along_line = iaxis.transpose() * (jpt - ipt);
                Eigen::Vector2d angle_axis;
                angle_axis << safeAcos(iaxis.transpose() * jaxis), M_PI - safeAcos(iaxis.transpose() * jaxis);
                double dist_angle_axis = angle_axis.minCoeff();
                double dist_from_normal = safeAcos(inormal.transpose() * jnormal);
                if (dist_from_line < 0.01 && dist_angle_axis < 0.34 && dist_from_normal < 0.34)
                {
                    Eigen::Vector2d inlier;
                    inlier << j, dist_along_line;
                    inliers.push_back(inlier);
                }
            }
            
            if (inliers.size() < min_inliers)
                continue;
            
            // shorten handle to a continuous piece
            double handle_gap_threshold = 0.02;
            std::vector<Eigen::Vector2d> inliers_list(inliers.begin(), inliers.end());
            while (!shortenHandle(inliers_list, handle_gap_threshold))
            {	};
            
            if (inliers_list.size() < min_inliers)
                continue;
            
            // find grasps farthest away from i-th grasp
            double min_dist = 10000000;
            double max_dist = -10000000;
            std::vector<int> in(inliers_list.size());
            for (int k = 0; k < inliers_list.size(); k++)
            {
                in[k] = inliers_list[k](0);
                
                if (inliers_list[k](1) < min_dist)
                    min_dist = inliers_list[k](1);
                if (inliers_list[k](1) > max_dist)
                    max_dist = inliers_list[k](1);
            }
            
            // handle found
            if (max_dist - min_dist > min_length)
            {
                handle_list.push_back(Handle(hand_list, in));
                std::cout << "handle found with " << in.size() << " inliers\n";
                
                // eliminate hands in this handle from future search
                for (int k = 0; k < in.size(); k++)
                {
                    reduced_hand_list[in[k]].setGraspWidth(-1);
                }
            }
        }
        
        std::cout << "Handle Search\n";
        std::cout << " runtime: " << omp_get_wtime() - t0 << " sec\n";
        std::cout << " " << handle_list.size() << " handles found\n";
        return handle_list;
    }
    
    
private:
    
    /**
     * \brief Shorten a handle to a continuous piece.
     *
     * This function finds continuous handles by searching for a gap that is larger than @p gap_threshold.
     *
     * \param inliers the list of grasp hypotheses that are part of the handle to be shortened
     * \param gap_threshold the maximum gap size
     * \return the shortened handle
     */
    bool shortenHandle(std::vector<Eigen::Vector2d> &inliers, double gap_threshold)
    {
        std::sort(inliers.begin(), inliers.end(), HandleSearch::LastElementComparator());
        bool is_done = true;
        
        for (int i = 0; i < inliers.size() - 1; i++)
        {
            double diff = inliers[i + 1](1) - inliers[i](1);
            if (diff > gap_threshold)
            {
                std::vector<Eigen::Vector2d> out;
                if (inliers[i](2) < 0)
                {
                    out.assign(inliers.begin() + i + 1, inliers.end());
                    is_done = false;
                }
                else
                {
                    out.assign(inliers.begin(), inliers.begin() + i);
                }
                inliers = out;
                break;
            }
        }
        
        return is_done;
    }
    
    /**
     * \brief Safe version of the acos(x) function.
     * \param x the value whose arc cosine is computed
     * \return the arc cosine of x, expressed in radians
     */
    double safeAcos(double x)
    {
        if (x < -1.0)
            x = -1.0;
        else if (x > 1.0)
            x = 1.0;
        return acos(x);
    }
    
    /**
     * \brief Comparator for equality of the first two elements of two 3D-vectors.
     */
    struct VectorFirstTwoElementsComparator
    {
        /**
         * \brief Compare the first two elements of two 3D-vectors for equality.
         * \param a the first 3D-vector to be compared
         * \param b the second 3D-vector to be compared
         * \return true if the first element of both vectors is equal or the second element of both
         * vectors is equal, false otherwise
         */
        bool operator ()(const Eigen::Vector3d& a, const Eigen::Vector3d& b)
        {
            for (int i = 0; i < 2; i++)
            {
                if (a(i) != b(i))
                {
                    return a(i) < b(i);
                }
            }
            
            return false;
        }
    };
    
    /**
     * \brief Comparator for equality of the last element of two 2D-vectors.
     */
    struct LastElementComparator
    {
        /**
         * \brief Compare the last element of two 2D-vectors for equality.
         * \param a the first 2D-vector to be compared
         * \param b the second 2D-vector to be compared
         * \return true if the last element of both vectors is equal, false otherwise
         */
        bool operator ()(const Eigen::Vector2d& a, const Eigen::Vector2d& b)
        {
            if (a(1) != b(1))
            {
                return a(1) < b(1);
            }
            
            return false;
        }
    };
};

#endif /* HANDLE_SEARCH_H_ */
