/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOCALIZATION_H_
#define LOCALIZATION_H_

#include "mex.h"
// system dependencies
#include <iostream>
#include <math.h>
#include <set>
#include <string>
#include <time.h>
#include <omp.h>                                // added 02/05/2016 : omp_get_wtime()
#include <vector>
// PCL dependencies
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>

// project dependencies
#include <agile_grasp/grasp_hypothesis.h>
#include <agile_grasp/hand_search.h>
#include <agile_grasp/handle.h>
#include <agile_grasp/handle_search.h>
#include <agile_grasp/learning.h>
#include <agile_grasp/plot.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;


/** Localization class
 *
 * \brief High level interface for the localization of grasp hypotheses and handles
 *
 * This class provides a high level interface to search for grasp hypotheses, antipodal grasps, and
 * handles. It can also preprocess the point clouds.
 *
 */
class Localization
{
public:
    
    /**
     * \brief Default Constructor.
     */
    Localization() : num_threads_(1), plotting_mode_(1), plots_camera_sources_(false), cloud_(new PointCloud)
    { }
    
    /**
     * \brief Constructor.
     * \param num_threads the number of threads to be used in the search
     * \param filter_boundaries whether grasp hypotheses that are close to the point cloud boundaries are filtered out
     * \param plots_hands whether grasp hypotheses are plotted
     */
    Localization(int num_threads, bool filters_boundaries, int plotting_mode) :
        num_threads_(num_threads), filters_boundaries_(filters_boundaries),
                plotting_mode_(plotting_mode), plots_camera_sources_(false),
                cloud_(new PointCloud)
        { }
        
        
        void leggo()
        {
            mexPrintf("Working ... \n");
        }
        
        /**
         * \brief Find handles given a list of grasp hypotheses.
         * \param hand_list the list of grasp hypotheses
         * \param min_inliers the minimum number of handle inliers
         * \param min_length the minimum length of the handle
         * \param is_plotting whether the handles are plotted
         */
        std::vector<Handle> findHandles(const std::vector<GraspHypothesis>& hand_list, int min_inliers,	double min_length)
        {
            HandleSearch handle_search;
            std::vector<Handle> handles = handle_search.findHandles(hand_list, min_inliers, min_length);
            if (plotting_mode_ == PCL_PLOTTING)
                plot_.plotHandles(handles, cloud_, "Handles");
            else if (plotting_mode_ == RVIZ_PLOTTING)
// 		plot_.plotHandlesRviz(handles, visuals_frame_);
                std::cout << " MATLAB 3 " << std::endl;
            return handles;
        }
        
        /**
         * \brief Predict antipodal grasps given a list of grasp hypotheses.
         * \param hand_list the list of grasp hypotheses
         * \param svm_filename the location and filename of the SVM
         */
        std::vector<GraspHypothesis> predictAntipodalHands(const std::vector<GraspHypothesis>& hand_list,
                const std::string& svm_filename)
        {
            double t0 = omp_get_wtime();
            std::vector<GraspHypothesis> antipodal_hands;
            Learning learn(num_threads_);
            Eigen::Matrix<double, 3, 2> cams_mat;
            cams_mat.col(0) = cam_tf_left_.block<3, 1>(0, 3);
            cams_mat.col(1) = cam_tf_right_.block<3, 1>(0, 3);
            antipodal_hands = learn.classify(hand_list, svm_filename, cams_mat);
            std::cout << " runtime: " << omp_get_wtime() - t0 << " sec\n";
            std::cout << antipodal_hands.size() << " antipodal hand configurations found\n";
            if (plotting_mode_ == PCL_PLOTTING)
                plot_.plotHands(hand_list, antipodal_hands, cloud_, "Antipodal Hands");
            else if (plotting_mode_ == RVIZ_PLOTTING)
// 		plot_.plotGraspsRviz(antipodal_hands, visuals_frame_, true);
                std::cout << " MATLAB 2 " << std::endl;
            return antipodal_hands;
        }
        
        /**
         * \brief Localize hands in a given point cloud.
         * \param cloud_in the input point cloud
         * \param indices the set of point cloud indices for which point neighborhoods are found
         * \param calculates_antipodal whether the grasp hypotheses are checked for being antipodal
         * \param uses_clustering whether clustering is used for processing the point cloud
         * \return the list of grasp hypotheses found
         */
        std::vector<GraspHypothesis> localizeHands(const PointCloud::Ptr& cloud_in, int size_left,
                const std::vector<int>& indices, bool calculates_antipodal, bool uses_clustering)
        {
            double t0 = omp_get_wtime();
            std::vector<GraspHypothesis> hand_list;
            mexPrintf("Here \n");
            if (size_left == 0 || cloud_in->size() == 0)
            {
//                 std::cout << "Input cloud is empty!\n";
                mexPrintf("Input cloud is empty! %d \n", size_left);
                hand_list.resize(0);
                return hand_list;
            }
            
            // set camera source for all points (0 = left, 1 = right)
//             std::cout << "Generating camera sources for " << cloud_in->size() << " points ...\n";
            mexPrintf("Generating camera sources for  %d  points  \n",cloud_in->size());
           Eigen::VectorXi pts_cam_source(cloud_in->size());
            if (size_left == cloud_in->size())
                pts_cam_source << Eigen::VectorXi::Zero(size_left);
            else
                pts_cam_source << Eigen::VectorXi::Zero(size_left), Eigen::VectorXi::Ones(cloud_in->size() - size_left);
            
            // remove NAN points from the cloud
            std::vector<int> nan_indices;
            pcl::removeNaNFromPointCloud(*cloud_in, *cloud_in, nan_indices);
            
            // reduce point cloud to workspace
            std::cout << "Filtering workspace ...\n";
            PointCloud::Ptr cloud(new PointCloud);
            filterWorkspace(cloud_in, pts_cam_source, cloud, pts_cam_source);
            std::cout << " " << cloud->size() << " points left\n";
            
            // store complete cloud for later plotting
            PointCloud::Ptr cloud_plot(new PointCloud);
            *cloud_plot = *cloud;
            *cloud_ = *cloud;
            
            // voxelize point cloud
            std::cout << "Voxelizing point cloud\n";
            double t1_voxels = omp_get_wtime();
            voxelizeCloud(cloud, pts_cam_source, cloud, pts_cam_source, 0.003);
            double t2_voxels = omp_get_wtime() - t1_voxels;
            std::cout << " Created " << cloud->points.size() << " voxels in " << t2_voxels << " sec\n";
            
            // plot camera source for each point in the cloud
            if (plots_camera_sources_)
                plot_.plotCameraSource(pts_cam_source, cloud);
            
            if (uses_clustering)
            {
                std::cout << "Finding point cloud clusters ... \n";
                
                // Create the segmentation object for the planar model and set all the parameters
                pcl::SACSegmentation<pcl::PointXYZ> seg;
                pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
                pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
                pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane(new pcl::PointCloud<pcl::PointXYZ>());
                seg.setOptimizeCoefficients(true);
                seg.setModelType(pcl::SACMODEL_PLANE);
                seg.setMethodType(pcl::SAC_RANSAC);
                seg.setMaxIterations(100);
                seg.setDistanceThreshold(0.01);
                
                // Segment the largest planar component from the remaining cloud
                seg.setInputCloud(cloud);
                seg.segment(*inliers, *coefficients);
                if (inliers->indices.size() == 0)
                {
                    std::cout << " Could not estimate a planar model for the given dataset." << std::endl;
                    hand_list.resize(0);
                    return hand_list;
                }
                
                std::cout << " PointCloud representing the planar component: " << inliers->indices.size()
                << " data points." << std::endl;
                
                // Extract the nonplanar inliers
                pcl::ExtractIndices<pcl::PointXYZ> extract;
                extract.setInputCloud(cloud);
                extract.setIndices(inliers);
                extract.setNegative(true);
                std::vector<int> indices_cluster;
                extract.filter(indices_cluster);
                PointCloud::Ptr cloud_cluster(new PointCloud);
                cloud_cluster->points.resize(indices_cluster.size());
                Eigen::VectorXi cluster_cam_source(indices_cluster.size());
                for (int i = 0; i < indices_cluster.size(); i++)
                {
                    cloud_cluster->points[i] = cloud->points[indices_cluster[i]];
                    cluster_cam_source[i] = pts_cam_source[indices_cluster[i]];
                }
                cloud = cloud_cluster;
                *cloud_plot = *cloud;
                std::cout << " PointCloud representing the non-planar component: " << cloud->points.size()
                << " data points." << std::endl;
            }
            
            // draw down-sampled and workspace reduced cloud
            cloud_plot = cloud;
           
            // set plotting within handle search on/off
            bool plots_hands;
            if (plotting_mode_ == PCL_PLOTTING)
                plots_hands = true;
            else
                plots_hands = false;
          
            // find hand configurations
            HandSearch hand_search(finger_width_, hand_outer_diameter_, hand_depth_, hand_height_, init_bite_, num_threads_,
                    num_samples_, plots_hands);
					
			 		
            hand_list = hand_search.findHands(cloud, pts_cam_source, indices, cloud_plot, calculates_antipodal, uses_clustering); // Sometimes CRASH,check . 11/07
             /*
            // remove hands at boundaries of workspace
            if (filters_boundaries_)
            {
                std::cout << "Filtering out hands close to workspace boundaries ...\n";
                hand_list = filterHands(hand_list);
                std::cout << " # hands left: " << hand_list.size() << "\n";
            }
            
            double t2 = omp_get_wtime();
             std::cout << "Hand localization done in " << t2 - t0 << " sec\n";
           
            if (plotting_mode_ == PCL_PLOTTING)
            {
                plot_.plotHands(hand_list, cloud_plot, "");
            }
            else if (plotting_mode_ == RVIZ_PLOTTING)
            {
			//plot_.plotGraspsRviz(hand_list, visuals_frame_);
                std::cout << " MATLAB 1 " << std::endl;
            }
*/
            return hand_list;
        }
        
        
        /**
         * \brief Localize hands given two point cloud files.
         * \param pcd_filename_left the first point cloud file location and name
         * \param pcd_filename_right the second point cloud file location and name
         * \param calculates_antipodal whether the grasp hypotheses are checked for being antipodal
         * \param uses_clustering whether clustering is used for processing the point cloud
         * \return the list of grasp hypotheses found
         */
        std::vector<GraspHypothesis> localizeHands(const std::string& pcd_filename_left,
                const std::string& pcd_filename_right, bool calculates_antipodal = false,
                bool uses_clustering = false)
        {
            std::vector<int> indices(0);
            return localizeHands(pcd_filename_left, pcd_filename_right, indices, calculates_antipodal, uses_clustering);
        }
        
        /**
         * \brief Localize hands given two point cloud files and a set of point cloud indices.
         * \param pcd_filename_left the first point cloud file location and name
         * \param pcd_filename_right the second point cloud file location and name
         * \param indices the set of point cloud indices for which point neighborhoods are found
         * \param calculates_antipodal whether the grasp hypotheses are checked for being antipodal
         * \param uses_clustering whether clustering is used for processing the point cloud
         * \return the list of grasp hypotheses found
         */
        std::vector<GraspHypothesis> localizeHands(const std::string& pcd_filename_left,
                const std::string& pcd_filename_right, const std::vector<int>& indices,
                bool calculates_antipodal =	false, bool uses_clustering = false)
        {
            double t0 = omp_get_wtime();
            
            // load point clouds
            PointCloud::Ptr cloud_left(new PointCloud);
            if (pcl::io::loadPCDFile<pcl::PointXYZ>(pcd_filename_left, *cloud_left) == -1) //* load the file
            {
                PCL_ERROR("Couldn't read pcd_filename_left file \n");
                std::vector<GraspHypothesis> hand_list(0);
                return hand_list;
            }
            if (pcd_filename_right.length() > 0)
                std::cout << "Loaded left point cloud with " << cloud_left->width * cloud_left->height << " data points.\n";
            else
                std::cout << "Loaded point cloud with " << cloud_left->width * cloud_left->height << " data points.\n";
            
            PointCloud::Ptr cloud_right(new PointCloud);
            if (pcd_filename_right.length() > 0)
            {
                if (pcl::io::loadPCDFile<pcl::PointXYZ>(pcd_filename_right, *cloud_right) == -1) //* load the file
                {
                    PCL_ERROR("Couldn't read pcd_filename_right file \n");
                    std::vector<GraspHypothesis> hand_list(0);
                    return hand_list;
                }
                std::cout << "Loaded right point cloud with " << cloud_right->width * cloud_right->height << " data points.\n";
                std::cout << "Loaded both clouds in " << omp_get_wtime() - t0 << " sec\n";
            }
            
            // concatenate point clouds
            std::cout << "Concatenating point clouds ...\n";
            PointCloud::Ptr cloud(new PointCloud);
            *cloud = *cloud_left + *cloud_right;
            
            return localizeHands(cloud, cloud_left->size(), indices, calculates_antipodal, uses_clustering);
        }
        
        /**
         * \brief Set the camera poses.
         * \param cam_tf_left the pose of the left camera
         * \param cam_tf_right the pose of the right camera
         */
        void setCameraTransforms(const Eigen::Matrix4d& cam_tf_left, const Eigen::Matrix4d& cam_tf_right)
        {
            cam_tf_left_ = cam_tf_left;
            cam_tf_right_ = cam_tf_right;
        }
        
        /**
         * \brief Return the camera pose of one given camera.
         * \param is_left true if the pose of the left camera is wanted, false if the pose of the right camera is wanted
         * \return the pose of the camera specified by @p is_left
         */
        const Eigen::Matrix4d& getCameraTransform(bool is_left)
        {
            if (is_left)
                return cam_tf_left_;
            else
                return cam_tf_right_;
        }
        
        /**
         * \brief Set the dimensions of the robot's workspace.
         * \param workspace 1x6 vector containing the robot's workspace dimensions
         */
        void setWorkspace(const Eigen::VectorXd& workspace)
        {
            workspace_ = workspace;
        }
        
        /**
         * \brief Set the number of samples to be used for the search.
         * \param num_samples the number of samples to be used for the search
         */
        void setNumSamples(int num_samples)
        {
            num_samples_ = num_samples;
        }
        
        /**
         * \brief Set the radius to be used for the point neighborhood search in the hand search.
         * \param nn_radius_hands the radius to be used for the point neighborhood search
         */
        void setNeighborhoodRadiusHands(double nn_radius_hands)
        {
            nn_radius_hands_ = nn_radius_hands;
        }
        
        /**
         * \brief Set the radius to be used for the point neighborhood search in the quadric fit.
         * \param nn_radius_hands the radius to be used for the point neighborhood search
         */
        void setNeighborhoodRadiusTaubin(double nn_radius_taubin)
        {
            nn_radius_taubin_ = nn_radius_taubin;
        }
        
        /**
         * \brief Set the finger width of the robot hand.
         * \param finger_width the finger width
         */
        void setFingerWidth(double finger_width)
        {
            finger_width_ = finger_width;
        }
        
        /**
         * \brief Set the hand depth of the robot hand.
         * \param hand_depth the hand depth of the robot hand (usually the finger length)
         */
        void setHandDepth(double hand_depth)
        {
            hand_depth_ = hand_depth;
        }
        
        /**
         * \brief Set the maximum aperture of the robot hand.
         * \param hand_outer_diameter the maximum aperture of the robot hand
         */
        void setHandOuterDiameter(double hand_outer_diameter)
        {
            hand_outer_diameter_ = hand_outer_diameter;
        }
        
        /**
         * \brief Set the initial "bite" of the robot hand (usually the minimum object "height").
         * \param init_bite the initial "bite" of the robot hand (@see FingerHand)
         */
        void setInitBite(double init_bite)
        {
            init_bite_ = init_bite;
        }
        
        /**
         * \brief Set the height of the robot hand.
         * \param hand_height the height of the robot hand, the hand extends plus/minus this value along the hand axis
         */
        void setHandHeight(double hand_height)
        {
            hand_height_ = hand_height;
        }
        
        /**
         * \brief Set the publisher for Rviz visualization, the lifetime of visual markers, and the frame associated with
         * the grasps.
         * \param node the ROS node
         * \param marker_lifetime the lifetime of each visual marker
         * \param frame the frame to which the grasps belong
         */
// 	void createVisualsPub(ros::NodeHandle& node, double marker_lifetime, const std::string& frame)
// 	{
// 		plot_.createVisualPublishers(node, marker_lifetime);
// 		visuals_frame_ = frame;
// 	}
        
        
private:
    
    /**
     * \brief Comparator for checking uniqueness of two 3D-vectors.
     */
    struct UniqueVectorComparator
    {
        /**
         * \brief Compares two 3D-vectors for uniqueness.
         * \param a the first 3D-vector
         * \param b the second 3D-vector
         * \return true if they have no equal elements, false otherwise
         */
        bool operator ()(const Eigen::Vector3i& a, const Eigen::Vector3i& b)
        {
            for (int i = 0; i < a.size(); i++)
            {
                if (a(i) != b(i))
                {
                    return a(i) < b(i);
                }
            }
            
            return false;
        }
    };
    
    /**
     * \brief Voxelize the point cloud and keep track of the camera source for each voxel.
     * \param[in] cloud_in the point cloud to be voxelized
     * \param[in] pts_cam_source_in the camera source for each point in the point cloud
     * \param[out] cloud_out the voxelized point cloud
     * \param[out] pts_cam_source_out the camera source for each point in the voxelized cloud
     * \param[in] cell_size the size of each voxel
     */
    void voxelizeCloud(const PointCloud::Ptr& cloud_in, const Eigen::VectorXi& pts_cam_source_in,
            PointCloud::Ptr& cloud_out, Eigen::VectorXi& pts_cam_source_out, double cell_size)
    {
        Eigen::Vector3d min_left, min_right;
        min_left  << 10000, 10000, 10000;
        min_right << 10000, 10000, 10000;
        Eigen::Matrix3Xd pts(3, cloud_in->points.size());
        int num_left = 0;
        int num_right = 0;
        for (int i = 0; i < cloud_in->points.size(); i++)
        {
            if (pts_cam_source_in(i) == 0)
            {
                if (cloud_in->points[i].x < min_left(0))
                    min_left(0) = cloud_in->points[i].x;
                if (cloud_in->points[i].y < min_left(1))
                    min_left(1) = cloud_in->points[i].y;
                if (cloud_in->points[i].z < min_left(2))
                    min_left(2) = cloud_in->points[i].z;
                num_left++;
            }
            else if (pts_cam_source_in(i) == 1)
            {
                if (cloud_in->points[i].x < min_right(0))
                    min_right(0) = cloud_in->points[i].x;
                if (cloud_in->points[i].y < min_right(1))
                    min_right(1) = cloud_in->points[i].y;
                if (cloud_in->points[i].z < min_right(2))
                    min_right(2) = cloud_in->points[i].z;
                num_right++;
            }
            pts.col(i) = cloud_in->points[i].getVector3fMap().cast<double>();
        }
        
        // find the cell that each point falls into
        std::set<Eigen::Vector3i, Localization::UniqueVectorComparator> bins_left;
        std::set<Eigen::Vector3i, Localization::UniqueVectorComparator> bins_right;
        int prev;
        for (int i = 0; i < pts.cols(); i++)
        {
            if (pts_cam_source_in(i) == 0)
            {
                Eigen::Vector3i v = floorVector((pts.col(i) - min_left) / cell_size);
                bins_left.insert(v);
                prev = bins_left.size();
            }
            else if (pts_cam_source_in(i) == 1)
            {
                Eigen::Vector3i v = floorVector((pts.col(i) - min_right) / cell_size);
                bins_right.insert(v);
            }
        }
        
        // calculate the cell values
        Eigen::Matrix3Xd voxels_left(3, bins_left.size());
        Eigen::Matrix3Xd voxels_right(3, bins_right.size());
        int i = 0;
        for (std::set<Eigen::Vector3i, Localization::UniqueVectorComparator>::iterator it = bins_left.begin();
        it != bins_left.end(); it++)
        {
            voxels_left.col(i) = (*it).cast<double>();
            i++;
        }
        i = 0;
        for (std::set<Eigen::Vector3i, Localization::UniqueVectorComparator>::iterator it = bins_right.begin();
        it != bins_right.end(); it++)
        {
            voxels_right.col(i) = (*it).cast<double>();
            i++;
        }
        
        voxels_left.row(0) = voxels_left.row(0) * cell_size
                + Eigen::VectorXd::Ones(voxels_left.cols()) * min_left(0);
        voxels_left.row(1) = voxels_left.row(1) * cell_size
                + Eigen::VectorXd::Ones(voxels_left.cols()) * min_left(1);
        voxels_left.row(2) = voxels_left.row(2) * cell_size
                + Eigen::VectorXd::Ones(voxels_left.cols()) * min_left(2);
        voxels_right.row(0) = voxels_right.row(0) * cell_size
                + Eigen::VectorXd::Ones(voxels_right.cols()) * min_right(0);
        voxels_right.row(1) = voxels_right.row(1) * cell_size
                + Eigen::VectorXd::Ones(voxels_right.cols()) * min_right(1);
        voxels_right.row(2) = voxels_right.row(2) * cell_size
                + Eigen::VectorXd::Ones(voxels_right.cols()) * min_right(2);
        
        PointCloud::Ptr cloud(new PointCloud);
        cloud->resize(bins_left.size() + bins_right.size());
        Eigen::VectorXi pts_cam_source(bins_left.size() + bins_right.size());
        for (int i = 0; i < bins_left.size(); i++)
        {
            pcl::PointXYZ p;
            p.x = voxels_left(0, i);
            p.y = voxels_left(1, i);
            p.z = voxels_left(2, i);
            cloud->points[i] = p;
            pts_cam_source(i) = 0;
        }
        for (int i = 0; i < bins_right.size(); i++)
        {
            pcl::PointXYZ p;
            p.x = voxels_right(0, i);
            p.y = voxels_right(1, i);
            p.z = voxels_right(2, i);
            cloud->points[bins_left.size() + i] = p;
            pts_cam_source(bins_left.size() + i) = 1;
        }
        
        cloud_out = cloud;
        pts_cam_source_out = pts_cam_source;
    }
    
    /**
     * \brief Filter out points in the point cloud that lie outside the workspace dimensions and keep
     * track of the camera source for each point that is not filtered out.
     * \param[in] cloud_in the point cloud to be filtered
     * \param[in] pts_cam_source_in the camera source for each point in the point cloud
     * \param[out] cloud_out the filtered point cloud
     * \param[out] pts_cam_source_out the camera source for each point in the filtered cloud
     */
    void filterWorkspace(const PointCloud::Ptr& cloud_in, const Eigen::VectorXi& pts_cam_source_in,
            PointCloud::Ptr& cloud_out, Eigen::VectorXi& pts_cam_source_out)
    {
        std::vector<int> indices(cloud_in->points.size());
	int c = 0;
	PointCloud::Ptr cloud(new PointCloud);
//  std::cout << "workspace_: " << workspace_.transpose() << "\n";

	for (int i = 0; i < cloud_in->points.size(); i++)
	{
//    std::cout << "i: " << i << "\n";
		const pcl::PointXYZ& p = cloud_in->points[i];
		if (p.x >= workspace_(0) && p.x <= workspace_(1) && p.y >= workspace_(2) && p.y <= workspace_(3)
				&& p.z >= workspace_(4) && p.z <= workspace_(5))
		{
			cloud->points.push_back(p);
			indices[c] = i;
			c++;
		}
	}

	Eigen::VectorXi pts_cam_source(c);
	for (int i = 0; i < pts_cam_source.size(); i++)
	{
		pts_cam_source(i) = pts_cam_source_in(indices[i]);
	}

	cloud_out = cloud;
	pts_cam_source_out = pts_cam_source;
    }
    
    /**
     * \brief Filter out grasp hypotheses that are close to the workspace boundaries.
     * \param hand_list the list of grasp hypotheses to be filtered
     * \return the list of grasp hypotheses that are not filtered out
     */
    std::vector<GraspHypothesis> filterHands(const std::vector<GraspHypothesis>& hand_list)
    {
        const double MIN_DIST = 0.02;
        
        std::vector<GraspHypothesis> filtered_hand_list;
        
        for (int i = 0; i < hand_list.size(); i++)
        {
            const Eigen::Vector3d& center = hand_list[i].getGraspSurface();
            int k;
            for (k = 0; k < workspace_.size(); k++)
            {
                if (fabs((center(floor(k / 2.0)) - workspace_(k))) < MIN_DIST)
                {
                    break;
                }
            }
            if (k == workspace_.size())
            {
                filtered_hand_list.push_back(hand_list[i]);
            }
        }
        
        return filtered_hand_list;
    }
    
    
    /**
     * \brief Round a 3D-vector down to the closest, smaller integers.
     * \param a the 3D-vector to be rounded down
     * \return the rounded down 3D-vector
     */
    Eigen::Vector3i floorVector(const Eigen::Vector3d& a)
    {
        Eigen::Vector3i b;
        b << floor(a(0)), floor(a(1)), floor(a(2));
        return b;
    }
    
    Plot plot_; ///< the plot object
    Eigen::Matrix4d cam_tf_left_, cam_tf_right_; ///< the camera poses
    Eigen::VectorXd workspace_; ///< the robot's workspace dimensions
    Eigen::Matrix3Xd cloud_normals_; ///< the normals for each point in the point cloud
    PointCloud::Ptr cloud_; ///< the input point cloud
    int num_threads_; ///< the number of CPU threads used in the search
    int num_samples_; ///< the number of samples used in the search
    double nn_radius_taubin_; ///< the radius of the neighborhood search used in the grasp hypothesis search
    double nn_radius_hands_; ///< the radius of the neighborhood search used in the quadric fit
    double finger_width_; ///< width of the fingers
    double hand_outer_diameter_; ///< maximum hand aperture
    double hand_depth_; ///< hand depth (finger length)
    double hand_height_; ///< hand height
    double init_bite_; ///< initial bite
    bool plots_camera_sources_; ///< whether the camera source is plotted for each point in the point cloud
    bool filters_boundaries_; ///< whether grasp hypotheses close to the workspace boundaries are filtered out
    int plotting_mode_; ///< what plotting mode is used
    std::string visuals_frame_; ///< visualization frame for Rviz
    
    /** constants for plotting modes */
    static const int NO_PLOTTING = 0; ///< no plotting
    static const int PCL_PLOTTING = 1; ///< plotting in PCL
    static const int RVIZ_PLOTTING = 2; ///< plotting in Rviz
};

#endif
