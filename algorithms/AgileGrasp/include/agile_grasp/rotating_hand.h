/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ROTATING_HAND_H
#define ROTATING_HAND_H

#include <Eigen/Dense>
#include <iostream>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <vector>

#include <agile_grasp/antipodal.h>
#include <agile_grasp/finger_hand.h>
#include <agile_grasp/grasp_hypothesis.h>

/** RotatingHand class
 *
 * \brief Calculate collision-free grasp hypotheses that fit a point neighborhood.
 * 
 * This class calculates a set of collision-free grasp hypotheses that fit a point neighborhood. 
 * The point neighborhood is first transformed into the robot hand frame, then rotated into one 
 * of the evaluated hand orientations, and finally tested for finger placements. A grasp 
 * hypothesis can also be tested for being antipodal with this class.
 * 
*/
class RotatingHand
{
public:

  /**
	 * \brief Default constructor.
	*/
  RotatingHand() : tolerant_antipodal_(true), cam_source_(-1) { }

	/**
	 * \brief Constructor.
	 * \param tolerant_antipodal whether the antipodal quality estimation uses "tolerant" thresholds
	 * \param the camera source of the sample for which the point neighborhood was found
	*/
	RotatingHand(bool tolerant_antipodal, int cam_source) 
    : tolerant_antipodal_(tolerant_antipodal), cam_source_(cam_source)
	{	}

	/**
	 * \brief Constructor.
	 * \param camera_origin_left the position of the left camera
	 * \param camera_origin_right the position of the right camera
	 * \param finger_hand the FingerHand object to be used
	 * \param tolerant_antipodal whether the antipodal quality estimation uses "tolerant" thresholds
	 * \param the camera source of the sample for which the point neighborhood was found
	 */
// 	RotatingHand(const Eigen::VectorXd& camera_origin_left, const Eigen::VectorXd& camera_origin_right,
// 			const FingerHand& finger_hand, bool tolerant_antipodal, int cam_source);

    RotatingHand(const Eigen::VectorXd& camera_origin_left,
	const Eigen::VectorXd& camera_origin_right, const FingerHand& finger_hand, bool tolerant_antipodal,
	int cam_source) :
		finger_hand_(finger_hand), tolerant_antipodal_(tolerant_antipodal), cam_source_(cam_source)
{
	cams_.col(0) = camera_origin_left;
	cams_.col(1) = camera_origin_right;

	// generate hand orientations
	int num_orientations = 8;
	Eigen::VectorXd angles = Eigen::VectorXd::LinSpaced(num_orientations + 1, -1.0 * M_PI, M_PI);
	angles_ = angles.block(0, 0, 1, num_orientations);
}
	/**
	 * \brief Transforms a set of points into the hand frame, defined by normal and axis.
	 * \param points the set of points to be transformed
	 * \param normal the normal, used as one axis in the hand frame
	 * \param axis the axis, used as another axis in the hand frame
	 * \param normals the set of normals for the set of points
	 * \param points_cam_source the set of camera sources for the set of points
	 * \param hand_height the hand height, the hand extends plus/minus this value along the hand axis
	 */
	void transformPoints(const Eigen::Matrix3Xd& points, const Eigen::Vector3d& normal,
			const Eigen::Vector3d& axis, const Eigen::Matrix3Xd& normals, const Eigen::VectorXi& points_cam_source,
			double hand_height)
    {
        // rotate points into hand frame
	hand_axis_ = axis;
	frame_ << normal, normal.cross(axis), axis;
	points_ = frame_.transpose() * points;
//  std::cout << "transformPoints(): " << std::endl;
//  std::cout << " points: " << points.rows() << " x " << points.cols() << std::endl;
//  std::cout << " frame_: " << frame_.rows() << " x " << frame_.cols() << std::endl;
//  std::cout << frame_ << std::endl;
//  std::cout << " points_: " << points_.rows() << " x " << points_.cols() << std::endl;

	normals_ = frame_.transpose() * normals;
	points_cam_source_ = points_cam_source;

	// crop points by hand_height
	std::vector<int> indices(points_.size());
	int k = 0;
//  std::cout << "h.pts: " << points_.cols() << ", -1.0 * hand_height: " << -1.0 * hand_height << "\n";
	for (int i = 0; i < points_.cols(); i++)
	{
//    std::cout << "i: " << i << " " << points_.col(i).transpose() << "\n";

		if (points_(2, i) > -1.0 * hand_height && points_(2, i) < hand_height)
		{
//      std::cout << " hand_height i: " << i << " " << points_(2, i) << "\n";
//      indices.push_back(i);
			indices[k] = i;
			k++;
		}
	}
//  indices[k+1] = -1;
//
////  std::cout << "indices.size(): " << indices.size() << std::endl;
//
////  for (int i = 0; i < indices.size(); i++)
////    std::cout << indices[i] << " ";
////  std::cout << "\n";

	Eigen::Matrix3Xd points_cropped(3, k);
	Eigen::Matrix3Xd normals_cropped(3, k);
	Eigen::VectorXi points_cam_source_cropped(k);
	for (int i = 0; i < k; i++)
	{
		points_cropped.col(i) = points_.col(indices[i]);
		normals_cropped.col(i) = normals_.col(indices[i]);
		points_cam_source_cropped(i) = points_cam_source_(indices[i]);
	}

//  std::cout << normals_cropped.col(0) << std::endl;

	points_ = points_cropped;
	normals_ = normals_cropped;
	points_cam_source_ = points_cam_source_cropped;
    }
	
	/**
	 * \brief Evaluate which hand orientations and which finger placements lead to a valid grasp.
	 * \param init_bite the minimum object height
	 * \param sample the sample for which the point neighborhood was found
	 * \param use_antipodal whether the hand orientations are checked for antipodal grasps
	 */
	std::vector<GraspHypothesis> evaluateHand(double init_bite, const Eigen::Vector3d& sample, bool use_antipodal)
    {
        // initialize hands to zero
	int n = angles_.size();

	std::vector<GraspHypothesis> grasp_list;

	for (int i = 0; i < n; i++)
	{
		// rotate points into <angles_(i)> orientation
		Eigen::Matrix3d rot;
		rot << cos(angles_(i)), -1.0 * sin(angles_(i)), 0.0, sin(angles_(i)), cos(angles_(i)), 0.0, 0.0, 0.0, 1.0;
		Eigen::Matrix3Xd points_rot = rot * points_;
		Eigen::Matrix3Xd normals_rot = rot * normals_;
		Eigen::Vector3d x, y;
		x << 1.0, 0.0, 0.0;
		y << 0.0, 1.0, 0.0;
		Eigen::Vector3d approach = frame_ * rot.transpose() * y;

		// reject hand if it is not within 90 degrees of one of the camera sources
		if (approach.dot(cams_.col(0)) > 0 && approach.dot(cams_.col(1)) > 0)
		{
			continue;
		}

		Eigen::Vector3d binormal = frame_ * rot.transpose() * x;

		// calculate free hand placements
		finger_hand_.setPoints(points_rot);
		finger_hand_.evaluateFingers(init_bite);
		finger_hand_.evaluateHand();

		if (finger_hand_.getHand().sum() > 0)
		{
			// find deepest hand
			finger_hand_.deepenHand(init_bite, finger_hand_.getHandDepth());
			finger_hand_.evaluateGraspParameters(init_bite);

			// save grasp parameters
			Eigen::Vector3d surface, bottom;
			surface << finger_hand_.getGraspSurface(), 0.0;
			bottom << finger_hand_.getGraspBottom(), 0.0;
			surface = frame_ * rot.transpose() * surface;
			bottom = frame_ * rot.transpose() * bottom;

			// save data for learning
			std::vector<int> points_in_box_indices;
			for (int j = 0; j < points_rot.cols(); j++)
			{
				if (points_rot(1, j) < finger_hand_.getBackOfHand() + finger_hand_.getHandDepth())
					points_in_box_indices.push_back(j);
			}

			Eigen::Matrix3Xd points_in_box(3, points_in_box_indices.size());
			Eigen::Matrix3Xd normals_in_box(3, points_in_box_indices.size());
			Eigen::VectorXi cam_source_in_box(points_in_box_indices.size());

			for (int j = 0; j < points_in_box_indices.size(); j++)
			{
				points_in_box.col(j) = points_rot.col(points_in_box_indices[j]) - surface;
				normals_in_box.col(j) = normals_rot.col(points_in_box_indices[j]);
				cam_source_in_box(j) = points_cam_source_(points_in_box_indices[j]);
			}

			std::vector<int> indices_cam1;
			std::vector<int> indices_cam2;
			for (int j = 0; j < points_in_box.cols(); j++)
			{
				if (cam_source_in_box(j) == 0)
					indices_cam1.push_back(j);
				else if (cam_source_in_box(j) == 1)
					indices_cam2.push_back(j);
			}

			surface += sample;
			bottom += sample;

			GraspHypothesis grasp(hand_axis_, approach, binormal, bottom, surface, finger_hand_.getGraspWidth(),
				points_in_box, indices_cam1, indices_cam2, cam_source_);

			if (use_antipodal) // if we need to evaluate whether the grasp is antipodal
			{
				Antipodal antipodal(normals_in_box);
				int antipodal_type = antipodal.evaluateGrasp(20, 20);
				if (antipodal_type == Antipodal::FULL_GRASP)
				{
					grasp.setHalfAntipodal(true);
					grasp.setFullAntipodal(true);
				}
				else if (antipodal_type == Antipodal::HALF_GRASP)
					grasp.setHalfAntipodal(true);
			}

			grasp_list.push_back(grasp);
		}
	}

	return grasp_list;
    }
    
			  
  /**
   * \brief Return the camera positions.
   * \return the 3x2 matrix containing the positions of the cameras
  */
  const Eigen::Matrix<double, 3, 2>& getCams() const 
  {
    return cams_;
  }
  
  /**
   * \brief Return the camera source for each point in the point neighborhood.
   * \return the 1xn matrix containing the camera source for each point in the point neighborhood
  */
  const Eigen::VectorXi& getPointsCamSource() const 
  {
    return points_cam_source_;
  }
  
  /**
   * \brief Return the camera source of the sample for which the point neighborhood was calculated.
   * \return the camera source of the sample
  */
  int getCamSource() const
  {
    return cam_source_;
  }


private:

  int cam_source_; ///< the camera source of the sample
	Eigen::Matrix<double, 3, 2> cams_; ///< the camera positions
	Eigen::VectorXd angles_; ///< the hand orientations that are evaluated
	Eigen::Matrix3Xd points_; ///< the points in the point neighborhood
	Eigen::Vector3d hand_axis_; ///< the robot hand axis for the point neighborhood
	Eigen::Matrix3d frame_; ///< the robot hand frame for the point neighborhood
	Eigen::Matrix3Xd normals_; ///< the normals for each point in the point neighborhood
	Eigen::VectorXi points_cam_source_;	 ///<	the camera source for each point in the point neighborhood
  FingerHand finger_hand_; ///< the finger hand object
  
  /** parameters */
  bool tolerant_antipodal_; ///< whether the antipodal testing uses "tolerant" thresholds
};

#endif
