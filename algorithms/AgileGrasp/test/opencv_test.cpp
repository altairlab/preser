
#include "mex.h"

// #ifndef HAS_OPENCV
// #define HAS_OPENCV
// #endif

#include <omp.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <string>

using namespace cv;
using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{


  double x = omp_get_wtime();

  mexPrintf("%f\n", x);

  std::cout << " --" << std::endl;
  
    char *file_name;
    file_name = mxArrayToString(prhs[0]); // prendo in input il file .pcd
  
    Mat image;
    image = imread(file_name, IMREAD_COLOR); // Read the file

    if( image.empty() )                      // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        mexPrintf("fail");
    }

    namedWindow( "Display window", WINDOW_AUTOSIZE ); // Create a window for display.
    imshow( "Display window", image );                // Show our image inside it.

    waitKey(0); // Wait for a keystroke in the window
    
    
}