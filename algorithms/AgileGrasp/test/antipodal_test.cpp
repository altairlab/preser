#include "mex.h"

#include <agile_grasp/localization.h>

#include <Eigen/Dense>

#include <stdlib.h>



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    char* file_name;
    file_name = mxArrayToString(prhs[0]); // prendo in input il file .pcd
    
    std::string pcd_file_name;
    std::string file_name_left;
    std::string file_name_right;
    
    pcd_file_name = std::string(file_name);
    
    if (pcd_file_name.find(".pcd") == std::string::npos)
    {
        file_name_left = pcd_file_name + "l_reg.pcd";
        file_name_right = pcd_file_name + "r_reg.pcd";
    }
    else
    {
        file_name_left = pcd_file_name;
        file_name_right = "";
    }
    
    mexPrintf("pcd_file_name %s \n" , pcd_file_name);
    // read SVM filename from command line
    std::string svm_file_name = mxArrayToString(prhs[1]);
    
    mexPrintf("svm_file_name %s \n" , svm_file_name);
    
    /* read number of samples, number of threads and min handle inliers
     * from command line */
    double *num_samples; // 400 default
    num_samples = mxGetPr(prhs[2]);
    
    double *num_threads; // 1 default
    num_threads = mxGetPr(prhs[3]);
    

    double taubin_radius = 0.03;
    double hand_radius = 0.08;

    Eigen::Matrix4d base_tf, sqrt_tf;

    base_tf << 0,  0.445417,  0.895323,   0.21,
               1,         0,         0,  -0.02,
               0,  0.895323, -0.445417,   0.24,
               0,         0,         0,   1;

    sqrt_tf << 0.9366,  -0.0162,  0.3500,  -0.2863,
               0.0151,   0.9999,  0.0058,   0.0058,
              -0.3501,  -0.0002,  0.9367,   0.0554,
                    0,        0,       0,        1;

    Eigen::VectorXd workspace(6);
    workspace << 0.4, 0.7, -0.02, 0.06, -0.2, 10;

    Localization loc((int)*num_threads, false, true);
    loc.setCameraTransforms(base_tf * sqrt_tf.inverse(), base_tf * sqrt_tf);
    loc.setWorkspace(workspace);
    loc.setNumSamples((int)*num_samples);
    loc.setNeighborhoodRadiusTaubin(taubin_radius);
    loc.setNeighborhoodRadiusHands(hand_radius);
    loc.setFingerWidth(0.01);
    loc.setHandOuterDiameter(0.09);
    loc.setHandDepth(0.06);
    loc.setInitBite(0.01);
    loc.setHandHeight(0.02);
    std::cout << "Localizing hands ...\n";

    std::vector<int> indices(5);
    indices[0] = 731;
    indices[1] = 4507;
    indices[2] = 4445;
    indices[3] = 2254;
    indices[4] = 3716;
    loc.localizeHands(file_name_left, file_name_right, indices, true);

		// std::vector<int> indices(0);
		// loc.localizeHands(file_name_left, file_name_right, indices, "". true);

		// loc.voxelizeCloud(file_name_left, file_name_right);
// 
//   
//   std::cout << "No PCD filename given!\n";
//   std::cout << "Usage: test_local_axes filename [num_samples]\n";

}
