 #include "mex.h"
 #include "blas.h"
 #include "lapack.h"
 #include <cstddef>

 /* Prototype for the DGEMM routine */
//  extern void dgemm(char*, char*, int*, int*, int*, double*, 
//                    double*, int*, double*, int*, double*, double*, int*);
//  
//  extern "C" void dggev(const char* JOBVL, const char* JOBVR, const int* N, const double* A, const int* LDA,
//         const double* B, const int* LDB, double* ALPHAR, double* ALPHAI, double* BETA, double* VL,
//         const int* LDVL, double* VR, const int* LDVR, double* WORK, const int* LWORK, int* INFO);

 void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
 {
   double *A, *B, *C, one = 1.0, zero = 0.0;
   int m,n,p;
   char *chn = "N";
   A = mxGetPr(prhs[0]);
   B = mxGetPr(prhs[1]);
   m = mxGetM(prhs[0]);
   p = mxGetN(prhs[0]);
   n = mxGetN(prhs[1]);
   if (p != mxGetM(prhs[1])) {
     mexErrMsgTxt("Inner dimensions of matrix multiply do not match");
   }
   plhs[0] = mxCreateDoubleMatrix(m, n, mxREAL);
   C = mxGetPr(plhs[0]);
   /* Pass all arguments to Fortran by reference */
  ptrdiff_t new_m =  ptrdiff_t(m);
  ptrdiff_t new_n =  ptrdiff_t(n);
  ptrdiff_t new_p =  ptrdiff_t(p);
  
   dgemm (chn, chn, &new_m, &new_n, &new_p, &one, A, &new_m, B, &new_p, &zero, C, &new_m);
 }