#include "mex.h" 
#include <string>
#include <vector>
#define _CRT_SECURE_NO_DEPRECATE

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/console/print.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <flann/flann.h>
#include <flann/io/hdf5.h>
#include <boost/filesystem.hpp>
//#include <H5Fpublic.h>    // per la H5Fopen, non necessario atm


using namespace std;
typedef std::pair<std::string, std::vector<float> > vfh_model;

/** \brief Carico un nD histogram, come una VFH Signature
* \param filename : nome del file di input
* \param vfh : il modello VFH risultante
*/
bool
loadHist(char *filename, vfh_model &vfh)
{
	int vfh_idx;
	// carico il file come PCD
	try
	{
		pcl::PCLPointCloud2 cloud;
		int version;
		Eigen::Vector4f origin;
		Eigen::Quaternionf orientation;
		pcl::PCDReader r;
		int type; unsigned int idx;
		r.readHeader(filename, cloud, origin, orientation, version, type, idx);

		vfh_idx = pcl::getFieldIndex(cloud, "vfh");
		if (vfh_idx == -1)
			return (false);
		if ((int)cloud.width * cloud.height != 1)
			return (false);
	}
	catch (const pcl::InvalidConversionException&)
	{
		return (false);
	}

	// Consideriamo una VFH Signature come una singola PointCloud 
	pcl::PointCloud <pcl::VFHSignature308> point;
	pcl::io::loadPCDFile(filename, point);
	vfh.second.resize(308);

	std::vector <pcl::PCLPointField> fields;
	getFieldIndex(point, "vfh", fields);

	for (size_t i = 0; i < fields[vfh_idx].count; ++i)
	{
		vfh.second[i] = point.points[0].histogram[i];
	}
	vfh.first =filename;
	return (true);
}

/** \brief Ricerca del knn pi� vicino
* \param index : l'albero
* \param model : query model
* \param k = numero di nn da ricercare
* \param indices : indice del nn risultante
* \param distances : distanza di ritorno
*/
inline void
nearestKSearch(flann::Index<flann::ChiSquareDistance<float> > &index, const vfh_model &model,
int k, flann::Matrix<int> &indices, flann::Matrix<float> &distances)
{
    if ( model.second.size() != 0 ) 
    {
	// Query point
    flann::Matrix<float> p = flann::Matrix<float>(new float[model.second.size()], 1, model.second.size());
	memcpy(&p.ptr()[0], &model.second[0], p.cols * p.rows * sizeof(float));
    indices = flann::Matrix<int>(new int[k], 1, k);
   	distances = flann::Matrix<float>(new float[k], 1, k);
    index.knnSearch(p, indices, distances, k, flann::SearchParams(512)); // crash here  ... 11/9
    delete[] p.ptr();
    }
    else
    {
        mexPrintf("[ERROR] on Loading model \n");
    }
}



/** \brief Carico la lista dei nomi dei modelli , da un ASCII FILE
* \param models : nomi della lista 
* \param filename : input filaname
*/
bool
loadFileList(std::vector<vfh_model> &models, const std::string &filename)
{
	ifstream fs;
	fs.open(filename.c_str());
	if (!fs.is_open() || fs.fail())
		return (false);

	std::string line;
	while (!fs.eof())
	{
		getline(fs, line);
		if (line.empty())
			continue;
		vfh_model m;
		m.first = line;
		models.push_back(m);
	}
	fs.close();
	return (true);
}
/////////////end functions

void mexFunction( int nlhs, mxArray *plhs[], 
                                  int nrhs, const mxArray *prhs[]) 
{ 
    
    double* fk; // numero di nn
    fk = mxGetPr(prhs[0]);  //valore da matlab
    
    double* thresh; // threshold della distanza
    thresh = mxGetPr(prhs[1]);   //valore da matlab
    
    std::string extension(".pcd");
	transform(extension.begin(), extension.end(), extension.begin(), (int(*)(int))tolower);

    char* fileName;
    fileName = mxArrayToString(prhs[2]); // prendo in input il file .pcd
    mexPrintf(" filename is %s \n",fileName);

    vfh_model histogram;
    if (!loadHist(fileName, histogram))
	{
        mexPrintf("Cannot load test file \n");
    }
    
    // nomi dei file dove sono salvate le informazioni del dataset
    string kdtree_idx_file_name = "preser_kdtree.idx";
	string training_data_h5_file_name = "preser_training_data.h5";
	string training_data_list_file_name = "preser_training_data.list";

    int k = (int)*fk; // cast necessario, matlab prende double, ma mi serve int! 
//     mexPrintf(" k - value :  %d \n",k);
    std::vector<vfh_model> models;
   	flann::Matrix<int> k_indices;
	flann::Matrix<float> k_distances;
	flann::Matrix<float> data;

	// Check if the data has already been saved to disk
	if (!boost::filesystem::exists("preser_training_data.h5") || !boost::filesystem::exists("preser_training_data.list"))
	{  
        mexPrintf("Could not find training data models files %s and %s!\n",
			training_data_h5_file_name.c_str(), training_data_list_file_name.c_str());
	}
	else
	{   

		loadFileList(models, training_data_list_file_name);

		flann::load_from_file(data, training_data_h5_file_name, "training_data");  

     //   mexPrintf("Training data found. Loaded %d VFH models from %s/%s.\n",
 	 //		(int)data.rows, training_data_h5_file_name.c_str(), training_data_list_file_name.c_str());
	}
       
	// Check if the tree index has already been saved to disk
	if (!boost::filesystem::exists(kdtree_idx_file_name))
	{
		mexPrintf("Could not find kd-tree index in file %s!", kdtree_idx_file_name.c_str());
	}
	else
	{
 		flann::Index<flann::ChiSquareDistance<float> > index(data, flann::SavedIndexParams("preser_kdtree.idx")); 
 		index.buildIndex();
   		nearestKSearch(index, histogram, k, k_indices, k_distances); 
	}
    
    mexPrintf(" *******END********  \n");
    mexPrintf("The closest %d neighbors for %s are:\n", k, fileName);
    mexPrintf(" *******END********  \n"); 
    
    
//      -----------------------------------------------------------------------------
 /*
     double* OutValues;
     plhs[0]=mxCreateDoubleMatrix(k,2,mxREAL); 
     // Get pointer to the return argument 
    OutValues = mxGetPr(plhs[0]); 
   */  
    
    for (int i = 0; i < k; ++i) 
    {
		mexPrintf("    %d - %s (%d) with a distance of: %f\n",
		i, models.at(k_indices[0][i]).first.c_str(), k_indices[0][i], k_distances[0][i]);
      
//         OutValues[i] =  (double)k_distances[0][i];
//         OutValues[i+k] =  i+k;
// 
//     OutValues[0] = 1;
//     OutValues[1] = 2;
//     OutValues[33] = 20;
//     OutValues[65] = 65;
     
    }
    
    
     double* OutValues;
     plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL); 
     OutValues = mxGetPr(plhs[0]); 
     OutValues[0] =  (double)k_distances[0][0];
     
     plhs[1] = mxCreateString(models.at(k_indices[0][0]).first.c_str());
     mexPrintf("-----------------------\n");
     mexPrintf("HISTOGRAM MATCH WITH \n %s ,\n  with DISTANCE : %d \n",models.at(k_indices[0][0]).first.c_str(),(double)k_distances[0][0]);
     mexPrintf("-----------------------\n");
     
         
     // stampo la point cloud relativa al _vfh
     std::string cloud_name = models.at(k_indices[0][0]).first; 
     boost::replace_last(cloud_name, "_vfh", "");
     mexPrintf(" la cloud �  :  %s \n", cloud_name.c_str());
     
     //-------------------------------------------------------------------------- [NAME SELECTION] 
//     std::string::size_type n,n1;
//     std::string const s = models.at(k_indices[0][0]).first;
//    // mexPrintf("STRING IS %s \n", s.c_str());
//     
//     n = s.find("\\data");  //mexPrintf("N-- %lu : " , n);   
//     //mexPrintf("sub string %s \n" , s.substr(n).c_str());
//     
//     std::string substring,final_substring;
//     substring = s.substr(n);
//     std::string const s1 = substring;// mexPrintf("STRING NOW IS %s \n", s1.c_str());
// //     n1 = s1.find("\\Mug") ;//  mexPrintf("N1 -- %lu : " , n1);
// //     final_substring = s1.substr(n1,n1);
// //     mexPrintf("\n2 The object is a %s !  \n" , final_substring.c_str());
// //     
// 
//     if(n1 = s1.find("\\Cup") != std::string::npos) {//  mexPrintf("N1 -- %lu : " , n1);
//       n1 = s1.find("\\Cup") ; final_substring = s1.substr(n1,n1);
//       mexPrintf("\n1 The object is a %s !  \n" , final_substring.c_str());
//     }
//     if(n1 = s1.find("\\Mug") != std::string::npos) {//  mexPrintf("N1 -- %lu : " , n1);
//       n1 = s1.find("\\Mug") ; final_substring = s1.substr(n1,n1);
//       mexPrintf("\n2 The object is a %s !  \n" , final_substring.c_str());
//     }
//     if(n1 = s1.find("\\Pyramid") != std::string::npos) {//  mexPrintf("N1 -- %lu : " , n1);
//       n1 = s1.find("\\Pyramid") ; final_substring = s1.substr(n1,n1);
//       mexPrintf("\n3 The object is a %s !  \n" , final_substring.c_str());
//     }

}     

