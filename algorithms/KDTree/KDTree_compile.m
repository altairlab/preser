% KDTree 
% Compile 
% USAGE :
%                   database_dir = [pwd '/database']
%                   buildTree(database_dir)

% You need a Environment variable :
% Called PCL_ROOT , it contains PCL root directory.
% Called HDF5_ROOT , it contains HDF5 root directory.


PCL_ROOT = getenv('PCL_ROOT');
HDF5_ROOT = getenv('HDF5_ROOT');

% Add Version of HDF5 library. Matlab2015a , has 1.8.12 version
HDF5_ROOT = [HDF5_ROOT '/1.8.12'];

% loadlibrary([HDF5_ROOT '/bin/hdf5_hl.dll'],[HDF5_ROOT '/include/hdf5_hl.h'])

IPath_1 = ['-I',PCL_ROOT,'/include/pcl-1.7'];
IPath_2=['-I',PCL_ROOT,'/3rdParty/Boost/include/boost-1_57'];
IPath_3=['-I',PCL_ROOT,'/3rdParty/Eigen/eigen3'];
IPath_4=['-I',PCL_ROOT,'/3rdParty/FLANN/include'];
IPath_5=['-I',PCL_ROOT,'/3rdParty/VTK/include/vtk-6.2'];
IPath_6=['-I',HDF5_ROOT,'/include'];

%LIB
LPath_1 = ['-L',fullfile(PCL_ROOT, 'lib')];  
LPath_2 = ['-L',PCL_ROOT,'/3rdParty/Boost/lib'];
LPath_3 = ['-L',PCL_ROOT,'/3rdParty/FLANN/lib'];
LPath_4 = ['-L',PCL_ROOT,'/3rdParty/Qhull/lib'];
LPath_5 = ['-L',PCL_ROOT,'/3rdParty/VTK/lib'];
LPath_6 = ['-L',HDF5_ROOT,'/lib'];


lPath_1 = ['-l','pcl_features_release.lib'];
lPath_2 = ['-l','pcl_io_release.lib'];
lPath_3 = ['-l','pcl_common_release.lib'];
lPath_4 = ['-l','pcl_kdtree_release.lib'];
lPath_5 = ['-l','pcl_search_release.lib'];

lhdf5lib = ['-l','hdf5.lib'];
lhdf5lib1 = ['-l','hdf5_cpp.lib'];
lhdf5lib2 = ['-l','hdf5_f90cstub.lib'];
lhdf5lib3 = ['-l','hdf5_fortran.lib'];
lhdf5lib4 = ['-l','hdf5_hl.lib'];
lhdf5lib5 = ['-l','hdf5_hl_cpp.lib'];
lhdf5lib6 = ['-l','hdf5_hl_f90cstub.lib'];
lhdf5lib7 = ['-l','hdf5_hl_fortran.lib'];
lhdf5lib8 = ['-l','szip.lib'];
lhdf5lib9 = ['-l','zlib.lib'];

% -largeArrayDims 
mex('buildTree.cpp','-g',lhdf5lib,lhdf5lib1,lhdf5lib2,lhdf5lib3,lhdf5lib4,...
                        lhdf5lib5,lhdf5lib6,lhdf5lib7,lhdf5lib8,lhdf5lib9,...
                        IPath_1,IPath_2,IPath_3,IPath_4,IPath_5,IPath_6,...
                        LPath_1,LPath_2,LPath_3,LPath_4,LPath_5,LPath_6,...
                        lPath_1,lPath_2,lPath_3,lPath_4,lPath_5);
