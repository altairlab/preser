


PCL_ROOT = getenv('PCL_ROOT');


IPath_1 = ['-I',PCL_ROOT,'/include/pcl-1.7'];
IPath_2=['-I',PCL_ROOT,'/3rdParty/Boost/include/boost-1_57'];
IPath_3=['-I',PCL_ROOT,'/3rdParty/Eigen/eigen3'];
IPath_4=['-I',PCL_ROOT,'/3rdParty/FLANN/include'];
IPath_5=['-I',PCL_ROOT,'/3rdParty/VTK/include/vtk-6.2'];


%LIB
LPath_1 = ['-L',fullfile(PCL_ROOT, 'lib')];  
LPath_2 = ['-L',PCL_ROOT,'/3rdParty/Boost/lib'];
LPath_3 = ['-L',PCL_ROOT,'/3rdParty/FLANN/lib'];
LPath_4 = ['-L',PCL_ROOT,'/3rdParty/Qhull/lib'];
LPath_5 = ['-L',PCL_ROOT,'/3rdParty/VTK/lib'];



lPath_1 = ['-l','pcl_features_release.lib'];
lPath_2 = ['-l','pcl_io_release.lib'];
lPath_3 = ['-l','pcl_common_release.lib'];
lPath_4 = ['-l','pcl_kdtree_release.lib'];
lPath_5 = ['-l','pcl_search_release.lib'];


% 
mex('vfh_estimation.cpp','-g',IPath_1,IPath_2,IPath_3,IPath_4,IPath_5,...
                        LPath_1,LPath_2,LPath_3,LPath_4,LPath_5,...
                        lPath_1,lPath_2,lPath_3,lPath_4,lPath_5);




