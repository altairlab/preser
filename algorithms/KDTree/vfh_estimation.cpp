#include "mex.h" 
#include "matrix.h"


#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/vfh.h>
#include <pcl/console/print.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/normal_3d.h>
#include <pcl/PCLPointCloud2.h>
#include <fstream>



void mexFunction( int nlhs, mxArray *plhs[], 
                                  int nrhs, const mxArray *prhs[]) 
{ 

      char* fileName;
      fileName = mxArrayToString(prhs[0]); // prendo in input il file .pcd
  
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

      if (pcl::io::loadPCDFile<pcl::PointXYZ> (fileName, *cloud) == -1) // load the file
      {
        PCL_ERROR ("Couldn't read file");

      }
 
    // Compute the normals
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimation;
    normal_estimation.setInputCloud (cloud);

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    normal_estimation.setSearchMethod (tree);

    pcl::PointCloud<pcl::Normal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::Normal>);
    // radius 1cm
    normal_estimation.setRadiusSearch (0.01);

    normal_estimation.compute (*cloud_with_normals);

    // Create the VFH estimation class, and pass the input dataset+normals to it
	pcl::VFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::VFHSignature308> vfh;
	vfh.setInputCloud(cloud);
	vfh.setInputNormals(cloud_with_normals);
	// alternatively, if cloud is of type PointNormal, do vfh.setInputNormals (cloud);

	// Create an empty kdtree representation, and pass it to the FPFH estimation object.
	// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree2(new pcl::search::KdTree<pcl::PointXYZ>());
	vfh.setSearchMethod(tree2);

	// Output datasets
	pcl::PointCloud<pcl::VFHSignature308>::Ptr vfhs(new pcl::PointCloud<pcl::VFHSignature308>());

	// Compute the features
	vfh.compute(*vfhs);
  
    // SAVE PCD WITH VFH INFORMATION 
    std::string extension = "_vfh.pcd";
    
    std::string filename_vfh = fileName + extension;
   
    pcl::PointCloud<pcl::VFHSignature308> output;
    
    pcl::io::savePCDFile(filename_vfh, *vfhs, false);
   
} 