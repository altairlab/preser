% Obstacle = -1,Target = 0, Robot = 1, Space = 2
classdef AStar < IPPlanner
	properties(Access = private)
		MAX_VAL
	end
	
	methods(Access = public)
		function cartPath = grid2Cart(obj)
			y_center = size(obj.in_occupacy_grid.data,1)/2;
			x_center = size(obj.in_occupacy_grid.data,2)/2;
			
			if(length(obj.out_optimalPath) <= 2)
				cartPath = [];
				return;
			end
			
			for i=1:length(obj.out_optimalPath)
				y_pix = obj.out_optimalPath(i,1);
				x_pix = obj.out_optimalPath(i,2);
				
				if x_pix <= x_center
					x_pos = - (x_center - x_pix)*obj.scale;
				end
				
				if y_pix <= y_center
					y_pos = - (y_center - y_pix)*obj.scale;
				end
				
				if x_pix > x_center
					x_pos = (x_pix - x_center)*obj.scale;
				end
				
				if y_pix > y_center
					y_pos = (y_pix - y_center)*obj.scale;
				end
				cartPath(i,:) = [x_pos y_pos 0];
			end
% 			figure;
% 			plot(cartPath(:,1),cartPath(:,2),'o');
		end	
	end
	
	methods
		function obj = AStar()
			obj = obj@IPPlanner();
			obj.threshold = 10;
		end
		
		function path = getPath(obj,~,~)
			is_interactive = true;
			
			obj.MAX_X = size(obj.in_occupacy_grid.data,1);
			obj.MAX_Y = size(obj.in_occupacy_grid.data,2);
			obj.MAX_VAL = 100;
			obj.scale = obj.in_occupacy_grid.resolution;
			
			% Inflate occupacy grid taking account of robot radius
			robot_size = 0.5;
			inflation_radius = (robot_size/obj.scale)/2;
			INFLATEDMAP = imdilate(obj.in_occupacy_grid.data,strel('disk',inflation_radius));			
			
			% This array stores the coordinates of the map and the Objects in each coordinate
			obj.MAP = INFLATEDMAP;
			obj.MAP(obj.MAP == -1 | obj.MAP < obj.threshold) = 2;
			obj.MAP(obj.MAP >= obj.threshold) = -1;
			
			yStart = fix(obj.in_odometry.pose.pose.position.x/obj.scale);
			xStart = fix(obj.in_odometry.pose.pose.position.y/obj.scale);
			
			xStart = xStart + obj.MAX_X/2;
			yStart = yStart + obj.MAX_Y/2;
			
			if is_interactive
				tmp = obj.MAP;
				tmp(tmp == 2) = 0;
				tmp(tmp == -1) = 155;
				tmp = tmp + obj.in_occupacy_grid.data;
				imshow(tmp,[0 255]);
				hold on;
				
				plot(xStart+.5,yStart+.5,'bo');
				
				xlabel('Please Select the Target using the Left Mouse button','Color','black');
				but = 0;
				while(but ~= 1) % Repeat until the Left button is not clicked
					[xval,yval,but] = ginput(1);
				end
				
				xval = floor(xval);
				yval = floor(yval);
				xTarget = xval;
				yTarget = yval;
				
				obj.MAP(xTarget,yTarget) = 0;
				plot(xTarget+.5,yTarget+.5,'yd');
			else
				xTarget = fix(y/obj.scale) + obj.MAX_X/2;
				yTarget = fix(x/obj.scale) + obj.MAX_Y/2;
			end
			
			obj.MAP(xStart,yStart) = 1;
			
			if is_interactive
				xlabel('Planning ...','Color','black');
			end
						
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%LISTS USED FOR ALGORITHM
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%OPEN LIST STRUCTURE
			%--------------------------------------------------------------------------
			%IS ON LIST 1/0 |X val |Y val |Parent X val |Parent Y val |h(n) |g(n)|f(n)|
			%--------------------------------------------------------------------------
			OPEN=[];
			%CLOSED LIST STRUCTURE
			%--------------
			%X val | Y val |
			%--------------
			% CLOSED=zeros(MAX_VAL,2);
			CLOSED=[];
			
			% Put all obstacles on the Closed list
			k = 1; % Dummy counter
			for i=1:obj.MAX_X
				for j=1:obj.MAX_Y
					if(obj.MAP(i,j) == -1)
						CLOSED(k,2) = i;
						CLOSED(k,1) = j;
						k = k+1;
					end
				end
			end
			CLOSED_COUNT=size(CLOSED,1);
			
			%set the starting node as the robot pose node
			xNode = xStart;
			yNode = yStart;
			OPEN_COUNT=1;
			path_cost=0;
			goal_distance=distance(xNode,yNode,xTarget,yTarget);
			OPEN(OPEN_COUNT,:)=insert_open(xNode,yNode,xNode,yNode,path_cost,goal_distance,goal_distance);
			OPEN(OPEN_COUNT,1)=0;
			CLOSED_COUNT=CLOSED_COUNT+1;
			CLOSED(CLOSED_COUNT,1)=xNode;
			CLOSED(CLOSED_COUNT,2)=yNode;
			NoPath=1;
			
			while((xNode ~= xTarget || yNode ~= yTarget) && NoPath == 1)
				exp_array = expand_array(xNode,yNode,path_cost,xTarget,yTarget,CLOSED,obj.MAX_X,obj.MAX_Y);
				exp_count = size(exp_array,1);
				
				%UPDATE LIST OPEN WITH THE SUCCESSOR NODES
				%OPEN LIST FORMAT
				%--------------------------------------------------------------------------
				%IS ON LIST 1/0 |X val |Y val |Parent X val |Parent Y val |h(n) |g(n)|f(n)|
				%--------------------------------------------------------------------------
				%EXPANDED ARRAY FORMAT
				%--------------------------------
				%|X val |Y val ||h(n) |g(n)|f(n)|
				%--------------------------------
				
				for i=1:exp_count
					flag=0;
					for j=1:OPEN_COUNT
						if(exp_array(i,1) == OPEN(j,2) && exp_array(i,2) == OPEN(j,3) )
							OPEN(j,8) = min(OPEN(j,8),exp_array(i,5)); % #ok<*SAGROW>
							if OPEN(j,8)== exp_array(i,5)
								% UPDATE PARENTS,gn,hn
								OPEN(j,4) = xNode;
								OPEN(j,5) = yNode;
								OPEN(j,6) = exp_array(i,3);
								OPEN(j,7) = exp_array(i,4);
							end; % End of minimum fn check
							flag=1;
						end % End of node check
					end % End of j for
					if flag == 0
						OPEN_COUNT = OPEN_COUNT+1;
						OPEN(OPEN_COUNT,:) = insert_open(exp_array(i,1),exp_array(i,2),xNode,yNode,exp_array(i,3),exp_array(i,4),exp_array(i,5));
					end % End of insert new element into the OPEN list
				end % End of i for
				
				%Find out the node with the smallest fn
				index_min_node = min_fn(OPEN,OPEN_COUNT,xTarget,yTarget);
				if(index_min_node ~= -1)
					% Set xNode and yNode to the node with minimum fn
					xNode = OPEN(index_min_node,2);
					yNode = OPEN(index_min_node,3);
					path_cost = OPEN(index_min_node,6); % Update the cost of reaching the parent node
					% Move the Node to list CLOSED
					CLOSED_COUNT = CLOSED_COUNT+1;
					CLOSED(CLOSED_COUNT,1) = xNode;
					CLOSED(CLOSED_COUNT,2) = yNode;
					OPEN(index_min_node,1)= 0;
				else
					NoPath = 0; % No path exists to the Target exits the loop!
				end % End of index_min_node check
			end % End of While Loop
			
			%Once algorithm has run The optimal path is generated by starting of at the
			%last node(if it is the target node) and then identifying its parent node
			%until it reaches the start node.This is the optimal path
			
			i = size(CLOSED,1);
			obj.out_optimalPath = [];
			xval = CLOSED(i,1);
			yval = CLOSED(i,2);
			i = 1;
			obj.out_optimalPath(i,1) = xval;
			obj.out_optimalPath(i,2) = yval;
			i = i+1;
			
			if((xval == xTarget) && (yval == yTarget))
				% Traverse OPEN and determine the parent nodes
				parent_x = OPEN(node_index(OPEN,xval,yval),4); % node_index returns the index of the node
				parent_y = OPEN(node_index(OPEN,xval,yval),5);
				
				while( parent_x ~= xStart || parent_y ~= yStart)
					obj.out_optimalPath(i,1) = parent_x;
					obj.out_optimalPath(i,2) = parent_y;
					%Get the grandparents :-)
					inode = node_index(OPEN,parent_x,parent_y);
					parent_x = OPEN(inode,4); % node_index returns the index of the node
					parent_y = OPEN(inode,5);
					i=i+1;
				end;
				
				j=size(obj.out_optimalPath,1);
				
				%Plot the Optimal Path!
				if is_interactive
					p = plot(obj.out_optimalPath(j,1)+.5,obj.out_optimalPath(j,2)+.5,'bo');
					j = j-1;
					for i=j:-1:1
						pause(.25);
						set(p,'XData',obj.out_optimalPath(i,1)+.5,'YData',obj.out_optimalPath(i,2)+.5);
						drawnow ;
					end;
					plot(obj.out_optimalPath(:,1)+.5,obj.out_optimalPath(:,2)+.5);
					xlabel('Done','Color','black');					
				end
			else
				if is_interactive
					h = msgbox('Sorry, No path exists to the Target!','warn');
					uiwait(h,5);
				end
			end
			obj.out_optimalPath = flip(obj.out_optimalPath);
			path = obj.grid2Cart();
		end		
	end
end