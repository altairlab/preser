% Obstacle = 1, Space = 0
classdef DStar < IPPlanner
	methods(Access = private)
		function cartPath = grid2Cart(obj)
			y_center = size(obj.in_occupacy_grid.data,1)/2;
			x_center = size(obj.in_occupacy_grid.data,2)/2;
			
			for i=1:length(obj.out_optimalPath)
				y_pix = obj.out_optimalPath(i,1);
				x_pix = obj.out_optimalPath(i,2);
				
				if x_pix <= x_center
					x_pos = - (x_center - x_pix)*obj.scale;
				end
				
				if y_pix <= y_center
					y_pos = - (y_center - y_pix)*obj.scale;
				end
				
				if x_pix > x_center
					x_pos = (x_pix - x_center)*obj.scale;
				end
				
				if y_pix > y_center
					y_pos = (y_pix - y_center)*obj.scale;
				end
				cartPath(i,:) = [x_pos y_pos 0];
			end
			%figure;
			%plot(cartPath(:,1),cartPath(:,2),'o');
		end		
	end
	
	methods
		function obj = DStar()
			obj = obj@IPPlanner();
		end
		
		function path = getPath(obj,x,y)
			obj.MAX_X = size(obj.in_occupacy_grid.data,1);
			obj.MAX_Y = size(obj.in_occupacy_grid.data,2);
			
			obj.MAP = obj.in_occupacy_grid.data;
			obj.MAP(obj.MAP == -1 | obj.MAP < obj.threshold) = 0;
			obj.MAP(obj.MAP >= obj.threshold) = 1;
			
			obj.scale = obj.in_occupacy_grid.resolution;
			ds = Dstar(obj.MAP);

			yStart = fix(obj.in_odometry.pose.pose.position.x/obj.scale);
			xStart = fix(obj.in_odometry.pose.pose.position.y/obj.scale);
			xStart = xStart + obj.MAX_X/2;
			yStart = yStart + obj.MAX_Y/2;
			
			start=[xStart,yStart];

			xTarget = fix(y/obj.scale) + obj.MAX_X/2;
			yTarget = fix(x/obj.scale) + obj.MAX_Y/2;

			goal = [xTarget,yTarget];
			
			ds.plan(goal);
			obj.out_optimalPath = ds.path(start);
			
			path = grid2Cart();			
		end
	end
end