#include <amcl_node.hpp>
#include "mex.h"
#include <iostream>

static const char* fields_odom[] = {"x","y","yaw"};

class AmclMex
{
private:
	AmclNode amcl;

public:
	AmclMex(){};
	~AmclMex(){};

	inline void mex2Amcl(const LaserScan& scan, const Odometry& odom, const Map& map)
	{
		amcl.doAMCL(scan, odom, map);
	}

	inline void initSystem(const Parameters_amcl& param)
	{
		amcl.init(param);
	}

	inline mxArray* getEstPose()
	{
		Odometry o = amcl.getPose();
		mwSize dims[2] = {1, 1};
		mxArray* strct= mxCreateStructArray(1,dims,3,fields_odom);
		
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"x"),mxCreateDoubleScalar(o.x));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"y"),mxCreateDoubleScalar(o.y));
		mxSetFieldByNumber(strct,0,mxGetFieldNumber(strct,"yaw"),mxCreateDoubleScalar(o.yaw));
		return strct;
	}
};

static AmclMex mex_class;
static bool isFirst = true;
/*
	1st arg: laser scan structure
	2nd arg: odometry structure
	return: occupacy grid structure
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	Odometry odom;
	LaserScan scan;
	Parameters_amcl param;
	Map map;

	const mxArray* in_data;

	// laser
	in_data = prhs[0];

	double* stamp = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"stamp")));
	double* ranges = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ranges")));
	double* intensities = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"intensities")));				

	scan = LaserScan
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_min"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_max"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"angle_increment"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"time_increment"))),
		{(int32_t)stamp[0],(int32_t)stamp[1]},
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"range_min"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"range_max"))),
		std::vector<double>(ranges,
			ranges + mxGetN(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"ranges")))),
		std::vector<double>(intensities,
			intensities + mxGetN(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"intensities")))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"x"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"y"))),
	};

	// odometry
	in_data = prhs[1];

	odom = Odometry
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"x"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"y"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"yaw")))
	};

	// map
	in_data = prhs[2];

	double* data = mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"data")));

	map = Map
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"resolution"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"width"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"height"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"position_x"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"position_y"))),				
		std::vector<double>(data,
			data + mxGetN(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"data")))),
		Odometry{0,0,0}
	};

	// parameters
	in_data = prhs[3];

	param = Parameters_amcl
	{
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"first_map_only"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"update_min_d"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"update_min_a"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"resample_interval"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_min_range"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_max_range"))),
		(int)*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_max_beams"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_alpha1"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_alpha2"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_alpha3"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_alpha4"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_alpha5"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_z_hit"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_z_short"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_z_max"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_z_rand"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_sigma_hit"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_lambda_short"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_likelihood_max_dist"))),
		mxArrayToString(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"laser_model_type"))),
		mxArrayToString(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"odom_model_type"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"min_particles"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"max_particles"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"recovery_alpha_slow"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"recovery_alpha_fast"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"do_beamskip"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"beam_skip_distance"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"beam_skip_threshold"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"kld_err"))),
		*mxGetPr(mxGetFieldByNumber(in_data,0,mxGetFieldNumber(in_data,"kld_z")))
	};

	if(isFirst)
	{
		std::cout << "Initializing amcl ..." << std::endl;
		mex_class.initSystem(param);
		std::cout << "Initialization done" << std::endl;		
		isFirst = false;
	}

	mex_class.mex2Amcl(scan,odom,map);
	plhs[0] = mex_class.getEstPose();
}