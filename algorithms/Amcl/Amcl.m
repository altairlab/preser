% The map data, in row-major order, starting with (0,0).
% Occupancy probabilities are in the range [0,100], unknown is -1.
classdef Amcl < handle
	properties(Access = private)
		parameters
	end

	properties(Access = public)
		in_laser_data
		in_odometry
		in_occupacy_grid
		out_odometry
	end
	
	methods
		function obj = Amcl()
			% code with first_map_only doesn't update the robot pose
			is_map_static = false;
			
			obj.in_laser_data = data_laser();
			obj.in_odometry = data_odometry();
			obj.in_occupacy_grid = data_occupacygrid();
			obj.out_odometry = data_odometry();
			
			obj.parameters = struct('first_map_only',is_map_static,'update_min_d',0.2, 'update_min_a',pi/6.0, 'resample_interval', 2, ...
			'laser_min_range',-1.0,'laser_max_range',-1.0,'laser_max_beams',30,'odom_alpha1',0.2,'odom_alpha2',0.2,'odom_alpha3',0.2, ...
			'odom_alpha4',0.2,'odom_alpha5',0.2,'laser_z_hit',0.95,'laser_z_short',0.1,'laser_z_max',0.05,'laser_z_rand',0.05, ...
			'laser_sigma_hit',0.2,'laser_lambda_short',0.1,'laser_likelihood_max_dist',2.0,'laser_model_type','beam', ...
			'odom_model_type','diff', 'min_particles',100,'max_particles',5000, 'recovery_alpha_slow',0.001,'recovery_alpha_fast',0.1, ...
			'do_beamskip',false,'beam_skip_distance',0.5,'beam_skip_threshold',0.3,'kld_err',0.01, 'kld_z',0.99);
		end
		
		function obj = configure(obj)
		end
	
		function obj = start(obj)
		end

		function obj = update(obj)
			if obj.in_laser_data.ranges(1) ~= -1 && size(obj.in_occupacy_grid.data,1) > 1
				laser_data = struct('angle_min',obj.in_laser_data.angle_min,'angle_max',obj.in_laser_data.angle_max,'angle_increment', ...
				obj.in_laser_data.angle_increment,'time_increment',obj.in_laser_data.time_increment,'stamp',obj.in_laser_data.stamp, ...
				'range_min',obj.in_laser_data.range_min,'range_max',obj.in_laser_data.range_max,'ranges',obj.in_laser_data.ranges, ...	
				'intensities',obj.in_laser_data.intensities,'x',0.3180,'y',0.00036);

				[~,~,y] = obj.in_odometry.pose.pose.getRPY();
				odometry_data = struct('x',obj.in_odometry.pose.pose.position.x,'y',obj.in_odometry.pose.pose.position.y,'yaw',y);

				occupacy_data = struct('resolution',obj.in_occupacy_grid.resolution,'width',size(obj.in_occupacy_grid.data,1),'height', ...
					size(obj.in_occupacy_grid.data,2),'position_x',0,'position_y',0,'data',obj.in_occupacy_grid.data);
				
				vec = AmclMex(laser_data, odometry_data, occupacy_data, obj.parameters);
				
				obj.out_odometry.pose.pose.position.x = vec.x;
				obj.out_odometry.pose.pose.position.y = vec.y;
				obj.out_odometry.pose.pose = obj.out_odometry.pose.pose.setOrientation(a2quat(0,0,vec.yaw,'XYZ')); % angle2quat -> Aerospace Toolbox
			end
		end
	end
end