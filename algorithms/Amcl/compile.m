% Importing all algos
addpath(genpath('algorithms'));

% Compile mex
% --> remember to add a space at the end of every filepath
g_mapping_source = ['src/amcl_node.cpp ' 'src/amcl/map/map.c ' 'src/amcl/map/map_cspace.cpp ' ...
'src/amcl/map/map_draw.c ' 'src/amcl/map/map_range.c ' 'src/amcl/map/map_store.c ' ...
'src/amcl/pf/eig3.c ' 'src/amcl/pf/pf.c ' 'src/amcl/pf/pf_draw.c ' 'src/amcl/pf/pf_kdtree.c ' 'src/amcl/pf/pf_pdf.c '...
'src/amcl/pf/pf_vector.c ' 'src/amcl/sensors/amcl_laser.cpp ' 'src/amcl/sensors/amcl_odom.cpp ' ...
'src/amcl/sensors/amcl_sensor.cpp '];
include_dir = '-Iinclude -Iinclude/amcl -I../ -Iinclude/amcl/pf -Iinclude/amcl/map -Iinclude/amcl/sensors';
main_file = 'amcl_mex.cpp';
output_name = 'AmclMex';

eval(['mex CXXFLAGS="$CXXFLAGS -Wall -std=c++11" -output ' output_name ' ' include_dir ' ' main_file ' ' g_mapping_source]);