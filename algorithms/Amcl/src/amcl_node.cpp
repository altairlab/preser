/*
 *  Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Author: Brian Gerkey */
#include "amcl_node.hpp"

/* 29/6/2015
   Added definition for random functions for windows compiler
 */

#include <stdlib.h>
#include <time.h>

#ifdef _WIN32
#define srand48(s) srand(s)
#define drand48() ((((double)rand())*(double)RAND_MAX))
#define lrand48() rand()
#endif

std::vector<std::pair<int,int> > AmclNode::free_space_indices;

AmclNode::AmclNode() :
        map_(NULL),
        pf_(NULL),
        resample_count_(0),
        odom_(NULL),
        laser_(NULL),
        initial_pose_hyp_(NULL),
        first_map_received_(false)
{
  first_map_only_ = false;
  laser_min_range_ = -1.0;
  laser_max_range_ = -1.0;
  max_beams_ = 30;
  min_particles_ = 100;
  max_particles_ = 5000;
  pf_err_ = 0.01;
  pf_z_ = 0.99;
  alpha1_ = 0.2;
  alpha2_ = 0.2;
  alpha3_ = 0.2;
  alpha4_ = 0.2;
  alpha5_ = 0.2;
  
  do_beamskip_ = false;
  beam_skip_distance_ = 0.5;
  beam_skip_threshold_ = 0.3;
  beam_skip_error_threshold_ = 0.9;

  z_hit_ = 0.95;
  z_short_ = 0.1;
  z_max_ = 0.05;
  z_rand_ = 0.05;
  sigma_hit_ = 0.2;
  lambda_short_ = 0.1;
  laser_likelihood_max_dist_ = 2.0;
  
  std::string tmp_model_type;
  tmp_model_type = std::string("beam");
  
  if(tmp_model_type == "beam")
    laser_model_type_ = LASER_MODEL_BEAM;
  else if(tmp_model_type == "likelihood_field")
    laser_model_type_ = LASER_MODEL_LIKELIHOOD_FIELD;
  else if(tmp_model_type == "likelihood_field_prob")
  {
    laser_model_type_ = LASER_MODEL_LIKELIHOOD_FIELD_PROB;
  }
  else
  {
    laser_model_type_ = LASER_MODEL_LIKELIHOOD_FIELD;
  }

  tmp_model_type = std::string("diff");

  if(tmp_model_type == "diff")
    odom_model_type_ = ODOM_MODEL_DIFF;
  else if(tmp_model_type == "omni")
    odom_model_type_ = ODOM_MODEL_OMNI;
  else if(tmp_model_type == "diff-corrected")
    odom_model_type_ = ODOM_MODEL_DIFF_CORRECTED;
  else if(tmp_model_type == "omni-corrected")
    odom_model_type_ = ODOM_MODEL_OMNI_CORRECTED;
  else
  {
    odom_model_type_ = ODOM_MODEL_DIFF;
  }

  d_thresh_ = 0.2;
  a_thresh_ = M_PI/6.0;
  odom_frame_id_ = std::string("odom");
  base_frame_id_ = std::string("base_link");
  global_frame_id_ = std::string("map");
  resample_interval_ = 2;
  double tmp_tol;
  tmp_tol = 0.1;
  alpha_slow_ = 0.001;
  alpha_fast_ = 0.1;

  updatePoseFromServer();

  m_force_update = false;
}

void AmclNode::init(const Parameters_amcl& config)
{
  first_map_only_ = config.first_map_only;
  d_thresh_ = config.update_min_d;
  a_thresh_ = config.update_min_a;

  resample_interval_ = config.resample_interval;

  laser_min_range_ = config.laser_min_range;
  laser_max_range_ = config.laser_max_range;

  max_beams_ = config.laser_max_beams;

  alpha1_ = config.odom_alpha1;
  alpha2_ = config.odom_alpha2;
  alpha3_ = config.odom_alpha3;
  alpha4_ = config.odom_alpha4;
  alpha5_ = config.odom_alpha5;

  z_hit_ = config.laser_z_hit;
  z_short_ = config.laser_z_short;
  z_max_ = config.laser_z_max;
  z_rand_ = config.laser_z_rand;
  sigma_hit_ = config.laser_sigma_hit;
  lambda_short_ = config.laser_lambda_short;
  laser_likelihood_max_dist_ = config.laser_likelihood_max_dist;

  if(config.laser_model_type == "beam")
    laser_model_type_ = LASER_MODEL_BEAM;
  else if(config.laser_model_type == "likelihood_field")
    laser_model_type_ = LASER_MODEL_LIKELIHOOD_FIELD;
  else if(config.laser_model_type == "likelihood_field_prob")
    laser_model_type_ = LASER_MODEL_LIKELIHOOD_FIELD_PROB;

  if(config.odom_model_type == "diff")
    odom_model_type_ = ODOM_MODEL_DIFF;
  else if(config.odom_model_type == "omni")
    odom_model_type_ = ODOM_MODEL_OMNI;
  else if(config.odom_model_type == "diff-corrected")
    odom_model_type_ = ODOM_MODEL_DIFF_CORRECTED;
  else if(config.odom_model_type == "omni-corrected")
    odom_model_type_ = ODOM_MODEL_OMNI_CORRECTED;

  double tmp_max,tmp_min;

  tmp_max = config.max_particles;
  tmp_min = config.min_particles;

  if(tmp_min > tmp_max)
  {
    tmp_max = tmp_min;
  }

  min_particles_ = tmp_min;
  max_particles_ = tmp_max;

  alpha_slow_ = config.recovery_alpha_slow;
  alpha_fast_ = config.recovery_alpha_fast;

  do_beamskip_= config.do_beamskip; 
  beam_skip_distance_ = config.beam_skip_distance; 
  beam_skip_threshold_ = config.beam_skip_threshold; 

  pf_err_ = config.kld_err; 
  pf_z_ = config.kld_z;
}

void AmclNode::updatePoseFromServer()
{
  init_pose_[0] = odom_pose.x;
  init_pose_[1] = odom_pose.y;
  init_pose_[2] = odom_pose.yaw;

  init_cov_[0] = 0.5 * 0.5;
  init_cov_[1] = 0.5 * 0.5;
  init_cov_[2] = (M_PI/12.0) * (M_PI/12.0);
}

void AmclNode::mapReceived(const Map& msg)
{
  if(first_map_only_ && first_map_received_)
  {
    return;
  }

  handleMapMessage(msg);
  first_map_received_ = true;
}

void AmclNode::handleMapMessage(const Map& msg)
{
  freeMapDependentMemory();

  lasers_.clear();
  lasers_update_.clear();
  frame_to_laser_.clear();

  map_ = convertMap(msg);

#if NEW_UNIFORM_SAMPLING
  // Index of free space
  free_space_indices.resize(0);
  for(int i = 0; i < map_->size_x; i++)
    for(int j = 0; j < map_->size_y; j++)
      if(map_->cells[MAP_INDEX(map_,i,j)].occ_state == -1)
        free_space_indices.push_back(std::make_pair(i,j));
#endif
  // Create the particle filter
  pf_ = pf_alloc(min_particles_, max_particles_,
                 alpha_slow_, alpha_fast_,
                 (pf_init_model_fn_t)AmclNode::uniformPoseGenerator,
                 (void *)map_);

  pf_->pop_err = pf_err_;
  pf_->pop_z = pf_z_;

  // Initialize the filter
  updatePoseFromServer();

  pf_vector_t pf_init_pose_mean = pf_vector_zero();
  pf_init_pose_mean.v[0] = init_pose_[0];
  pf_init_pose_mean.v[1] = init_pose_[1];
  pf_init_pose_mean.v[2] = init_pose_[2];

  pf_matrix_t pf_init_pose_cov = pf_matrix_zero();

  pf_init_pose_cov.m[0][0] = init_cov_[0];
  pf_init_pose_cov.m[1][1] = init_cov_[1];
  pf_init_pose_cov.m[2][2] = init_cov_[2];

  pf_init(pf_, pf_init_pose_mean, pf_init_pose_cov);
  pf_init_ = false;

  // Instantiate the sensor objects

  // Odometry
  delete odom_;
  odom_ = new AMCLOdom();
  odom_->SetModel( odom_model_type_, alpha1_, alpha2_, alpha3_, alpha4_, alpha5_ );

  // Laser
  delete laser_;
  laser_ = new AMCLLaser(max_beams_, map_);
  if(laser_model_type_ == LASER_MODEL_BEAM)
  {
    laser_->SetModelBeam(z_hit_, z_short_, z_max_, z_rand_, sigma_hit_, lambda_short_, 0.0);
  }
  else if(laser_model_type_ == LASER_MODEL_LIKELIHOOD_FIELD_PROB)
  {
    laser_->SetModelLikelihoodFieldProb(z_hit_, z_rand_, sigma_hit_,
					laser_likelihood_max_dist_, 
					do_beamskip_, beam_skip_distance_, 
					beam_skip_threshold_, beam_skip_error_threshold_);
  }
  else
  {
    laser_->SetModelLikelihoodField(z_hit_, z_rand_, sigma_hit_, laser_likelihood_max_dist_);
  }

  // In case the initial pose message arrived before the first map,
  // try to apply the initial pose now that the map has arrived.
  //std::cout << "Calling applyInit\n";
  applyInitialPose();
}

void AmclNode::freeMapDependentMemory()
{
  if( map_ != NULL )
  {
    map_free( map_ );
    map_ = NULL;
  }

  if( pf_ != NULL )
  {
    pf_free( pf_ );
    pf_ = NULL;
  }
  
  delete odom_;
  odom_ = NULL;
  delete laser_;
  laser_ = NULL;
}

/**
 * Convert an OccupancyGrid map message into the internal
 * representation.  This allocates a map_t and returns it.
 */
map_t* AmclNode::convertMap(const Map& map_msg)
{
  map_t* map = map_alloc();

  map->size_x = map_msg.width;
  map->size_y = map_msg.height;
  map->scale = map_msg.resolution;
  map->origin_x = map_msg.position_x + (map->size_x / 2) * map->scale;
  map->origin_y = map_msg.position_y + (map->size_y / 2) * map->scale;

  // Convert to player format
  map->cells = (map_cell_t*)malloc(sizeof(map_cell_t)*map->size_x*map->size_y);

  for(int i=0;i<map->size_x * map->size_y;i++)
  {
    if(map_msg.data[i] == 0)
      map->cells[i].occ_state = -1;
    else if(map_msg.data[i] == 100)
      map->cells[i].occ_state = +1;
    else
      map->cells[i].occ_state = 0;
  }

  return map;
}

AmclNode::~AmclNode()
{
  freeMapDependentMemory();
}

Odometry AmclNode::getPose()
{
  return est_pose;
}

void AmclNode::doAMCL(const LaserScan& scan, const Odometry& odom, const Map& map)
{
  this->scan = scan;
  this->odom_pose = odom;
  this->in_map = map;

  mapReceived(in_map);
  laserReceived(scan);
}

bool AmclNode::getOdomPose(Odometry& pose,double& x, double& y, double& yaw,
                          const stamp_t& t, const std::string& f)
{
  pose = this->odom_pose;
  x = pose.x;
  y = pose.y;
  yaw = pose.yaw;
  return true;
}


pf_vector_t AmclNode::uniformPoseGenerator(void* arg)
{
  map_t* map = (map_t*)arg;
#if NEW_UNIFORM_SAMPLING
  unsigned int rand_index = drand48() * free_space_indices.size();
  std::pair<int,int> free_point = free_space_indices[rand_index];
  pf_vector_t p;
  p.v[0] = MAP_WXGX(map, free_point.first);
  p.v[1] = MAP_WYGY(map, free_point.second);
  p.v[2] = drand48() * 2 * M_PI - M_PI;
#else
  double min_x, max_x, min_y, max_y;

  min_x = (map->size_x * map->scale)/2.0 - map->origin_x;
  max_x = (map->size_x * map->scale)/2.0 + map->origin_x;
  min_y = (map->size_y * map->scale)/2.0 - map->origin_y;
  max_y = (map->size_y * map->scale)/2.0 + map->origin_y;

  pf_vector_t p;

  for(;;)
  {
    p.v[0] = min_x + drand48() * (max_x - min_x);
    p.v[1] = min_y + drand48() * (max_y - min_y);
    p.v[2] = drand48() * 2 * M_PI - M_PI;

    // Check that it's a free cell
    int i,j;
    i = MAP_GXWX(map, p.v[0]);
    j = MAP_GYWY(map, p.v[1]);
    if(MAP_VALID(map,i,j) && (map->cells[MAP_INDEX(map,i,j)].occ_state == -1))
      break;
  }
#endif
  return p;
}

void AmclNode::laserReceived(const LaserScan& laser_scan)
{
  if(map_ == NULL)
  {
    return;
  }

  int laser_index = -1;

  // Do we have the base->base_laser Tx yet?
  if(frame_to_laser_.find("laser_scan->header.frame_id") == frame_to_laser_.end())
  {
    lasers_.push_back(new AMCLLaser(*laser_));
    lasers_update_.push_back(true);
    laser_index = frame_to_laser_.size();

    pf_vector_t laser_pose_v;

    double laser_x = laser_scan.x;
    double laser_y = laser_scan.y;
    double laser_theta = odom_pose.yaw;

    double laser_dist = sqrt(laser_x*laser_x + laser_y*laser_y);

    laser_pose_v.v[0] = odom_pose.x + (laser_dist*cos(laser_theta));
    laser_pose_v.v[1] = odom_pose.y + (laser_dist*sin(laser_theta));

    // laser mounting angle gets computed later -> set to 0 here!
    laser_pose_v.v[2] = odom_pose.yaw;
    lasers_[laser_index]->SetLaserPose(laser_pose_v);
    frame_to_laser_["laser_scan->header.frame_id"] = laser_index;
  }
  else
  {
    // we have the laser pose, retrieve laser index
    laser_index = frame_to_laser_["aser_scan->header.frame_id"];
  }

  // Where was the robot when this scan was taken?
  pf_vector_t pose;
  if(!getOdomPose(latest_odom_pose_, pose.v[0], pose.v[1], pose.v[2],
                  laser_scan.stamp, "base_frame_id_"))
  {
    return;
  }

  pf_vector_t delta = pf_vector_zero();

  if(pf_init_)
  {
    // Compute change in pose
    delta.v[0] = pose.v[0] - pf_odom_pose_.v[0];
    delta.v[1] = pose.v[1] - pf_odom_pose_.v[1];
    delta.v[2] = angle_diff(pose.v[2], pf_odom_pose_.v[2]);

    // See if we should update the filter
    bool update = fabs(delta.v[0]) > d_thresh_ ||
                  fabs(delta.v[1]) > d_thresh_ ||
                  fabs(delta.v[2]) > a_thresh_;

    update = update || m_force_update;
    m_force_update=false;

    // Set the laser update flags
    if(update)
      for(unsigned int i=0; i < lasers_update_.size(); i++)
        lasers_update_[i] = true;
  }

  bool force_publication = false;
  if(!pf_init_)
  {
    // Pose at last filter update
    pf_odom_pose_ = pose;

    // Filter is now initialized
    pf_init_ = true;

    // Should update sensor data
    for(unsigned int i=0; i < lasers_update_.size(); i++)
      lasers_update_[i] = true;

    force_publication = true;
    resample_count_ = 0;
  }
  // If the robot has moved, update the filter
  else if(pf_init_ && lasers_update_[laser_index])
  {
    AMCLOdomData odata;
    odata.pose = pose;

    // HACK
    // Modify the delta in the action data so the filter gets
    // updated correctly
    odata.delta = delta;
    // Use the action data to update the filter
    odom_->UpdateAction(pf_, (AMCLSensorData*)&odata);
  }

  bool resampled = false;
  // If the robot has moved, update the filter
  if(lasers_update_[laser_index])
  {
    AMCLLaserData ldata;
    ldata.sensor = lasers_[laser_index];
    ldata.range_count = laser_scan.ranges.size();

    double angle_min = laser_scan.angle_min;
    double angle_increment = laser_scan.angle_increment;

    // wrapping angle to [-pi .. pi]
    angle_increment = fmod(angle_increment + 5*M_PI, 2*M_PI) - M_PI;

    // Apply range min/max thresholds, if the user supplied them
    if(laser_max_range_ > 0.0)
      ldata.range_max = std::min(laser_scan.range_max, (double)laser_max_range_);
    else
      ldata.range_max = laser_scan.range_max;
    double range_min;
    if(laser_min_range_ > 0.0)
      range_min = std::max(laser_scan.range_min, (double)laser_min_range_);
    else
      range_min = laser_scan.range_min;

    // The AMCLLaserData destructor will free this memory
    ldata.ranges = new double[ldata.range_count][2];
    
    for(int i=0;i<ldata.range_count;i++)
    {
      // amcl doesn't (yet) have a concept of min range.  So we'll map short
      // readings to max range.
      if(laser_scan.ranges[i] <= range_min)
        ldata.ranges[i][0] = ldata.range_max;
      else
        ldata.ranges[i][0] = laser_scan.ranges[i];
      // Compute bearing
      ldata.ranges[i][1] = angle_min + (i * angle_increment);
    }

    lasers_[laser_index]->UpdateSensor(pf_, (AMCLSensorData*)&ldata);

    lasers_update_[laser_index] = false;
    pf_odom_pose_ = pose;

    // Resample the particles
    if(!(++resample_count_ % resample_interval_))
    {
      pf_update_resample(pf_);
      resampled = true;
    }

    pf_sample_set_t* set = pf_->sets + pf_->current_set;
  }

  if(resampled || force_publication)
  {
    // Read out the current hypotheses
    double max_weight = 0.0;
    int max_weight_hyp = -1;
    std::vector<amcl_hyp_t> hyps;
    hyps.resize(pf_->sets[pf_->current_set].cluster_count);
    for(int hyp_count = 0; hyp_count < pf_->sets[pf_->current_set].cluster_count; hyp_count++)
    {
      double weight;
      pf_vector_t pose_mean;
      pf_matrix_t pose_cov;
      if (!pf_get_cluster_stats(pf_, hyp_count, &weight, &pose_mean, &pose_cov))
      {
        break;
      }

      hyps[hyp_count].weight = weight;
      hyps[hyp_count].pf_pose_mean = pose_mean;
      hyps[hyp_count].pf_pose_cov = pose_cov;

      if(hyps[hyp_count].weight > max_weight)
      {
        max_weight = hyps[hyp_count].weight;
        max_weight_hyp = hyp_count;
      }
    }

    if(max_weight > 0.0)
    {
      est_pose.x = hyps[max_weight_hyp].pf_pose_mean.v[0];
      est_pose.y = hyps[max_weight_hyp].pf_pose_mean.v[1];
      est_pose.yaw = hyps[max_weight_hyp].pf_pose_mean.v[2];
    }
  }
}

/**
 * If initial_pose_hyp_ and map_ are both non-null, apply the initial
 * pose to the particle filter state.  initial_pose_hyp_ is deleted
 * and set to NULL after it is used.
 */
void AmclNode::applyInitialPose()
{
  if( initial_pose_hyp_ != NULL && map_ != NULL )
  {
    pf_init(pf_, initial_pose_hyp_->pf_pose_mean, initial_pose_hyp_->pf_pose_cov);
    pf_init_ = false;

    delete initial_pose_hyp_;
    initial_pose_hyp_ = NULL;
  }
}