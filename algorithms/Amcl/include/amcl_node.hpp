#define _USE_MATH_DEFINES
#include <algorithm>
#include <vector>
#include <map>
#include <cmath>
#include <string>
#include <iostream>

#include <map/map.h>
#include <pf/pf.h>
#include <sensors/amcl_odom.h>
#include <sensors/amcl_laser.h>

#include <common.h>
#define NEW_UNIFORM_SAMPLING 1

using namespace amcl;

// Pose hypothesis
typedef struct
{
	// Total weight (weights sum to 1)
	double weight;
	// Mean of pose esimate
	pf_vector_t pf_pose_mean;
	// Covariance of pose estimate
	pf_matrix_t pf_pose_cov;
} amcl_hyp_t;

static double normalize(double z)
{
	return atan2(sin(z),cos(z));
}
static double angle_diff(double a, double b)
{
	double d1, d2;
	a = normalize(a);
	b = normalize(b);
	d1 = a-b;
	d2 = 2*M_PI - fabs(d1);
	if(d1 > 0)
	{
		d2 *= -1.0;
	}
  	if(fabs(d1) < fabs(d2))
  	{
		return(d1);
  	}
  	else
  	{
		return(d2);
	}
}

class AmclNode
{
public:
	AmclNode();
	~AmclNode();

	void doAMCL(const LaserScan& scan, const Odometry& odom, const Map& map);
	void init(const Parameters_amcl& param);
	Odometry getPose();

private:
	// Pose-generating function used to uniformly distribute particles over the map
	static pf_vector_t uniformPoseGenerator(void* arg);

	void laserReceived(const LaserScan& laser_scan);
	void mapReceived(const Map& msg);
	void handleMapMessage(const Map& msg);
	void freeMapDependentMemory();
	map_t* convertMap(const Map& map_msg);
	void updatePoseFromServer();
	void applyInitialPose();

	bool getOdomPose(Odometry& pose,
					 double& x, double& y, double& yaw,
					 const stamp_t& t, const std::string& f);

	Odometry odom_pose;
	LaserScan scan;
	Map in_map;
	Odometry est_pose;

#if NEW_UNIFORM_SAMPLING
	static std::vector<std::pair<int,int> > free_space_indices;
#endif

	//parameter for what odom to use
	std::string odom_frame_id_;

	//paramater to store latest odom pose
	Odometry latest_odom_pose_;

	//parameter for what base to use
	std::string base_frame_id_;
	std::string global_frame_id_;

	bool first_map_only_;

	map_t* map_;
	char* mapdata;
	int sx, sy;
	double resolution;

	std::vector< AMCLLaser* > lasers_;
	std::vector< bool > lasers_update_;
	std::map< std::string, int > frame_to_laser_;

	// Particle filter
	pf_t *pf_;
	double pf_err_, pf_z_;
	bool pf_init_;
	pf_vector_t pf_odom_pose_;
	double d_thresh_, a_thresh_;
	int resample_interval_;
	int resample_count_;
	double laser_min_range_;
	double laser_max_range_;

	//Nomotion update control
	bool m_force_update;  // used to temporarily let amcl update samples even when no motion occurs...

	AMCLOdom* odom_;
	AMCLLaser* laser_;

	amcl_hyp_t* initial_pose_hyp_;
	bool first_map_received_;

	int max_beams_, min_particles_, max_particles_;
	double alpha1_, alpha2_, alpha3_, alpha4_, alpha5_;
	double alpha_slow_, alpha_fast_;
	double z_hit_, z_short_, z_max_, z_rand_, sigma_hit_, lambda_short_;

  //beam skip related params
	bool do_beamskip_;
	double beam_skip_distance_, beam_skip_threshold_, beam_skip_error_threshold_;
	double laser_likelihood_max_dist_;
	odom_model_t odom_model_type_;
	double init_pose_[3];
	double init_cov_[3];
	laser_model_t laser_model_type_;
};
