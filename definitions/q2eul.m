function angles = q2eul( q )

qin = bsxfun(@rdivide, q, sqrt(sum(q.^2, 2)));

phi = atan2(2.*(qin(:,3).*qin(:,4) + qin(:,1).*qin(:,2)), ...
                 qin(:,1).^2 - qin(:,2).^2 - qin(:,3).^2 + qin(:,4).^2);

theta = asin(-2.*(qin(:,2).*qin(:,4) - qin(:,1).*qin(:,3)));

psi = atan2(2.*(qin(:,2).*qin(:,3) + qin(:,1).*qin(:,4)), ...
                 qin(:,1).^2 + qin(:,2).^2 - qin(:,3).^2 - qin(:,4).^2);

angles = [phi theta psi];