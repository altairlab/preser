classdef ctrl_modes < Simulink.IntEnumType
   enumeration
      PLANE_ANGLE (1)
			ANGULAR_VELOCITY(2)
			TORQUE (3)
			MOTOR_STOP (4)
			TWIST (5)
   end
end