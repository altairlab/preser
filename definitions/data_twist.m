classdef data_twist
	properties(Access = public)
		linear = struct('x',0,'y',0,'z',0);
		angular = struct('x',0,'y',0,'z',0,'w',1);
	end

	methods 
		function obj = setLinear(obj,x,y,z)
			obj.linear = struct('x',x,'y',y,'z',z);
		end
	
		function obj = setAngular(obj,x_or_quat,y,z,w)
			if nargin == 2
				quat = x_or_quat;
				obj.angular = struct('x',quat(1),'y',quat(2),'z',quat(3),'w',quat(4));
			else
				x = x_or_quat;
				obj.angular = struct('x',x,'y',y,'z',z,'w',w);
			end
		end		
	end	
end