classdef data_pose
	properties(Access = public)
		position = struct('x',0,'y',0,'z',0);
		orientation = struct('x',0,'y',0,'z',0,'w',1);
	end
	
	methods 
		function obj = setPose(obj,x,y,z)
			obj.position = struct('x',x,'y',y,'z',z);
		end
	
		function obj = setOrientation(obj,x_or_quat,y,z,w)
			if nargin == 2
				quat = x_or_quat;
				obj.orientation = struct('x',quat(1),'y',quat(2),'z',quat(3),'w',quat(4));
			else
				x = x_or_quat;
				obj.orientation = struct('x',x,'y',y,'z',z,'w',w);
			end
		end
		
		function [r, p, y] = getRPY(obj)
% 			[r, p, y] = quat2angle(struct2array(obj.orientation),'XYZ');   % quat2angle -> Aerospace toolbox 
            [r, p, y] = q2angle(struct2array(obj.orientation),'XYZ'); 
		end
	end
end