% a2quat
% Convert rotation angles to quaternion
% XYZ - roll pitch yaw

function q = a2quat( r, p, y , options)
    T = rpy2tr(r,p,y);
    q = tform2quat(T);
end