classdef data_laser
	properties(Access = public)
		angle_min = 0;
		angle_max = 0;
		angle_increment = 0;
		time_increment = 0;
		stamp = [0 0];
		range_min = 0;
		range_max = 0;
		ranges = -1;
		intensities = 0;
	end
end