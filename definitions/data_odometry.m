classdef data_odometry
	properties(Access = public)
		pose = struct('pose',data_pose());
		twist = struct('twist',data_twist());
	end
end