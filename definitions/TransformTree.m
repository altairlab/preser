% TransformTree
% TransformTree(id,vrep, camera, robot.Base)
% Get Transform WHILE simulation is Running

classdef TransformTree < handle
    properties(Access = public)
        orientation
        position
    end
    
    methods
        function obj = TransformTree(id,vrep, frame, reference_Frame)
            
            
            if ~isempty( frame.link ) && ~isempty( reference_Frame.link )
                if ~isempty( frame.link.name ) && ~isempty( reference_Frame.link.name )
                    
                    [res1, obj.orientation] = vrep.simxGetObjectOrientation(id, frame.link.frame_id , reference_Frame.link.frame_id ,vrep.simx_opmode_oneshot_wait);
                    [res2, obj.position ] = vrep.simxGetObjectPosition(id, frame.link.frame_id , reference_Frame.link.frame_id ,vrep.simx_opmode_oneshot_wait);
                end
            else
                disp('********** No Transforms Available **********');
            end
            
            
        end
        
    end
    
end

