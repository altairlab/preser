% q2angle
% Convert quaternion to rotation angles

function [r, p, y] = q2angle(quat,options)

    tf_quat = quat2tform(quat) ;
    RPY = tr2rpy(tf_quat);
    r = RPY(1,1);
    p = RPY(1,2);
    y = RPY(1,3);

end
