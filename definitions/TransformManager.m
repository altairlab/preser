% Transform Manager
%  TransformManager(RotMatrix,TranVector)
%
% varargin = 1
% input :
%           Pose (pose.position, pose.orientation)
%
% varargin = 2
% input :
%           RotMatrix : Rotation Matrix 3x3
%           TranVector : Translation Vector 3x1

classdef TransformManager < handle
    properties(Access = public)
        homTransform                               % source coordinate frame
        two_frameTransform
        pose
    end
    
    methods
        function obj = TransformManager(varargin) % (1)pose,  (2) RotMatrix,TranVector
            
            if length(varargin) == 1
                tf_quat = quat2tform([varargin{1}.orientation.x,varargin{1}.orientation.y,varargin{1}.orientation.z,varargin{1}.orientation.w])
                tf_trans = [varargin{1}.position.x ; varargin{1}.position.y;varargin{1}.position.z]
                obj.homTransform = rt2tr(tf_quat(1:3,1:3) , tf_trans);
            end
            
            if length(varargin) == 2
                obj.homTransform = rt2tr(varargin{1},varargin{2});
            end
            
            obj.pose = data_pose();
            
        end
        
        
        
        
        % Returns the transform between two coordinate frames , current and
        % target frame
        function obj = lookupTransform_p(obj,target_frame)
            obj.two_frameTransform = inv(obj.homTransform) * target_frame;
        end
        
        
        function tf = getTransform_p(obj,target_frame,source_frame)
            tf = inv(source_frame) * target_frame;
            
            [temp_R , obj.pose.position] = tr2rt(tf);
            temp_quat = rotm2quat(temp_R);
            obj.pose.orientation = zeros(1,4);
            obj.pose.orientation(1:3) = temp_quat(2:4);
            obj.pose.orientation(4) = temp_quat(1);
        end
        
        
        
    end
end