% Returns segmented image and centroids

classdef data_centroids
    properties(Access = public)
        centroids
        segmented_image
    end
    methods
        function obj = data_centroids(segmented_image)
            segmented_image = segmented_image > 0.9;
            se = strel('disk',10);
            obj.segmented_image = imerode(segmented_image,se);
            
            
            cc = bwconncomp(obj.segmented_image); 
            stats = regionprops(cc, 'Area'); 
            idx = find([stats.Area] > 80 );                      % I will consider only REGION with 80+ pixel. 
            BW2 = ismember(labelmatrix(cc), idx);  
            
            s = regionprops(BW2,'centroid');
            obj.centroids = cat(1, s.Centroid);
            
                                    
%             
%             figure,imshow(segmented_image)
%             hold on
%             plot(obj.centroids(:,1),obj.centroids(:,2), 'r*')
%             hold off
% %             
        end
        
        
    end
end