% Usage -- with every Image N x N
%
% drawMapScript(Img, colorPixel,scale_gm)
% colorPixel -> 1 white . 0 black
% EXAMPLE
% Img = zeros(50,50); % or I2 = imread('procedures\Img_150x150.png');
% Img(1:50,1) = 1;
% Img(1:50,50) = 1;
% Img(50,1:50) = 1;
% Img(1,1:50) = 1;
% drawMapScript(Img,1,0.05);

function  drawMapScript(Img, colorPixel,scale_gm)

    if (nargin<1) || isempty(Img)
        disp('MISSING IMAGE');
        return
    end

    addpath('simulator/V-REP')
    addpath('common','common/ini_parser','definitions')

    % Create socket connection with the simulator
    vrep = remApi('remoteApi');
    vrep.simxFinish(-1);

    % Reading configuration
    ini = IniConfig();
    ini.ReadFile('config/Preser.ini');

    sim_address = ini.GetValues('simulator', 'socketaddr');
    sim_port = ini.GetValues('simulator', 'socketport');

    id = vrep.simxStart(sim_address, sim_port, true, false, 10000, 5);

    if id < 0
        fprintf('Failed connecting to remote API server. Exiting.');
        vrep.delete();
        return;
    end

    fprintf('Connection %d to remote API server open.\n', id);

    if (id > -1)
        disp('Connected to remote API server');
        scene = 'CreatingObjects.ttt';
        myPath = pwd;
        newPath = [myPath filesep 'simulator/scenes'];
        myScene = [newPath filesep scene];
        [res] = vrep.simxLoadScene(id,myScene,0,vrep.simx_opmode_oneshot_wait);
        vrchk(vrep, res);
        pause(1);
    end
    
%     Img = rgb2gray(Img);
%     Img = Img<100;

%     if(size(Img,3) == 3)
%        Img = rgb2gray(Img);
%        Img = Img<100;
%        disp('Coverted Image to grayscale')
%     end

    sizeImg = size(Img);
    Img = im2bw(Img);
%     figure, imshow(Img); title('Processing image...');
    width  = sizeImg(1);   % larghezza
    height = sizeImg(2);   % altezza

    CubeSize = [scale_gm , scale_gm ,0.5];

    vrep.simxStartSimulation(id, vrep.simx_opmode_oneshot_wait);

    packedData=vrep.simxPackFloats(CubeSize);
    [res]=vrep.simxSetStringSignal(id,'objCreation',packedData,vrep.simx_opmode_oneshot_wait) ;
    pause(3);
    disp('Cuboid created ...')

    if ( res ~= 0)
        disp('You failed this city')
        return
    end

    [res, cuboid] = vrep.simxGetObjectHandle(id,'Cuboid',vrep.simx_opmode_oneshot_wait);
    vrchk(vrep, res);
    [~, eulerAngles]=vrep.simxGetObjectOrientation(id,cuboid,-1,vrep.simx_opmode_oneshot_wait);
    sign = -1;
    CuboidHalfSize = CubeSize(1)/2 ;
    HandleCuboid = zeros(width,height);
    newFloorSize = scale_gm * width/2;
    ScaleFactor =  (width/2) / newFloorSize;

    for i = 1:width
        for j = 1:height
            %if(Img(i,j) >= colorPixel ) % 1 white . 0 black
			if(Img(i,j) == colorPixel )
                [~, HandleCuboid(i,j)] = vrep.simxCopyPasteObjects( id, cuboid, vrep.simx_opmode_oneshot_wait) ;
                vrep.simxSetObjectOrientation(id,HandleCuboid(i,j),-1, eulerAngles,vrep.simx_opmode_oneshot_wait);

                if( i <= width/2 && j <= height/2)
                    vrep.simxSetObjectPosition(id ,HandleCuboid(i,j),-1,[ sign*( newFloorSize + CuboidHalfSize - i/ScaleFactor)  ,  sign*(newFloorSize + CuboidHalfSize  - j/ScaleFactor)  , 0.05],vrep.simx_opmode_oneshot_wait) ;
                end

                if( i > width/2 && j <= height/2)
                    vrep.simxSetObjectPosition(id ,HandleCuboid(i,j),-1,[ (  -CuboidHalfSize  + (i - width/2)/ScaleFactor) ,sign*(newFloorSize + CuboidHalfSize - j/ScaleFactor) , 0.05],vrep.simx_opmode_oneshot_wait) ;
                end

                if( i <= width/2 && j > height/2)
                    vrep.simxSetObjectPosition(id ,HandleCuboid(i,j),-1,[sign*( newFloorSize + CuboidHalfSize  - i/ScaleFactor) , -CuboidHalfSize  + (j - height/2)/ScaleFactor , 0.05],vrep.simx_opmode_oneshot_wait) ;
                end

                if( i > width/2 && j > height/2)
                    vrep.simxSetObjectPosition(id ,HandleCuboid(i,j),-1,[ (  -CuboidHalfSize  + (i - width/2)/ScaleFactor) ,  -CuboidHalfSize  + (j - height/2)/ScaleFactor , 0.05],vrep.simx_opmode_oneshot_wait) ;
                end

                pause(0.2);
            end
        end
    end

end % end function