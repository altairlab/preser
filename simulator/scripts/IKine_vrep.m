[res , yTarget] = vrep.simxGetObjectHandle(id,'youBot_positionTipTarget',vrep.simx_opmode_oneshot)
%  pose Home position
arrayPos = [-0.7102; -0.0889; 0.5020]
arrayOri = [-4.65; -22.58; 77.88]

res = vrep.simxSetObjectOrientation(id, yTarget, -1, arrayOri', vrep.simx_opmode_oneshot)
res = vrep.simxSetObjectPosition(id, yTarget, -1, arrayPos', vrep.simx_opmode_oneshot)

% leave item
arrayPos = [-1.0763; -0.0620; 0.2772]
arrayOri = [-4.59; -22.58; 77.88]

% take item 
arrayPos = [0.1001; -0.1290; 0.2380]




% 
%  initPos = [0,0,0,0,0]
%  res = vrep.simxSetIntegerSignal(id, 'grasp1', 1, vrep.simx_opmode_oneshot_wait);
% api.armSetPos(1,[-0.05 ,0,-2.5, 0.5, 0]) % grasp
% api.gripperSetStat(1,0)                  % close gripper
% api.armSetPos(1,[-pi, -0.08,-2.5,0.3,0]) % load
% api.gripperSetStat(1,1)                  % open gripper

