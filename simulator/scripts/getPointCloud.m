function  pts = getPointCloud(id,vrep,camera)
            if  evalin('base','exist(''camera'')') == 1
%                 id = evalin('base','id');
%                 vrep = evalin('base','vrep');
%                 camera = evalin('base','camera');
            
%                 [res, resolution, rgb_image] = vrep.simxGetVisionSensorImage2(id, camera.rgb_sensor, 0,vrep.simx_opmode_oneshot_wait);
%                 vrchk(vrep, res);
%                 
                [res, det, auxData, auxPacketInfo] = vrep.simxReadVisionSensor(id, camera.depth_sensor, vrep.simx_opmode_oneshot_wait);
                vrchk(vrep, res, true);    
                width = auxData(auxPacketInfo(1)+1);
                height = auxData(auxPacketInfo(1)+2);
                pts = reshape(auxData((auxPacketInfo(1)+2+1):end), 4, width*height);
                
                             
%                 [TO DECIDE] CAMERA LOCATION
                pts = pts(1:3,pts(4,:) < 1 ); % 1 meter
                pts = rotx(pi/2) * pts;
%                 pts = rotx(1.57) * roty(1.57) * pts;
%                 figure,scatter3(pts(1,:),pts(2,:),pts(3,:),'.'); 
                
                
%                 Tf = robot_kine.robot_chain.fkine(robot.Arm(1).out_joint_positions);
%                 Rot_tf = t2r(Tf) ; 
%                 convertedPts = Rot_tf * pts(1:3,:) ;
%                 figure,scatter3(convertedPts(1,:),convertedPts(2,:),convertedPts(3,:),'.');

            end
            
end
        

% id = evalin('base','id');
% vrep = evalin('base','vrep');
% camera = evalin('base','camera');
% 
% pts = getPointCloud(id,vrep,camera);
% h_pcd = findobj('Tag','axes_pcd');
% scatter3(h_pcd,pts(1,:),pts(2,:),pts(3,:),'.')
