classdef GuiJoypad < handle
	properties(Access = protected)
		gui
		th
		max_vel
		max_ang_vel
	end
	
	properties(Access = public)
		out_twist_cmd;
	end
	
	methods
		function obj = GuiJoypad()
			obj.gui = PreserController();
			obj.out_twist_cmd = [0 0 0];
			obj.max_vel = 1.0;
			obj.max_ang_vel = 1.0;
		end
		
		function obj = show(obj)
			obj.gui = PreserController();
		end

		function obj = configure(obj)
			
		end
		
		function obj = start(obj)
			
		end
		
		function obj = update(obj)
			h = guidata(obj.gui);

			x = get(h.twistx);
			y = get(h.twisty);
			z = get(h.twistz);
			
			obj.out_twist_cmd = [x.Value y.Value z.Value];
		end
	end
end