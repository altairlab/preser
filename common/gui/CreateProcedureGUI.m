function varargout = CreateProcedureGUI(varargin)
% CREATEPROCEDUREGUI MATLAB code for CreateProcedureGUI.fig
%      CREATEPROCEDUREGUI, by itself, creates a new CREATEPROCEDUREGUI or raises the existing
%      singleton*.
%
%      H = CREATEPROCEDUREGUI returns the handle to a new CREATEPROCEDUREGUI or the handle to
%      the existing singleton*.
%
%      CREATEPROCEDUREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATEPROCEDUREGUI.M with the given input arguments.
%
%      CREATEPROCEDUREGUI('Property','Value',...) creates a new CREATEPROCEDUREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CreateProcedureGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CreateProcedureGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CreateProcedureGUI

% Last Modified by GUIDE v2.5 03-Dec-2015 16:35:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CreateProcedureGUI_OpeningFcn, ...
    'gui_OutputFcn',  @CreateProcedureGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CreateProcedureGUI is made visible.
function CreateProcedureGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CreateProcedureGUI (see VARARGIN)

% MULTI PLATFORM SETTINGS
set(findall(handles.figure1, '-property','FontSize'),'FontSize',10);
set(findall(handles.figure1, '-property','FontName'),'FontName','default');


handles.init_handle = [];
handles.proc_handle = [];
set(handles.init_handle,'Value',varargin{1});
set(handles.proc_handle,'Value',varargin{2});

set(findobj('Tag','rB_PlaneAngle_Arm'),'Enable', 'off');
set(findobj('Tag','rB_PlaneAngle_Base'),'Enable', 'off');
set(findobj('Tag','rB_PlaneAngle_Gripper'),'Enable', 'off');
set(findobj('Tag','rB_AngularVelocity_Arm'),'Enable', 'off');
set(findobj('Tag','rB_AngularVelocity_Base'),'Enable', 'off');
set(findobj('Tag','rB_AngularVelocity_Gripper'),'Enable', 'off');
set(findobj('Tag','rB_Torque_Arm'),'Enable', 'off');
set(findobj('Tag','rB_Torque_Base'),'Enable', 'off');
set(findobj('Tag','rB_Torque_Gripper'),'Enable', 'off');
set(findobj('Tag','rB_MotorStop_Arm'),'Enable', 'off');
set(findobj('Tag','rB_MotorStop_Base'),'Enable', 'off');
set(findobj('Tag','rB_MotorStop_Gripper'),'Enable', 'off');
set(findobj('Tag','rB_Twist_Base'),'Enable', 'off');

set(findobj('Tag','edit_LoadMap'),'Enable', 'off');
set(findobj('Tag','pB_LoadMap'),'Enable', 'off');
% Choose default command line output for CreateProcedureGUI
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CreateProcedureGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CreateProcedureGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rB_Joy.
function rB_Joy_Callback(hObject, eventdata, handles)

% --- Executes on button press in rB_Camera.
function rB_Camera_Callback(hObject, eventdata, handles)

% --- Executes on button press in rB_Laser.
function rB_Laser_Callback(hObject, eventdata, handles)

% --- Executes on button press in rB_Key.
function rB_Key_Callback(hObject, eventdata, handles)

% --- Executes on button press in pB_Save.
function pB_Save_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% if ~exist('procedures','dir')
% mkdir('procedures');
% end

[file,path] = uiputfile([pwd '/procedures/*.m'],'Save procedure name');
if isequal(file,0)
    delete(get(hObject,'parent')); return;
end

fid = fopen(fullfile(path,file),'w');

fprintf(fid,'robot.update();\n');
if (get(handles.rB_Joy,'Value') || get(handles.rB_Key,'Value') )   fprintf(fid,'joypad.update();\n'); fprintf(fid,'robot.Base(1).in_twist_cmd = joypad.out_twist_cmd;\n');end
if(get(handles.rB_Laser,'Value'))    fprintf(fid,'laser.update();\n');end
if(get(handles.rB_Traj,'Value')  )   fprintf(fid,'traj.update();\n'); fprintf(fid,'robot.Base(1).in_twist_cmd = traj.out_twist_cmd;\n');end

if(get(handles.rB_GMapping,'Value'))
    fprintf(fid,'slam.update();\n');
    fprintf(fid,'slam.in_odometry = robot.Base(1).out_odometry;\n');
    fprintf(fid,'slam.in_laser_data = laser.out_laser_points;\n');
end
if(get(handles.rB_ACML,'Value'))
    fprintf(fid,'amcl.update();\n');
    fprintf(fid,'amcl.in_odometry = robot.Base(1).out_odometry;\n');
    fprintf(fid,'amcl.in_laser_data = laser.out_laser_points;\n');
    fprintf(fid,'amcl.in_occupacy_grid = slam.out_occupacy_grid;\n');
end
if(get(handles.rB_AStar,'Value'))
    fprintf(fid,'path_planner.in_odometry = robot.Base(1).out_odometry;\n');
    if isempty(get(handles.edit_LoadMap,'String')) && ~ get(handles.rB_GMapping,'Value')
        warndlg('Missing Map -> Slam  Enabled ',' Warning')
        fprintf(fid,'path_planner.in_occupacy_grid = slam.out_occupacy_grid;\n');
    end
end
if get(handles.rB_DStar,'Value')
    fprintf(fid,'path_planner_1 = DStar();\n');
    fprintf(fid,'path_planner_1.in_odometry = robot.Base(1).out_odometry;\n');
    if isempty(get(handles.edit_LoadMap,'String')) && ~ get(handles.rB_GMapping,'Value')
        warndlg('Missing Map -> Slam  Enabled ',' Warning')
        fprintf(fid,'path_planner_1.in_occupacy_grid = slam.out_occupacy_grid;\n');
    end
end
fclose(fid);

% ------------------------------------------------ Init File
[~,file_b,~] = fileparts(file);
fid_I = fopen(fullfile(path,[file_b '_init.m']),'w');

if get(handles.rB_Joy,'Value')    fprintf(fid_I,'joypad = XBoxJoypad();\n');end
if get(handles.rB_Key,'Value')    fprintf(fid_I,'joypad = GuiJoypad();\n');end
if get(handles.rB_Traj,'Value')   fprintf(fid_I,'traj = TrajPlanner();\n');end
if get(handles.rB_Laser,'Value')    fprintf(fid_I,'laser = Hokuyo(vrep,id);\n');end

if get(handles.cB_Arm,'Value')
    if get(handles.rB_PlaneAngle_Arm,'Value')   fprintf(fid_I,'api.armSetCtrlModes(1,1);\n'); end
    if get(handles.rB_AngularVelocity_Arm,'Value')   fprintf(fid_I,'api.armSetCtrlModes(1,2);\n'); end
    if get(handles.rB_Torque_Arm,'Value')   fprintf(fid_I,'api.armSetCtrlModes(1,3);\n'); end
    if get(handles.rB_MotorStop_Arm,'Value')   fprintf(fid_I,'api.armSetCtrlModes(1,4);\n'); end
end
if get(handles.cB_Base,'Value')
    if get(handles.rB_PlaneAngle_Base,'Value')   fprintf(fid_I,'api.baseSetCtrlModes(1,1);\n'); end
    if get(handles.rB_AngularVelocity_Base,'Value')   fprintf(fid_I,'api.baseSetCtrlModes(1,2);\n'); end
    if get(handles.rB_Torque_Base,'Value')   fprintf(fid_I,'api.baseSetCtrlModes(1,3);\n'); end
    if get(handles.rB_MotorStop_Base,'Value')   fprintf(fid_I,'api.baseSetCtrlModes(1,4);\n'); end
    if get(handles.rB_Twist_Base,'Value')   fprintf(fid_I,'api.baseSetCtrlModes(1,5);\n'); end
end
if get(handles.cB_Gripper,'Value')
    if get(handles.rB_PlaneAngle_Gripper,'Value')   fprintf(fid_I,'api.gripperSetCtrlModes(1,1);\n'); end
    if get(handles.rB_AngularVelocity_Gripper,'Value')   fprintf(fid_I,'api.gripperSetCtrlModes(1,2);\n'); end
    if get(handles.rB_Torque_Gripper,'Value')   fprintf(fid_I,'api.gripperSetCtrlModes(1,3);\n'); end
    if get(handles.rB_MotorStop_Gripper,'Value')   fprintf(fid_I,'api.gripperSetCtrlModes(1,4);\n'); end
end

if get(handles.rB_GMapping,'Value')    fprintf(fid_I,'slam = GMapping();\n');end
if get(handles.rB_ACML,'Value')    fprintf(fid_I,'amcl = Amcl();\n');end
if get(handles.rB_AStar,'Value')    
    fprintf(fid_I,'path_planner = AStar();\n');
%     if ~isempty(handles.edit_LoadMap.String)  && ~handles.rB_GMapping.Value
    if ~isempty(get(handles.edit_LoadMap,'String'))  && ~ get(handles.rB_GMapping,'Value')
        fprintf(fid_I,'api.loadMap(''%s'');\n',handles.edit_LoadMap.String);
        fprintf(fid_I,'path_planner.in_occupacy_grid = static_map;\n');
    else
        fprintf(fid_I,'slam = GMapping();\n');
    end
end
if get(handles.rB_DStar,'Value')   
    fprintf(fid_I,'path_planner_1 = DStar();\n');
%     if ~isempty(handles.edit_LoadMap.String)  && ~handles.rB_GMapping.Value
    if ~isempty(get(handles.edit_LoadMap,'String'))  && ~ get(handles.rB_GMapping,'Value')
        fprintf(fid_I,'api.loadMap(''%s'');\n',handles.edit_LoadMap.String);
        fprintf(fid_I,'path_planner.in_occupacy_grid = static_map;\n');
    end
end
fclose(fid_I);

% -------------------------------------------------end Init file
% configuration

%Save to file Ini
evalin('base',strcat(strcat('init_proc_name ='' ', [file_b '_init']),''';'));
evalin('base',strcat(strcat('proc_name ='' ',file_b),''';'));
evalin('base','api.saveConfigToFile();');
set(get(handles.init_handle,'Value'),'string',[file_b '_init']);
set(get(handles.proc_handle,'Value'),'string',file_b);
% update Label in PreserController
set(findobj('Tag','selected_init_procedure_name'),'String', evalin('base','init_proc_name'));
set(findobj('Tag','selected_procedure_name'),'String', evalin('base','proc_name'));
close(handles.figure1);
%delete(get(hObject,'parent'));


% --- Executes on button press in rB_GMapping.
function rB_GMapping_Callback(hObject, eventdata, handles)
% hObject    handle to rB_GMapping (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rB_GMapping


% --- Executes on button press in rB_AStar.
function rB_AStar_Callback(hObject, eventdata, handles)
if get(handles.rB_AStar,'Value') % (handles.rB_AStar.Value) % Matlab 2015 +
    set(findobj('Tag','edit_LoadMap'),'Enable', 'on');
    set(findobj('Tag','pB_LoadMap'),'Enable', 'on');
    set(findobj('Tag','rB_DStar'),'Value', 0);
else
    set(findobj('Tag','edit_LoadMap'),'Enable', 'off');
    set(findobj('Tag','pB_LoadMap'),'Enable', 'off');
%     set(findobj('Tag','rB_DStar'),'Value', 1);
    
end

% --- Executes on button press in rB_DStar.
function rB_DStar_Callback(hObject, eventdata, handles)
if get(handles.rB_DStar,'Value') % (handles.rB_DStar.Value)
    set(findobj('Tag','edit_LoadMap'),'Enable', 'on');
    set(findobj('Tag','pB_LoadMap'),'Enable', 'on');
    set(findobj('Tag','rB_AStar'),'Value', 0);
else
    set(findobj('Tag','edit_LoadMap'),'Enable', 'off');
    set(findobj('Tag','pB_LoadMap'),'Enable', 'off');
%     set(findobj('Tag','rB_AStar'),'Value', 1);
end

% --- Executes on button press in rB_ACML.
function rB_ACML_Callback(hObject, eventdata, handles)
% hObject    handle to rB_ACML (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rB_ACML


% --- Executes on button press in pB_Cancel.
function pB_Cancel_Callback(hObject, eventdata, handles)
close(handles.figure1);


% --- Executes on button press in cB_Arm.
function cB_Arm_Callback(hObject, eventdata, handles)
if get(handles.cB_Arm,'Value') %(handles.cB_Arm.Value)
    set(findobj('Tag','rB_PlaneAngle_Arm'),'Enable', 'on');
    set(findobj('Tag','rB_AngularVelocity_Arm'),'Enable', 'on');
    set(findobj('Tag','rB_Torque_Arm'),'Enable', 'on');
    set(findobj('Tag','rB_MotorStop_Arm'),'Enable', 'on');
else
    set(findobj('Tag','rB_PlaneAngle_Arm'),'Enable', 'off');
    set(findobj('Tag','rB_AngularVelocity_Arm'),'Enable', 'off');
    set(findobj('Tag','rB_Torque_Arm'),'Enable', 'off');
    set(findobj('Tag','rB_MotorStop_Arm'),'Enable', 'off');
end

% --- Executes on button press in cB_Base.
function cB_Base_Callback(hObject, eventdata, handles)
if get(handles.cB_Base,'Value') % (handles.cB_Base.Value)
    set(findobj('Tag','rB_PlaneAngle_Base'),'Enable', 'on');
    set(findobj('Tag','rB_AngularVelocity_Base'),'Enable', 'on');
    set(findobj('Tag','rB_Torque_Base'),'Enable', 'on');
    set(findobj('Tag','rB_MotorStop_Base'),'Enable', 'on');
    set(findobj('Tag','rB_Twist_Base'),'Enable', 'on');
else
    set(findobj('Tag','rB_PlaneAngle_Base'),'Enable', 'off');
    set(findobj('Tag','rB_AngularVelocity_Base'),'Enable', 'off');
    set(findobj('Tag','rB_Torque_Base'),'Enable', 'off');
    set(findobj('Tag','rB_MotorStop_Base'),'Enable', 'off');
    set(findobj('Tag','rB_Twist_Base'),'Enable', 'off');
end

% --- Executes on button press in cB_Gripper.
function cB_Gripper_Callback(hObject, eventdata, handles)
if get(handles.cB_Gripper,'Value') %(handles.cB_Gripper.Value)
    set(findobj('Tag','rB_PlaneAngle_Gripper'),'Enable', 'on');
    set(findobj('Tag','rB_AngularVelocity_Gripper'),'Enable', 'on');
    set(findobj('Tag','rB_Torque_Gripper'),'Enable', 'on');
    set(findobj('Tag','rB_MotorStop_Gripper'),'Enable', 'on');
else
    set(findobj('Tag','rB_PlaneAngle_Gripper'),'Enable', 'off');
    set(findobj('Tag','rB_AngularVelocity_Gripper'),'Enable', 'off');
    set(findobj('Tag','rB_Torque_Gripper'),'Enable', 'off');
    set(findobj('Tag','rB_MotorStop_Gripper'),'Enable', 'off');
    
end



function edit_LoadMap_Callback(hObject, eventdata, handles)
% hObject    handle to edit_LoadMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_LoadMap as text
%        str2double(get(hObject,'String')) returns contents of edit_LoadMap as a double


% --- Executes during object creation, after setting all properties.
function edit_LoadMap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_LoadMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pB_LoadMap.
function pB_LoadMap_Callback(hObject, eventdata, handles)
% hObject    handle to pB_LoadMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[MapName,PathName] = uigetfile('*.mat','Select Map');
set(handles.edit_LoadMap ,'String',MapName);

