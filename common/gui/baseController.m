function varargout = baseController(varargin)
% BASECONTROLLER MATLAB code for baseController.fig
%      BASECONTROLLER, by itself, creates a new BASECONTROLLER or raises the existing
%      singleton*.
%
%      H = BASECONTROLLER returns the handle to a new BASECONTROLLER or the handle to
%      the existing singleton*.
%
%      BASECONTROLLER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BASECONTROLLER.M with the given input arguments.
%
%      BASECONTROLLER('Property','Value',...) creates a new BASECONTROLLER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before baseController_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to baseController_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help baseController

% Last Modified by GUIDE v2.5 16-Jun-2015 14:58:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @baseController_OpeningFcn, ...
                   'gui_OutputFcn',  @baseController_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before baseController is made visible.
function baseController_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to baseController (see VARARGIN)

% Choose default command line output for baseController
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes baseController wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(findobj('Tag','twistx'),'Value', 0);
set(findobj('Tag','twistx'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twisty'),'Value', 0);
set(findobj('Tag','twisty'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twistz'),'Value', 0);
set(findobj('Tag','twistz'),'String', strcat(num2str(0),' rad/s'));

% --- Outputs from this function are returned to the command line.
function varargout = baseController_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in xpos.
function xpos_Callback(hObject, eventdata, handles)
% hObject    handle to xpos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistx'),'Value') + 0.1;
set(findobj('Tag','twistx'),'Value', twist);
set(findobj('Tag','twistx'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in xneg.
function xneg_Callback(hObject, eventdata, handles)
% hObject    handle to xneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistx'),'Value') - 0.1;
set(findobj('Tag','twistx'),'Value', twist);
set(findobj('Tag','twistx'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in ypos.
function ypos_Callback(hObject, eventdata, handles)
% hObject    handle to ypos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twisty'),'Value') + 0.1;
set(findobj('Tag','twisty'),'Value', twist);
set(findobj('Tag','twisty'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in yneg.
function yneg_Callback(hObject, eventdata, handles)
% hObject    handle to yneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twisty'),'Value') - 0.1;
set(findobj('Tag','twisty'),'Value', twist);
set(findobj('Tag','twisty'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in zneg.
function zneg_Callback(hObject, eventdata, handles)
% hObject    handle to zneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistz'),'Value') - 0.1;
set(findobj('Tag','twistz'),'Value', twist);
set(findobj('Tag','twistz'),'String', strcat(num2str(twist),' rad/s'));

% --- Executes on button press in zpos.
function zpos_Callback(hObject, eventdata, handles)
% hObject    handle to zpos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistz'),'Value') + 0.1;
set(findobj('Tag','twistz'),'Value', twist);
set(findobj('Tag','twistz'),'String', strcat(num2str(twist),' rad/s'));


% --- Executes on button press in stop.
function stop_Callback(hObject, eventdata, handles)
% hObject    handle to stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(findobj('Tag','twistx'),'Value', 0);
set(findobj('Tag','twistx'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twisty'),'Value', 0);
set(findobj('Tag','twisty'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twistz'),'Value', 0);
set(findobj('Tag','twistz'),'String', strcat(num2str(0),' rad/s'));
