function varargout = LoadProcedureGUI(varargin)
% LOADPROCEDUREGUI MATLAB code for LoadProcedureGUI.fig
%      LOADPROCEDUREGUI, by itself, creates a new LOADPROCEDUREGUI or raises the existing
%      singleton*.
%
%      H = LOADPROCEDUREGUI returns the handle to a new LOADPROCEDUREGUI or the handle to
%      the existing singleton*.
%
%      LOADPROCEDUREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADPROCEDUREGUI.M with the given input arguments.
%
%      LOADPROCEDUREGUI('Property','Value',...) creates a new LOADPROCEDUREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LoadProcedureGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LoadProcedureGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LoadProcedureGUI

% Last Modified by GUIDE v2.5 02-Dec-2015 18:17:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LoadProcedureGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @LoadProcedureGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT

% --- Executes just before LoadProcedureGUI is made visible.
function LoadProcedureGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LoadProcedureGUI (see VARARGIN)

% MULTI PLATFORM SETTINGS
set(findall(handles.figure1, '-property','FontSize'),'FontSize',10);
set(findall(handles.figure1, '-property','FontName'),'FontName','default');

handles.init_handle = [];
handles.proc_handle = [];
set(handles.init_handle,'Value',varargin{1});
set(handles.proc_handle,'Value',varargin{2});

% Choose default command line output for LoadProcedureGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = LoadProcedureGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pB_Init.
function pB_Init_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Init (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName_init,PathName] = uigetfile('*.m','Select the INIT procedure');
set(handles.edit1 ,'String',FileName_init);


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% REMEMBER TO CHANGE IN INSPECTOR THE #num FIGURE
% Hint: delete(hObject) closes the figure

guidata(hObject,handles);
delete(hObject);

% --- Executes on button press in pB_procedure.
function pB_procedure_Callback(hObject, eventdata, handles)
% hObject    handle to pB_procedure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName_procedure,PathName] = uigetfile('*.m','Select the INIT procedure');
set(handles.edit2 ,'String',FileName_procedure);

% --- Executes on button press in pB_Load.
function pB_Load_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
init_procedure = get(handles.edit1,'string');
[~,name_init,~] = fileparts(init_procedure) ;

procedure = get(handles.edit2,'string');
[~,name_proc,~] = fileparts(procedure) ;

if isempty(procedure) && isempty(init_procedure)
  warndlg('Missing Procedures ',' Warning')
else
	evalin('base',strcat(strcat('init_proc_name ='' ', name_init),''';'));
	evalin('base',strcat(strcat('proc_name ='' ',name_proc),''';'));
	evalin('base','api.saveConfigToFile();');
end

set(get(handles.init_handle,'Value'),'string',name_init);
set(get(handles.proc_handle,'Value'),'string',name_proc);
% update Label in PreserController
set(findobj('Tag','selected_init_procedure_name'),'String', evalin('base','init_proc_name'));
set(findobj('Tag','selected_procedure_name'),'String', evalin('base','proc_name'));

close(handles.figure1);

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);
