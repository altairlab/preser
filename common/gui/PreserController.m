function varargout = PreserController(varargin)
% PreserController MATLAB code for PreserController.fig
%      PreserController, by itself, creates a new PreserController or raises the existing
%      singleton*.
%
%      H = PreserController returns the handle to a new PreserController or the handle to
%      the existing singleton*.
%
%      PreserController('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PreserController.M with the given input arguments.
%
%      PreserController('Property','Value',...) creates a new PreserController or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PreserController_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PreserController_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PreserController

% Last Modified by GUIDE v2.5 01-Dec-2015 13:47:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name', mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PreserController_OpeningFcn, ...
    'gui_OutputFcn',  @PreserController_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before PreserController is made visible.
function PreserController_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PreserController (see VARARGIN)

% MULTI PLATFORM SETTINGS
set(findall(handles.figure1, '-property','FontSize'),'FontSize',10);
set(findall(handles.figure1, '-property','FontName'),'FontName','default');

ZIn_img = imread('common/gui/ZoomIn.png');
% ZOut_img = imread('common/gui/ZoomOut.png');
set(findobj('Tag','tB_ZoomIn'),'cdata', ZIn_img);
% set(findobj('Tag','tB_ZoomOut'),'cdata', ZOut_img);

handles.selectScene = [];
handles.startScene = [];
% Choose default command line output for PreserController
handles.output = hObject;

TestFiles = dir([pwd '/robots']);
myRobot{1} = 'Choose robot';
c=2;
for i = 1:length(TestFiles)
    filename = TestFiles(i).name;
    if isequal(filename,'Wam')
        filename = 'WAM';
    end
    if(filename ~= '.')
        myRobot{c} = filename;
        if(strcmp(evalin('base','robot_name'),filename))
            set(handles.menuRobot,'Value',c);
        end
        c = c + 1;
    end
end

set(findobj('Tag','selected_init_procedure_name'),'String', evalin('base','init_proc_name'));
set(findobj('Tag','selected_procedure_name'),'String', evalin('base','proc_name'));

% set(findobj('Tag','cB_Pad'),'Enable', 'off');
set(findobj('Tag','cB_Pad'),'Value', evalin('base','joy'));

set(findobj('Tag','twistx'),'Value', 0);
set(findobj('Tag','twistx'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twisty'),'Value', 0);
set(findobj('Tag','twisty'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twistz'),'Value', 0);
set(findobj('Tag','twistz'),'String', strcat(num2str(0),' rad/s'));

% we can comment this , On start we do the check
% c=textread([get(handles.selected_init_procedure_name,'String') '.m'],'%s','delimiter','\n');
% foundJoypad = find(~cellfun(@isempty,strfind(c,'XBoxJoypad()')));
% if ~isempty(foundJoypad)
%     set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'off')
% else
%     set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on')
% end

set(handles.menuRobot,'string',myRobot);

Color_tb =   { 'White','Silver','Gray','Black','Red','Maroon','Yellow','Olive','Lime','Green','Aqua','Teal' , 'Blue' ,'Navy' ,'Magenta' ,'Purple', 'Orange' };

set(handles.popup_color,'string',Color_tb);


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PreserController wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = PreserController_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in menuRobot.
function menuRobot_Callback(hObject, eventdata, handles)
% Hints: contents = cellstr(get(hObject,'String')) returns menuRobot contents as cell array
%        contents{get(hObject,'Value')} returns selected item from menuRobot

robotValue = get(handles.menuRobot,'Value');
namesRobot = get(handles.menuRobot,'String');
selRobot = namesRobot(robotValue);
evalin('base',strcat(strcat('robot_name ='' ',cell2mat(selRobot)),''';'));
evalin('base','api.saveConfigToFile();');

% --- Executes during object creation, after setting all properties.
function menuRobot_CreateFcn(hObject, eventdata, handles)
handles.menuRobot=hObject;  %pass handles the popup menu object
guidata(hObject, handles);

% Popupmenu controls usually have a white background on Windows. See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pB_Settings.
function pB_Settings_Callback(hObject, eventdata, handles)
SettingsGUI();

% --- Executes on button press in pB_CreateProcedure.
function pB_CreateProcedure_Callback(hObject, eventdata, handles)
CreateProcedureGUI(handles.selected_init_procedure_name,handles.selected_procedure_name);

% --- Executes on button press in pB_LoadProcedure.
function pB_LoadProcedure_Callback(hObject, eventdata, handles)
LoadProcedureGUI(handles.selected_init_procedure_name,handles.selected_procedure_name);


% --- Executes on button press in pB_Start.
function pB_Start_Callback(hObject, eventdata, handles)

handles.selectScene = evalin('base','scene;');
handles.startScene = evalin('base','start_scene;');

set(findobj('Tag','menuRobot'),'Enable', 'off');
set(findobj('Tag','pB_Settings'),'Enable', 'off');
set(findobj('Tag','pB_CreateProcedure'),'Enable', 'off');
set(findobj('Tag','pB_LoadProcedure'),'Enable', 'off');

if isequal(handles.selectScene,'CreatingObjects') && isequal(handles.startScene,1)
    
    CreateSceneGUI();
    
else
    
    c=textread([ get(handles.selected_init_procedure_name,'String') '.m'],'%s','delimiter','\n');
    foundJoypad = find(~cellfun(@isempty,strfind(c,'XBoxJoypad()')));
    if ~isempty(foundJoypad)
        set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'off')
    else
        set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on')
    end
    
    %     foundTraj = find(~cellfun(@isempty,strfind(c,'traj')));
    %     if ~isempty(foundTraj)
    %         set(findobj('Tag','cB_Pad'),'Enable', 'on');
    %     else
    %         set(findobj('Tag','cB_Pad'),'Enable', 'off');
    %     end
    
    evalin('base','start_preser');
    set(findobj('Tag','pB_Start'),'Enable', 'off');
    set(findobj('Tag','pB_Stop'),'Enable', 'on');
    
end

% --- Executes on button press in pB_Stop.
function pB_Stop_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(findobj('Tag','menuRobot'),'Enable', 'on');
set(findobj('Tag','pB_Settings'),'Enable', 'on');
set(findobj('Tag','pB_CreateProcedure'),'Enable', 'on');
set(findobj('Tag','pB_LoadProcedure'),'Enable', 'on');

% evalin('base','if ~exist(''hilas'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''checkExist'',temp)')
if  evalin('base','exist(''hilas'')') == 1
    evalin('base','hilas.stopRun();');
    set(findobj('Tag','pB_Start'),'Enable', 'on');
    set(findobj('Tag','pB_Stop'),'Enable', 'off');
end
% --- Executes on button press in xpos.
function xpos_Callback(hObject, eventdata, handles)
% hObject    handle to xpos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistx'),'Value') + 0.1;
set(findobj('Tag','twistx'),'Value', twist);
set(findobj('Tag','twistx'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in xneg.
function xneg_Callback(hObject, eventdata, handles)
% hObject    handle to xneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistx'),'Value') - 0.1;
set(findobj('Tag','twistx'),'Value', twist);
set(findobj('Tag','twistx'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in ypos.
function ypos_Callback(hObject, eventdata, handles)
% hObject    handle to ypos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twisty'),'Value') + 0.1;
set(findobj('Tag','twisty'),'Value', twist);
set(findobj('Tag','twisty'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in yneg.
function yneg_Callback(hObject, eventdata, handles)
% hObject    handle to yneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twisty'),'Value') - 0.1;
set(findobj('Tag','twisty'),'Value', twist);
set(findobj('Tag','twisty'),'String', strcat(num2str(twist),' m/s'));

% --- Executes on button press in zneg.
function zneg_Callback(hObject, eventdata, handles)
% hObject    handle to zneg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistz'),'Value') - 0.1;
set(findobj('Tag','twistz'),'Value', twist);
set(findobj('Tag','twistz'),'String', strcat(num2str(twist),' rad/s'));

% --- Executes on button press in zpos.
function zpos_Callback(hObject, eventdata, handles)
% hObject    handle to zpos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
twist = get(findobj('Tag','twistz'),'Value') + 0.1;
set(findobj('Tag','twistz'),'Value', twist);
set(findobj('Tag','twistz'),'String', strcat(num2str(twist),' rad/s'));

% --- Executes on button press in stop.
function stop_Callback(hObject, eventdata, handles)
% hObject    handle to stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(findobj('Tag','twistx'),'Value', 0);
set(findobj('Tag','twistx'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twisty'),'Value', 0);
set(findobj('Tag','twisty'),'String', strcat(num2str(0),' m/s'));
set(findobj('Tag','twistz'),'Value', 0);
set(findobj('Tag','twistz'),'String', strcat(num2str(0),' rad/s'));

% --- Executes on button press in plotMap.
function plotMap_Callback(hObject, eventdata, handles)
% hObject    handle to plotMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
iptsetpref('ImshowAxesVisible','on');
grid on;

if(evalin('base','exist(''slam'')') == 1)
    imshow(evalin('base','slam.out_occupacy_grid.data'),'Parent', handles.axes1);
    if(evalin('base','exist(''robot'')') == 1)
        if(evalin('base','exist(''amcl'')') == 1)
            evalin('base','api.overlaySlamData(slam,robot,amcl);');
        else
            evalin('base','api.overlaySlamData(slam,robot);');
        end
    end
else
    if evalin('base','exist(''static_map'')') == 1
        imshow(evalin('base','static_map.data'),'Parent', handles.axes1);
        if(evalin('base','exist(''amcl'')') == 1)
            evalin('base','api.overlayLocalizationData(static_map,amcl,robot);');
        else
            evalin('base','api.overlayRobotData(static_map,robot);');
        end
    end
end

% --- Executes on button press in pB_Resume.
function pB_Resume_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Resume (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% v = evalin('base','exist(''hilas'')')
% evalin('base','if ~exist(''hilas'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''checkExist'',temp)')
if  evalin('base','exist(''hilas'')') == 1
    evalin('base','hilas.resume();');
end

% --- Executes on button press in pB_Pause.
function pB_Pause_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% evalin('base','if ~exist(''hilas'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''checkExist'',temp)')
if  evalin('base','exist(''hilas'')') == 1
    evalin('base','hilas.suspend();');
end

% --- Executes on button press in p_B_saveMap.
function p_B_saveMap_Callback(hObject, eventdata, handles)
% hObject    handle to p_B_saveMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% evalin('base','if ~exist(''slam'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''slamExist'',temp)')
if  evalin('base','exist(''slam'')') == 1
    evalin('base','api.saveMap(slam)');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in startPlanner.
function startPlanner_Callback(hObject, eventdata, handles)
% evalin('base','if ~exist(''path_planner'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''pathExist'',temp)')
if evalin('base','exist(''path_planner'')') == 1
    evalin('base','path_planner.getPath();');
end

function startPlannerAndTraj_Callback(hObject, eventdata, handles)
% evalin('base','if ~exist(''path_planner'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''pathExist'',temp)')
% evalin('base','if ~exist(''traj'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''trajExist'',temp)')
pathExist = evalin('base','exist(''path_planner'')');
trajExist = evalin('base','exist(''traj'')');
if pathExist
    evalin('base','path_planner.getPath();');
    if trajExist
        evalin('base','traj.generateTraj(path_planner.grid2Cart(), 0.01, 2.0);');
    end
end

function cB_Pad_Callback(hObject,eventdata,handles)
if  evalin('base','exist(''hilas'')') == 1
    
    if get(handles.cB_Pad,'Value')
        evalin('base','hilas.suspend();');
        eval('base','assignin(''base'',''joy'',1)')
        evalin('base','hilas.resume();');
    else
        eval('base','assignin(''base'',''joy'',0)')
    end
    
end

function tB_ZoomIn_Callback(hObject, eventdata, handles)
togglestate = get(hObject,'Value');
% togglestate = hObject.Value
switch togglestate
    case 1
        % Toggle on, turn on zoom
        zoom(handles.axes1, 'on')
    case 0
        % Toggle off, turn off zoom
        zoom(handles.axes1, 'off')
end

function tB_ZoomOut_Callback(hObject, eventdata, handles)
zoom out
% togglestate = get(hObject,'Value');
% switch togglestate
%     case 1
%         % Toggle on, turn on zoom
%         zoom(handles.axes1, 'on')
%     case 0
%         % Toggle off, turn off zoom
%         zoom(handles.axes1, 'off')
% end
%

function pB_Update_Callback(hObject, eventdata, handles)
% hObject    handle to p_B_saveMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% evalin('base','if ~exist(''slam'',''var'');temp=0; else; temp=1; end;assignin(''caller'',''slamExist'',temp)')
if(evalin('base','exist(''camera'')') == 1)
    %     h_rgb = findobj('Tag','axes_rgb');
    imshow(evalin('base','camera.out_rgb_buffer'),'Parent',handles.axes_rgb);
    drawnow;
    
    col_seg = ColorSegmentation(evalin('base','camera.out_rgb_buffer'));
    
    colorValue = get(handles.popup_color,'Value');
    colorName = get(handles.popup_color,'String');
    selColor = colorName(colorValue);
    
    col_seg.getSegmentedRegion( selColor);
    col_seg.getPlot(handles.axes_segReg);
    
    assignin('base','Segmentation',col_seg);
    
end


function pB_pointcloud_Callback(hObject, eventdata, handles)
if(evalin('base','exist(''camera'')') == 1)
    pts = getPointCloud(evalin('base','id'),evalin('base','vrep'),evalin('base','camera'));
    % imshow(evalin('base','camera.out_rgb_buffer'),'Parent',handles.axes_rgb);
    scatter3(handles.axes_segReg,pts(1,:),pts(2,:),pts(3,:),'.');
    view(handles.axes_segReg,95,24)
end

function pB_Grasp_Callback(hObject, eventdata, handles)
id = evalin('base','id');
vrep = evalin('base','vrep');
if(evalin('base','exist(''camera'')') == 1)
    colorValue = get(handles.popup_color,'Value');
    colorName = get(handles.popup_color,'String');
    selColor = colorName(colorValue);
    res = vrep.simxSetIntegerSignal(id, 'grasp1', 1, vrep.simx_opmode_oneshot_wait);
    if isequal(selColor{1},'Blue')
        res = vrep.simxSetIntegerSignal(id, 'grasp1', 1, vrep.simx_opmode_oneshot_wait);
    end
    if isequal(selColor{1},'Orange')
        res = vrep.simxSetIntegerSignal(id, 'grasp3', 1, vrep.simx_opmode_oneshot_wait);
    end
    if isequal(selColor{1},'Green')
        res = vrep.simxSetIntegerSignal(id, 'grasp2', 1, vrep.simx_opmode_oneshot_wait);
    end
end