function varargout = SettingsGUI(varargin)
% SETTINGSGUI MATLAB code for SettingsGUI.fig
%      SETTINGSGUI, by itself, creates a new SETTINGSGUI or raises the existing
%      singleton*.
%
%      H = SETTINGSGUI returns the handle to a new SETTINGSGUI or the handle to
%      the existing singleton*.
%
%      SETTINGSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETTINGSGUI.M with the given input arguments.
%
%      SETTINGSGUI('Property','Value',...) creates a new SETTINGSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SettingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SettingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SettingsGUI

% Last Modified by GUIDE v2.5 30-Nov-2015 19:57:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @SettingsGUI_OpeningFcn, ...
    'gui_OutputFcn',  @SettingsGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SettingsGUI is made visible.
function SettingsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SettingsGUI (see VARARGIN)

% MULTI PLATFORM SETTINGS
set(findall(handles.figure1, '-property','FontSize'),'FontSize',10);
set(findall(handles.figure1, '-property','FontName'),'FontName','default');

% Choose default command line output for SettingsGUI
handles.output = hObject;

set(handles.checkbox_thread,'Value',evalin('base','start_thread;'));
set(handles.edit_threadSleep,'String',num2str(evalin('base','thread_sleep;')));
set(handles.checkbox_matlabWarning,'Value',evalin('base','matlab_warning;'));
set(handles.checkbox_javaLog,'Value',evalin('base','java_log;'));
set(handles.checkbox_MCLog,'Value',evalin('base','matlabcontrol_log;'));
set(handles.edit_socketAddr,'String',evalin('base','sim_address;'));
set(handles.edit_socketPort,'String',num2str(evalin('base','sim_port;')));
set(handles.edit_scene,'String',evalin('base','scene;'));
set(handles.checkbox_startScene,'Value',evalin('base','start_scene;'));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SettingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function setWhiteBackground(hObject)
% Hint: edit controls usually have a white background on Windows. See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Outputs from this function are returned to the command line.
function varargout = SettingsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes during object creation, after setting all properties.
function edit_threadSleep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_threadSleep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

setWhiteBackground(hObject);

% --- Executes during object creation, after setting all properties.
function edit_socketAddr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_socketAddr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

setWhiteBackground(hObject);

% --- Executes during object creation, after setting all properties.
function edit_socketPort_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_socketPort (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

setWhiteBackground(hObject);

% --- Executes during object creation, after setting all properties.
function edit_scene_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_scene (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

setWhiteBackground(hObject);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% REMEMBER TO CHANGE IN INSPECTOR THE #num FIGURE
% Hint: delete(hObject) closes the figure

guidata(hObject,handles);
delete(hObject);

% --- Executes on button press in pB_exploreScene.
function pB_exploreScene_Callback(hObject, eventdata, handles)
% hObject    handle to pB_exploreScene (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[sceneName,PathName] = uigetfile('*.ttt','Select scene');
[~,sceneName,~] = fileparts(sceneName) ;
if (sceneName == 0)
   set(handles.edit_scene ,'String','CreatingObjects'); 
else
    set(handles.edit_scene ,'String',[sceneName '.ttt']);
end


% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

evalin('base',strcat(strcat('start_thread = ',num2str(get(handles.checkbox_thread,'Value'))),';'));
evalin('base',strcat(strcat('thread_sleep = ',get(handles.edit_threadSleep,'string')),';'));
evalin('base',strcat(strcat('matlab_warning = ',num2str(get(handles.checkbox_matlabWarning,'Value'))),';'));
evalin('base',strcat(strcat('java_log = ',num2str(get(handles.checkbox_javaLog,'Value'))),';'));
evalin('base',strcat(strcat('matlabcontro_log = ',num2str(get(handles.checkbox_MCLog,'Value'))),';'));
evalin('base',strcat(strcat('sim_address ='' ',get(handles.edit_socketAddr,'string')),''';'));
evalin('base',strcat(strcat('sim_port = ',get(handles.edit_socketPort,'string')),';'));
evalin('base',strcat(strcat('start_scene = ',num2str(get(handles.checkbox_startScene,'Value'))),';'));
evalin('base',strcat(strcat('scene ='' ',get(handles.edit_scene,'string')),''';'));

evalin('base','api.saveConfigToFile();');
close(handles.figure1);

% --- Executes on button press in cancelbutton.
function cancelbutton_Callback(hObject, eventdata, handles)
% hObject    handle to cancelbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);
