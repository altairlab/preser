function varargout = CreateSceneGUI(varargin)
% CREATESCENEGUI MATLAB code for CreateSceneGUI.fig
%      CREATESCENEGUI, by itself, creates a new CREATESCENEGUI or raises the existing
%      singleton*.
%
%      H = CREATESCENEGUI returns the handle to a new CREATESCENEGUI or the handle to
%      the existing singleton*.
%
%      CREATESCENEGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATESCENEGUI.M with the given input arguments.
%
%      CREATESCENEGUI('Property','Value',...) creates a new CREATESCENEGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CreateSceneGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CreateSceneGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CreateSceneGUI

% Last Modified by GUIDE v2.5 04-Dec-2015 17:40:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @CreateSceneGUI_OpeningFcn, ...
    'gui_OutputFcn',  @CreateSceneGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CreateSceneGUI is made visible.
function CreateSceneGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CreateSceneGUI (see VARARGIN)

% MULTI PLATFORM SETTINGS
set(findall(handles.figure1, '-property','FontSize'),'FontSize',10);
set(findall(handles.figure1, '-property','FontName'),'FontName','default');

handles.Img = [];
handles.sceneImg = [];
handles.path = [];
% Choose default command line output for CreateSceneGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CreateSceneGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CreateSceneGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pB_Image.
function pB_Image_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles.sceneImg,handles.path] = uigetfile({'*.png;*.jpg'},'Select the Images');
set(handles.edit1 ,'String',handles.sceneImg);

if ~isequal(handles.sceneImg,' ') & ~isequal(handles.sceneImg,0)
    handles.Img = imread([handles.path handles.sceneImg]);
    handles.Img = rgb2gray(handles.Img);
    handles.Img = handles.Img<100;
    
    %  imshow(handles.sceneImg,'Parent', findobj('Tag','axes1'));
    h = findobj('Tag','axes1');
    imshow(handles.Img,'Parent', h); title(h,'Processing Image...');
    
end
guidata(hObject, handles);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pB_Create.
function pB_Create_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Create (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

colorPixel = get(handles.rB_White,'Value');
cubeSize = num2str(get(handles.edit_CubeSize,'string'));
cubeSize = str2double(cubeSize);
% [~,Img,~] = fileparts(handles.sceneImg)
% Img = imread([handles.path handles.sceneImg]);
if ~isequal(handles.edit_CubeSize,' ')
    drawMapScript(handles.Img,colorPixel,cubeSize)
end

% --- Executes on button press in pB_Cancel.
function pB_Cancel_Callback(hObject, eventdata, handles)
% hObject    handle to pB_Cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);



function edit_CubeSize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_CubeSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_CubeSize as text
%        str2double(get(hObject,'String')) returns contents of edit_CubeSize as a double


% --- Executes during object creation, after setting all properties.
function edit_CubeSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_CubeSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
