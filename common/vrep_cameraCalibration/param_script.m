function cameraParams = param_script()

% Define images to process
imageFileNames = {'/home/andrea/preser/common/vrep_cameraCalibration/checkboard01.png',...
    '/home/andrea/preser/common/vrep_cameraCalibration/checkboard02.png',...
    '/home/andrea/preser/common/vrep_cameraCalibration/checkboard03.png',...
    };

% Detect checkerboards in images
[imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames);
imageFileNames = imageFileNames(imagesUsed);

% Generate world coordinates of the corners of the squares
squareSize = 300;  % in units of 'mm'
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera
[cameraParams, imagesUsed, estimationErrors] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', false, 'EstimateTangentialDistortion', false, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'mm');

% View reprojection errors - PLOT
% h1=figure; showReprojectionErrors(cameraParams, 'BarGraph');

% Visualize pattern locations
% h2=figure; showExtrinsics(cameraParams, 'CameraCentric');

% Display parameter estimation errors
% displayErrors(estimationErrors, cameraParams);

% For example, you can use the calibration data to remove effects of lens distortion.
% originalImage = imread(imageFileNames{1});
% undistortedImage = undistortImage(originalImage, cameraParams);

% See additional examples of how to use the calibration data.  At the prompt type:
% showdemo('MeasuringPlanarObjectsExample')
% showdemo('SparseReconstructionExample')

end