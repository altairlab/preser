classdef TrajPlanner < handle
	properties(Access = private)
		Ts_v
		prev_t
		i
		generated
		dt
	end
	
	properties(Access = public)
		out_twist_cmd		
	end
	
	methods
		function obj = TrajPlanner()
			obj.prev_t = 0;
			obj.i = 1;
			obj.generated = false;
			obj.dt = 0.1;
			obj.out_twist_cmd = [0 0 0];
		end
		
		function obj = configure(obj)
		end
		
		function obj = start(obj)
		end
		
		function obj = generateTraj(obj,path,dt,segment_time)
			obj.generated = false;
			obj.i = 1;
			obj.dt = dt;
			Ts_p = [];
			obj.Ts_v = [];
			Ts_a = [];
			tt = [];
			
			if length(path) < 2
				error('Path must be >2 ');
			end
			
			t_seg = 0:dt:segment_time';
			
			for j=2:size(path,1)
				%ROT_prev = angle2dcm(0,0,0,'XYZ');
				POS_prev = path(j-1,:,:,:);
				
				%ROT = angle2dcm(0,0,0,'XYZ');
				POS = path(j,:,:,:);
				
				[p,v,a] = jtraj(POS_prev,POS,t_seg);
				
				Ts_p = cat(1,p,Ts_p);
				obj.Ts_v = cat(1,v,obj.Ts_v);
				Ts_a = cat(1,a,Ts_a);
				tt = cat(2,tt,t_seg);
			end
			obj.generated = true;
		end
		
		function obj = update(obj)
			if obj.generated == false
				obj.out_twist_cmd = [0 0 0];
				return
			end
			
			if obj.i > size(obj.Ts_v,1)
				disp('Trajectory finished');
				obj.i = 1;
				obj.generated = false;
				obj.out_twist_cmd = [0 0 0];
				return
			end
			
			global t;
			if (t - obj.prev_t) >= obj.dt*1000
				obj.out_twist_cmd = [obj.Ts_v(obj.i,1) obj.Ts_v(obj.i,2) obj.Ts_v(obj.i,3)];
				obj.i = obj.i+1;
				obj.prev_t = t;
			end
		end
	end
end