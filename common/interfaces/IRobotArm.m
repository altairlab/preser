classdef IRobotArm < handle
    properties(Access = protected)
        vrep
        id
        
        NR_OF_SLAVES
        
        control_modes
        joint_limits
        joint_handle
        
        last_joint_position
    end
    
    properties(Access = public)
        out_joint_positions
        out_joint_velocities
        out_joint_efforts
        
        in_positions_cmd
        in_velocities_cmd
        in_efforts_cmd
        
        link;
    end
    
    methods
        function obj = IRobotArm(vrep, id, jointNum, jointNames, link)
            obj.NR_OF_SLAVES = jointNum;
            obj.vrep = vrep;
            obj.id = id;
            
            obj.control_modes(1:obj.NR_OF_SLAVES) = ctrl_modes.MOTOR_STOP;
            obj.joint_limits(1:obj.NR_OF_SLAVES) = -1;
            obj.joint_handle(1:obj.NR_OF_SLAVES) = -1;
            
            obj.out_joint_positions(1:obj.NR_OF_SLAVES) = 0.0;
            obj.out_joint_velocities(1:obj.NR_OF_SLAVES) = 0.0;
            obj.out_joint_efforts(1:obj.NR_OF_SLAVES) = 0.0;
            
            obj.in_positions_cmd(1:obj.NR_OF_SLAVES) = 0.0;
            obj.in_velocities_cmd(1:obj.NR_OF_SLAVES) = 0.0;
            obj.in_efforts_cmd(1:obj.NR_OF_SLAVES) = 0.0;
            
            for i = 0:obj.NR_OF_SLAVES-1
                [res, obj.joint_handle(i+1)] = vrep.simxGetObjectHandle(obj.id, num2str(cell2mat(jointNames(i+1))), vrep.simx_opmode_oneshot_wait);
                vrchk(obj.vrep, res);
            end
            
            % preparing for buffering data
            for i = 1:obj.NR_OF_SLAVES
                obj.vrep.simxGetJointPosition(obj.id, obj.joint_handle(i), obj.vrep.simx_opmode_streaming);
                obj.vrep.simxGetObjectFloatParameter(obj.id, obj.joint_handle(i), 2012, obj.vrep.simx_opmode_streaming);
                obj.vrep.simxGetJointForce(obj.id, obj.joint_handle(i), obj.vrep.simx_opmode_streaming);
            end
            
            % coordinate frame : arm link , reference frame World.
            if ~isempty(link)
                obj.link.name = link;
                [res, obj.link.frame_id] = vrep.simxGetObjectHandle(obj.id, link , vrep.simx_opmode_oneshot_wait);
                vrchk(obj.vrep, res);
                obj.link.position  = obj.vrep.simxGetObjectPosition(obj.id,obj.link.frame_id, -1 , obj.vrep.simx_opmode_streaming);
                obj.link.orientation  = obj.vrep.simxGetObjectOrientation(obj.id, obj.link.frame_id , -1 , obj.vrep.simx_opmode_streaming);
            end
        end
        
        function obj = setControlMode(obj, modes)
            obj.control_modes = modes;
        end
        
        function obj = setControlModesAll(obj,one_mode)
            obj.control_modes(1:obj.NR_OF_SLAVES) = one_mode;
            for i = 1:obj.NR_OF_SLAVES
                switch one_mode
                    case ctrl_modes.PLANE_ANGLE
                        res = obj.vrep.simxSetObjectIntParameter(obj.id, obj.joint_handle(i),2001,1,obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.ANGULAR_VELOCITY
                        res = obj.vrep.simxSetObjectIntParameter(obj.id, obj.joint_handle(i),2001,0,obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.TORQUE
                        res = obj.vrep.simxSetObjectIntParameter(obj.id, obj.joint_handle(i),2001,0,obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.MOTOR_STOP
                        res = obj.vrep.simxSetObjectIntParameter(obj.id, obj.joint_handle(i),2001,1,obj.vrep.simx_opmode_oneshot);
                        obj.last_joint_position = obj.out_joint_positions;
                        vrchk(obj.vrep, res, true);
                    otherwise
                        fprintf('CTRL_MODES not recognized for arm');
                end
            end
        end
        
        function obj = readCoordinateFrame(obj)
            if ~exist('obj.link.frame_id','var')
                [res,obj.link.position ] = obj.vrep.simxGetObjectPosition(obj.id,obj.link.frame_id, -1 , obj.vrep.simx_opmode_buffer);
                vrchk(obj.vrep, res);
                [res,obj.link.orientation ] = obj.vrep.simxGetObjectOrientation(obj.id, obj.link.frame_id , -1 , obj.vrep.simx_opmode_buffer);
                vrchk(obj.vrep, res);
            end
        end
        
        
        function obj = readJointState(obj)
            for i = 1:obj.NR_OF_SLAVES
                [res, obj.out_joint_positions(i)] = obj.vrep.simxGetJointPosition(obj.id, obj.joint_handle(i), obj.vrep.simx_opmode_buffer);
                vrchk(obj.vrep, res, true);
                [res, obj.out_joint_velocities(i)] = obj.vrep.simxGetObjectFloatParameter(obj.id, obj.joint_handle(i), 2012, obj.vrep.simx_opmode_buffer);
                vrchk(obj.vrep, res, true);
                [res, obj.out_joint_efforts(i)] = obj.vrep.simxGetJointForce(obj.id, obj.joint_handle(i), obj.vrep.simx_opmode_buffer);
                vrchk(obj.vrep, res, true);
            end
        end
        
        function obj = setJointSetpoints(obj)
            obj.vrep.simxPauseCommunication(obj.id,1);
            for i = 1:obj.NR_OF_SLAVES
                switch obj.control_modes(i)
                    case ctrl_modes.PLANE_ANGLE
                        res = obj.vrep.simxSetJointTargetPosition(obj.id, obj.joint_handle(i),obj.in_positions_cmd(i),obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.ANGULAR_VELOCITY
                        res = obj.vrep.simxSetJointTargetVelocity(obj.id, obj.joint_handle(i),obj.in_velocities_cmd(i),obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.TORQUE
                        res = obj.vrep.simxSetJointForce(obj.id, obj.joint_handle(i),obj.in_efforts_cmd(i),obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    case ctrl_modes.MOTOR_STOP
                        res = obj.vrep.simxSetJointTargetPosition(obj.id, obj.joint_handle(i),obj.last_joint_position(i),obj.vrep.simx_opmode_oneshot);
                        vrchk(obj.vrep, res, true);
                    otherwise
                        fprintf('CTRL_MODES not recognized for arm');
                end
            end
            obj.vrep.simxPauseCommunication(obj.id,0);
        end
        
        function obj = configure(obj)
            
        end
        
        function obj = start(obj)
            
        end
        
        function obj = update(obj)
            readJointState(obj);
            setJointSetpoints(obj);
        end
        
        function obj = stop(obj)
            
        end
        
    end
end