% Obstacle = -1,Target = 0, Robot = 1, Space = 2
classdef IPPlanner < handle
	properties(Access = protected)
		threshold
		scale
		MAP
		MAX_X
		MAX_Y		
	end
	
	properties(Access = public)
		in_odometry
		in_occupacy_grid
		out_optimalPath
	end
	
	methods(Abstract)
		path = getPath(obj,x,y)
	end
	
	methods
		function obj = IPPlanner()
			obj.in_odometry = data_odometry();
			obj.in_occupacy_grid = [];
			obj.threshold = 50;
			obj.scale = 0.05;
			obj.out_optimalPath = [];
		end
				
		function obj = configure(obj)
		end
		
		function obj = start(obj)
		end
		
		function obj = update(obj)
		end
	end
end