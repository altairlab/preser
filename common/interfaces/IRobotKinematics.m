classdef IRobotKinematics < handle
	properties(Access = protected)
		vrep
		id
		base_handle
		base_pose
		
		solver_type
		DH
		offset
		robot_chain
		
		joint_min_limits
		joint_max_limits
		
		chain_joint_count
		robot_joint_count
	end
	
	properties(Access = public)
		out_joint_positions
		out_joint_velocities		
		out_EE_pose
		out_EE_twist
		
		in_joint_positions
		in_joint_velocities
		in_odometry
		
		in_EE_pose_cmd
		in_EE_twist_cmd
	end
	
	methods(Access = private)
		function createKinematicChain(obj)
			ROT = quat2d(struct2array(obj.base_pose.orientation));  % quat2dcm -> Aerospace Toolbox		 	
			T = zeros(4);
			T(1:3,1:3) = ROT;
			POS = struct2array(obj.base_pose.position);
			T(4,:) = [0 0 0 1];
			T(:,4) = [POS 1];
			
			obj.robot_chain = SerialLink(obj.DH,'base',T);
			obj.robot_chain.offset = obj.offset;
			obj.robot_chain.qlim = [obj.joint_min_limits; obj.joint_max_limits]';
		end
				
		function differentialInverseKinematic(obj)
			J = obj.robot_chain.jacob0(obj.in_joint_positions);
			cart_vector = [struct2array(obj.in_EE_twist_cmd.linear) q2eul(struct2array(obj.in_EE_twist_cmd.angular))]; % quat2euler -> Aerospace Toolbox
			obj.out_joint_velocities = pinv(J)*cart_vector';
		end
		
		function differentialforwardKinematic(obj)
			J = obj.robot_chain.jacob0(obj.in_joint_positions);
			cart_velocites = J*obj.in_joint_velocities';
			
			obj.out_EE_twist.linear.x = cart_velocites(1);
			obj.out_EE_twist.linear.y = cart_velocites(2);
			obj.out_EE_twist.linear.z = cart_velocites(3);
			
			orientation = a2quat(cart_velocites(4),cart_velocites(5),cart_velocites(6),'XYZ');	% angle2quat -> Aerospace Toolbox
			
			obj.out_EE_twist.angular.x = orientation(1);
			obj.out_EE_twist.angular.y = orientation(2);
			obj.out_EE_twist.angular.z = orientation(3);
			obj.out_EE_twist.angular.w = orientation(4);
		end
		
		function InverseKinematic(obj)
			T = quat2d(struct2array(obj.in_EE_pose_cmd.orientation));  % quat2dcm -> Aerospace Toolbox
			T(4,1:3) = 0;
			T(1:4,4) = [obj.in_EE_pose_cmd.position.x,obj.in_EE_pose_cmd.position.y,obj.in_EE_pose_cmd.position.z,1];
			
			obj.out_joint_positions = obj.robot_chain.ikcon(T);
		end
		
		function forwardKinematic(obj)
			T = obj.robot_chain.fkine(obj.in_joint_positions);
			obj.out_EE_pose.position.x = T(1,4);
			obj.out_EE_pose.position.y = T(2,4);
			obj.out_EE_pose.position.z = T(3,4);
			
			orientation = d2quat(T(1:3,1:3)); % dcm2quat -> Aerospace Toolbox
			
			obj.out_EE_pose.orientation.x = orientation(1);
			obj.out_EE_pose.orientation.y = orientation(2);
			obj.out_EE_pose.orientation.z = orientation(3);
			obj.out_EE_pose.orientation.w = orientation(4);
		end		
	end
		
	methods
		function obj = IRobotKinematics(vrep, id, base_link_name, DH, chain_joint_count, robot_joint_count, offset, min_limits, max_limits)
			obj.vrep = vrep;
			obj.id = id;
			obj.solver_type = 'POS';
			obj.DH = DH;
			obj.offset = offset;
			obj.base_pose = data_pose();
			
			obj.joint_min_limits = min_limits;
			obj.joint_max_limits = max_limits;
			
			obj.chain_joint_count = chain_joint_count;
			obj.robot_joint_count = robot_joint_count;
			
			obj.in_joint_positions = zeros(robot_joint_count, 1);
			obj.in_joint_velocities = zeros(robot_joint_count, 1);

			obj.out_joint_positions = zeros(robot_joint_count, 1);
			obj.out_joint_velocities = zeros(robot_joint_count, 1);

			obj.in_odometry = data_odometry;
			obj.out_EE_pose = data_pose;		
			obj.in_EE_pose_cmd = data_pose;
			obj.in_EE_twist_cmd = data_twist;
			
			% Get cartesian init position
			[~, obj.base_handle] = obj.vrep.simxGetObjectHandle(obj.id,base_link_name,obj.vrep.simx_opmode_oneshot_wait);

			[res, position] = obj.vrep.simxGetObjectPosition(obj.id, obj.base_handle,-1, obj.vrep.simx_opmode_oneshot_wait);
			vrchk(obj.vrep, res);
			
			[res, orientation] = obj.vrep.simxGetObjectOrientation(obj.id, obj.base_handle,-1, obj.vrep.simx_opmode_oneshot_wait);
			vrchk(obj.vrep, res);
			
			obj.base_pose = obj.base_pose.setPose(position(1),position(2),position(3));			
			obj.base_pose = obj.base_pose.setOrientation(a2quat(orientation(1),orientation(2),orientation(3),'XYZ'));	% angle2quat -> Aerospace Toolbox		
		end
		
		function obj = setSolverType(obj,type)
			obj.solver_type = type;
		end
		
		function joints = robotChain2kinematicChain(obj)
			joints = obj.in_joint_positions;
		end
		
		function obj = configure(obj)
			obj.createKinematicChain()
		end
		
		function obj = start(obj)
		end
		
		function obj = update(obj)
			obj.differentialforwardKinematic();
			obj.forwardKinematic();
				
			if strcmp(obj.solver_type,'POS')
				obj.InverseKinematic();
			elseif strcmp(obj.solver_type,'VEL')
				obj.differentialInverseKinematic();
			end
		end
		
		function obj = stop(obj)
		end
	end
end