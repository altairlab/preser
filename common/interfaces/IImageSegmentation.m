
classdef IImageSegmentation < handle
% 	properties(Access = protected)
%         
% 	end
	
	properties(Access = public)
        original_image
        mod_image
        centroids
        regionSeg
	end
	
	methods(Abstract)
		seg = getSegmentedRegion(obj);
        colorInfo = getColorInfo(obj,colorSpace);
        plotInfo = getPlot(obj,axes_handle);
	end
% 	
	methods
		function obj = IImageSegmentation(image)
            obj.original_image = image;
            obj.centroids = [];
            obj.regionSeg = [];
		end
				
		function obj = configure(obj)
		end
		
		function obj = start(obj)
		end
		
		function obj = update(obj)
		end
	end
end