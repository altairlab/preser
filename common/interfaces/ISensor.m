classdef ISensor < handle
	properties(Access = protected)
		vrep
		id
		sensor_name
		sensor_handle
	end
	
	properties(Access = public)
		out_sensor_pose
        link
	end
	
	methods(Abstract, Access = protected)
		getData(obj)
	end
	
	methods(Access = private)
		function updatePose(obj)
			[res, position] = obj.vrep.simxGetObjectPosition(obj.id, obj.sensor_handle,-1, obj.vrep.simx_opmode_oneshot_wait);
			vrchk(obj.vrep, res);
			
			[res, orientation] = obj.vrep.simxGetObjectOrientation(obj.id, obj.sensor_handle,-1, obj.vrep.simx_opmode_oneshot_wait);
			vrchk(obj.vrep, res);
			
			obj.out_sensor_pose = obj.out_sensor_pose.setPose(position(1),position(2),position(3));			
			obj.out_sensor_pose = obj.out_sensor_pose.setOrientation(a2quat(orientation(1),orientation(2),orientation(3),'XYZ')); % angle2quat -> Aerospace Toolbox
		
        
            obj.link.position = position;
            obj.link.orientation = orientation;
        end		
	end
	
	methods
		function obj = ISensor(vrep,id,sensor_name)
			obj.vrep = vrep;
			obj.id = id;
			obj.sensor_name = sensor_name;
			obj.out_sensor_pose = data_pose();
			
			[~, obj.sensor_handle] = obj.vrep.simxGetObjectHandle(obj.id,num2str(cell2mat(obj.sensor_name)),obj.vrep.simx_opmode_oneshot_wait);
            
            obj.link.frame_id = obj.sensor_handle;
            obj.link.name = sensor_name;
		end
				
		function configure(obj)
		end
		
		function start(obj)
		end
		
		function update(obj)
			obj.updatePose();
			obj.getData();
		end
				
	end
end