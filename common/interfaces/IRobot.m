classdef IRobot < handle
	properties(Access = protected)
		vrep
		id
		robot_name
		
		arm_count
		base_count
		gripper_count
	end
	
	properties(Access = public)
		Arm
		Base
		Gripper
	end
		
	methods
		function obj = IRobot(vrep,id,robot_name)
			obj.vrep = vrep;
			obj.id = id;
			obj.robot_name = robot_name;
			
			ini = IniConfig();
			ini.ReadFile(strcat(obj.robot_name,'.ini'));

			obj.arm_count = ini.GetValues('robot', 'armCount');
			obj.base_count = ini.GetValues('robot', 'baseCount');
			obj.gripper_count = ini.GetValues('robot', 'gripperCount');
			
			% Creating empty classes instances
			if obj.arm_count > 0
				eval(strcat(strcat('obj.Arm = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Arm.empty(obj.arm_count,0);'));
			end

			if obj.base_count > 0			
				eval(strcat(strcat('obj.Base = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Base.empty(obj.base_count,0);'));
			end
			
			if obj.gripper_count > 0
				eval(strcat(strcat('obj.Gripper = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Gripper.empty(obj.gripper_count,0);'));
			end
			
			% Calling the appropriate constructor
			for i = 1:obj.arm_count
				eval(strcat(strcat('obj.Arm(i) = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Arm(vrep,id);'));
			end

			for i = 1:obj.base_count
				eval(strcat(strcat('obj.Base(i) = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Base(vrep,id);'));
			end

			for i = 1:obj.gripper_count
				eval(strcat(strcat('obj.Gripper(i) = ',strcat(upper(robot_name(1)),(robot_name(2:length(robot_name))))),'Gripper(vrep,id);'));
			end
		end
		
		function obj = configure(obj)
			for i = 1:obj.arm_count
				obj.Arm(i).configure();
			end

			for i = 1:obj.base_count
				obj.Base(i).configure();
			end

			for i = 1:obj.gripper_count
				obj.Gripper(i).configure();
			end		 
		end

		function obj = start(obj)
			for i = 1:obj.arm_count
				obj.Arm(i).start();
			end

			for i = 1:obj.base_count
				obj.Base(i).start();
			end

			for i = 1:obj.gripper_count
				obj.Gripper(i).start();
			end
		end

		function obj = update(obj)
			for i = 1:obj.arm_count
				obj.Arm(i).update();
			end

			for i = 1:obj.base_count
				obj.Base(i).update();
			end

			for i = 1:obj.gripper_count
				obj.Gripper(i).update();
			end
		end
				
		function obj = stop(obj)
			for i = 1:obj.arm_count
				obj.Arm(i).stop();
			end

			for i = 1:obj.base_count
				obj.Base(i).stop();
			end

			for i = 1:obj.gripper_count
				obj.Gripper(i).stop();
			end
		end

	end
end