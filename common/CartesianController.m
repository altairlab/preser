classdef CartesianController < handle
	properties(Access = protected)
		K;
		dt;
	end
	
	properties(Access = public)
		in_EE_pose;
		in_desired_EE_pose;
		out_EE_twist;
	end
	
	methods
		function obj = CartesianController()
			obj.K = 0.02;
			obj.dt = 0.001;
			
			obj.in_EE_pose = data_pose;
			obj.in_desired_EE_pose = data_pose;
			obj.out_EE_twist = data_twist;
		end
		
		function setK(obj,K)
			obj.K = K;
		end
		
		function setDt(obj,dt)
			obj.dt = dt;
		end
		
		function configure(obj)
			
		end
		
		function start(obj)
			
		end
		
		function update(obj)			
			pos_diff = (struct2array(obj.in_desired_EE_pose.position) - struct2array(obj.in_EE_pose.position))./obj.dt;
			pos_diff = pos_diff * obj.K;
			
			matrix_ee_pose = quat2d(struct2array(obj.in_EE_pose.orientation)); % quat2dcm -> Aerospace Toolbox
			matrix_des_ee_pose = quat2d(struct2array(obj.in_desired_EE_pose.orientation));  % quat2dcm -> Aerospace Toolbox
			axis_angle = vrrotmat2vec((inv(matrix_ee_pose)*matrix_des_ee_pose));
			rot = axis_angle(1,1:3) * axis_angle(1,4);
			
			orie_diff = (matrix_ee_pose * rot')./obj.dt;
			orie_diff = orie_diff * obj.K;
			
			obj.out_EE_twist.linear.x = pos_diff(1);
			obj.out_EE_twist.linear.y = pos_diff(2);
			obj.out_EE_twist.linear.z = pos_diff(3);
			
			orientation = a2quat(orie_diff(1),orie_diff(2),orie_diff(3),'XYZ'); % angle2quat -> Aerospace Toolbox
			
			obj.out_EE_twist.angular.x = orientation(1);
			obj.out_EE_twist.angular.y = orientation(2);
			obj.out_EE_twist.angular.z = orientation(3);
			obj.out_EE_twist.angular.w = orientation(4);
		end
		
	end
end
