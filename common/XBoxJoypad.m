classdef XBoxJoypad < handle
	properties(Access = protected)
		joy
		th
		max_vel
		max_ang_vel
	end
	
	properties(Access = public)
		out_twist_cmd;
	end
	
	methods
		function obj = XBoxJoypad()
			obj.joy = vrjoystick(1);			
			obj.out_twist_cmd = [0 0 0];
			obj.th = 0.15;
			obj.max_vel = 1.0;
			obj.max_ang_vel = 1.0;
		end
				
		function obj = configure(obj)
			
		end
		
		function obj = start(obj)
			
		end
		
		function obj = update(obj)			
			[axis,~,~] = read(obj.joy);
			axis(abs(axis) <= obj.th) = 0;
			obj.out_twist_cmd = [-axis(2) axis(1) -axis(4)];%*[obj.max_vel obj.max_vel obj.max_ang_vel]';
		end
	end
end