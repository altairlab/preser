import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.io.PrintWriter;

public class HilasRunner extends Thread
{
	PrintWriter log;
	MatlabControl matlab;
	boolean run_hilas;
	int sleep_ms;
  	Object[] args;
  	long counter;

  	String procedure_name;
  	Semaphore semaphore;
	boolean java_log;
	boolean matlab_log;
	int startup_sleep;

  private void log2file(String content)
  {
  	if(java_log)
  	{
	 		this.log.println(content);
	 		this.log.flush();
  	}
  }

  public void evalCmdLock(String cmd) throws InterruptedException
  {  		
		this.args[0] = cmd;
		this.matlab.blockingFeval("eval",this.args);
  }

	public void task() throws InterruptedException
	{
		// one step simulation
		// evalCmdLock("disp('sim step')");
		
		evalCmdLock("try\nvrep.simxSynchronousTrigger(id);\ncatch\nend");
		evalCmdLock("try\nt = vrep.simxGetLastCmdTime(id);\ncatch\nend");

		// evaluating procedure
		evalCmdLock("try\n"+this.procedure_name+";\ncatch\nend");
        
        if((this.counter % 2) != 0)
            evalCmdLock("if exist('laser') \n laser.update();\nend");

        
        this.counter++;
		log2file("task done");
	}

	public void stopRun() throws InterruptedException
	{
		// stop the thread
		this.semaphore.acquire();
		this.run_hilas = false;
		this.semaphore.release();
	}

	public HilasRunner(int t_to_sleep, String procedure_name, boolean enable_java_log, boolean enable_matlab_log)
	{
		try
		{
			this.run_hilas = true;
			this.sleep_ms = t_to_sleep;
			this.procedure_name = procedure_name;
			this.java_log = enable_java_log;
			this.matlab_log = enable_matlab_log;
			this.startup_sleep = 0;
		}
		catch(Exception e){log2file(e.toString());};
	}

	@Override
	public void run()
	{
		try
		{
			this.matlab = new MatlabControl();
			this.matlab.setEchoEval(this.matlab_log);

			this.semaphore = new Semaphore(1);
			this.args = new Object[1];

	  		if(java_log)
	  		{
				this.log = new PrintWriter("hilas-java.log", "UTF-8");
			}

			this.matlab.eval("disp('Runner thread built correctly...')");
			log2file("main-run");
			this.counter = 0;
			Thread.sleep(this.startup_sleep);
			this.matlab.eval("disp('Starting up...')");

			while(this.run_hilas)
			{
				//this.counter++;
				//log2file("while-run "+Long.toString(this.counter));
				if(this.semaphore.availablePermits() > 0)
				{
					task();
				}				
				Thread.sleep(this.sleep_ms);
			}

			// stop simulation
			this.matlab.eval("vrep.simxStopSimulation(id,vrep.simx_opmode_oneshot_wait);");
			this.matlab.eval("vrep.simxFinish(id);");

			log2file("close-run");
	  		if(java_log)
	  		{
				this.log.close();
			}
		}
		catch(Exception e){log2file(e.toString());}
	}
}