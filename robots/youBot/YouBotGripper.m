classdef YouBotGripper < IRobotGripper & handle
	
	methods
		function obj = YouBotGripper(vrep, id)
			obj = obj@IRobotGripper(vrep, id, 2,{'gripper_finger_joint_l','gripper_finger_joint_r'}, 'youBot_positionTip');
			obj.in_positions_cmd = [-0.0100 -0.0100];
		end
	end
end