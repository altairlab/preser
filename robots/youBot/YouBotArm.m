classdef YouBotArm < IRobotArm
	methods
		function obj = YouBotArm(vrep, id)
			obj = obj@IRobotArm(vrep, id, 5,{'arm_joint_1','arm_joint_2','arm_joint_3','arm_joint_4','arm_joint_5'},'');
		end
	end
end