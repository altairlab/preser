classdef YouBot < IRobot
	methods
		function obj = YouBot(vrep, id)
			obj = obj@IRobot(vrep, id, 'youBot');
		end
	end
end