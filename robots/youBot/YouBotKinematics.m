classdef YouBotKinematics < IRobotKinematics
	properties(Access = private)
		rotationRatio = 1.0;
		slideRatio = 1.0;
		
		lengthBetweenFrontAndRearWheels = 0.47;
		lengthBetweenFrontWheels = 0.30;
		wheelRadius = 0.0475;		
	end
	
	methods(Access = private)
		function createKinematicChain(obj)
			r2 = SerialLink(obj.DH);
			r2.offset = obj.offset;
			r2.qlim = [obj.joint_min_limits; obj.joint_max_limits]';
			
			%					theta			d							a							alpha
			DH_BASE = [0				0							0							-pi/2				1;
								 -pi/2		0							0							pi/2				1;
								 0				0.056+0.115		0.143+0.024		0						0];

			r = 0;
			p = -pi/2;
			y = 0;
			ROT = angle2dcm(r,p,y,'XYZ');
			
			T = zeros(4);
			T(1:3,1:3) = ROT;
			T(4,:) = [0 0 0 1];
			T(:,4) = [0 0 0 1];
			
			r1 = SerialLink(DH_BASE,'base',T);
			r1.offset = [0 0 0];
			
			obj.robot_chain = SerialLink([r1 r2]);
			%obj.robot_chain.plot([0 0 0 0 0 0 0 0],'workspace',[-1 1 -1 1 -1 1]);
		end
				
		function joints = velRobotChain2kinematicChain(obj)
			angular = cell2mat(struct2cell(obj.in_odometry.twist.twist.angular));
			joints = [angular(1) angular(2) angular(3) obj.in_joint_velocities(5:9)]';			
		end
		
		function [w1,w2,w3,w4] = twist2JointVelocities(obj,longitudinalVelocity,transversalVelocity,angularVelocity)
			% ----------------------------------
			% Twist to wheel: from youBot driver
			% ----------------------------------
			RadPerSec_FromX = longitudinalVelocity/obj.wheelRadius;
			RadPerSec_FromY = transversalVelocity/(obj.wheelRadius*obj.slideRatio);
			
			% Calculate Rotation Component
			RadPerSec_FromTheta = ((obj.lengthBetweenFrontAndRearWheels + obj.lengthBetweenFrontWheels) / (2.0 * obj.wheelRadius)) * angularVelocity;
			
			w1 = -RadPerSec_FromX + RadPerSec_FromY + RadPerSec_FromTheta;
			w2 = RadPerSec_FromX + RadPerSec_FromY + RadPerSec_FromTheta;
			w3 = -RadPerSec_FromX - RadPerSec_FromY + RadPerSec_FromTheta;
			w4 = RadPerSec_FromX - RadPerSec_FromY + RadPerSec_FromTheta;
			% ----------------------------------
			% End driver
			% ----------------------------------
		end
		
		function [w1, w2, w3, w4] = position2JointPositions(obj,longitudinalPosition,transversalPosition,angularPosition)
			% ----------------------------------
			% Position to wheel: from youBot driver
			% ----------------------------------
			Rad_FromX = longitudinalPosition/obj.wheelRadius;
			Rad_FromY = transversalPosition/(obj.wheelRadius*obj.slideRatio);
			
			Rad_FromTheta = ((obj.lengthBetweenFrontAndRearWheels + obj.lengthBetweenFrontWheels) / (2.0 * obj.wheelRadius)) * angularPosition;
			
			w1 = -Rad_FromX + Rad_FromY + Rad_FromTheta;
			w2 = Rad_FromX + Rad_FromY + Rad_FromTheta;
			w3 = -Rad_FromX - Rad_FromY + Rad_FromTheta;
			w4 = Rad_FromX - Rad_FromY + Rad_FromTheta;
			% ----------------------------------
			% End driver
			% ----------------------------------
		end
		
		function differentialInverseKinematic(obj)
			J = obj.robot_chain.jacob0(obj.robotChain2kinematicChain(),'rpy');
			cart_vector = [ struct2array(obj.in_EE_twist_cmd.linear) q2eul(struct2array(obj.in_EE_twist_cmd.angular))]; % quat2euler -> Aerospace Toolbox			
			joints = pinv(J)*cart_vector';
			[w1, w2, w3, w4] = obj.twist2JointVelocities(joints(1),joints(2),joints(3));
			obj.out_joint_velocities = [w1 w2 w3 w4 joints(4:8)'];
		end
		
		function differentialforwardKinematic(obj)
			J = obj.robot_chain.jacob0(obj.robotChain2kinematicChain());
			cart_velocites = J*obj.velRobotChain2kinematicChain();
			
			obj.out_EE_twist.linear.x = cart_velocites(1);
			obj.out_EE_twist.linear.y = cart_velocites(2);
			obj.out_EE_twist.linear.z = cart_velocites(3);
			
			orientation = a2quat(cart_velocites(4),cart_velocites(5),cart_velocites(6),'XYZ');	 % angle2quat -> Aerospace Toolbox		
			
			obj.out_EE_twist.angular.x = orientation(1);
			obj.out_EE_twist.angular.y = orientation(2);
			obj.out_EE_twist.angular.z = orientation(3);
			obj.out_EE_twist.angular.w = orientation(4);
		end
				
		function InverseKinematic(obj)
			T = quat2d(obj.in_EE_pose_cmd.orientation.x,obj.in_EE_pose_cmd.orientation.y,obj.in_EE_pose_cmd.orientation.z,obj.in_EE_pose_cmd.orientation.w);  % quat2dcm -> Aerospace Toolbox
			T(4,1:3) = 0;
			T(1:4,4) = [obj.in_EE_pose_cmd.position.x,obj.in_EE_pose_cmd.position.y,obj.in_EE_pose_cmd.position.z,1];
			
			%trplot(T);
			
			[joints, ~] = obj.robot_chain.ikcon(T);
			[w1, w2, w3, w4] = obj.position2JointPositions(joints(1),joints(2),joints(3));
			obj.out_joint_positions = [w1 w2 w3 w4 joints(4:8)];
		end
		
		function forwardKinematic(obj)
			T = obj.robot_chain.fkine(obj.robotChain2kinematicChain());
			obj.out_EE_pose.position.x = T(1,4);
			obj.out_EE_pose.position.y = T(2,4);
			obj.out_EE_pose.position.z = T(3,4);
			
			orientation = d2quat(T(1:3,1:3));  % dcm2quat -> Aerospace Toolbox
			
			obj.out_EE_pose.orientation.x = orientation(1);
			obj.out_EE_pose.orientation.y = orientation(2);
			obj.out_EE_pose.orientation.z = orientation(3);
			obj.out_EE_pose.orientation.w = orientation(4);
		end		
	end
	
	methods
		function obj = YouBotKinematics(vrep,id)
			%     theta       d               a           alpha
			DH = [0						0						    0.0330      pi/2;
						0			      0               0.1550      0;
						0           0               0.1350      0;
						0						0               0		       -pi/2;
						0		        -0.2175					0           0];
			
			offset = [deg2rad(169)-pi pi/2+deg2rad(-65) deg2rad(146) pi/2+deg2rad(-102.5) pi/2];
			
			min_limits = [0.01 0.01 -5.0215 0.022 0.11073];
			max_limits = [5.8343 2.61538 -0.0157 3.42577 5.63595];
			obj = obj@IRobotKinematics(vrep, id, 'youBot', DH, 8, 9, offset, min_limits, max_limits);
		end
		
		function joints = robotChain2kinematicChain(obj)
			[~, ~, yaw] = q2angle(struct2array(obj.in_odometry.pose.pose.orientation),'XYZ'); % quat2angle -> Aerospace toolbox
            joints = [obj.in_odometry.pose.pose.position.x obj.in_odometry.pose.pose.position.y yaw obj.in_joint_positions(5:9)'];
		end
	end
end
