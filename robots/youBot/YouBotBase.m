classdef YouBotBase < IRobotBase
	properties(Access = private)
		rotationRatio = 1.0;
		slideRatio = 1.0;
		
		lengthBetweenFrontAndRearWheels = 0.47;
		lengthBetweenFrontWheels = 0.30;
		wheelRadius = 0.0475;
		
		lastWheelPositionInitialized;
		lastWheelPositions;
		longitudinalPos;
		transversalPos;
		angle;
		
		joint_base_position_prev;
		odom_wheelPositions;
	end
		
	methods
		function obj = YouBotBase(vrep, id)
			obj = obj@IRobotBase(vrep, id, 4,{'wheel_joint_fl','wheel_joint_fr','wheel_joint_bl','wheel_joint_br'},'base_link');
			
			obj.lastWheelPositionInitialized = false;
			obj.lastWheelPositions = 0;
			obj.longitudinalPos = 0;
			obj.transversalPos = 0;
			obj.angle = 0;
			
			obj.joint_base_position_prev = zeros(obj.NR_OF_SLAVES,1);
			obj.odom_wheelPositions = zeros(obj.NR_OF_SLAVES,1);
		end
		
		function obj = readOdometry(obj)
			% Weird code! Don't touch it, here be dragons.
			deltapos = zeros(obj.NR_OF_SLAVES,1);
			joints = zeros(obj.NR_OF_SLAVES,1);
			vels = zeros(obj.NR_OF_SLAVES,1);

			sign = [1 -1 -1 1];

			% ----------------------MAGIC---------------------------- %

			joints(1) = obj.out_joint_positions(4) * sign(1);
			joints(2) = obj.out_joint_positions(3) * sign(2);
			joints(3) = obj.out_joint_positions(1) * sign(3);
			joints(4) = obj.out_joint_positions(2) * sign(4);        

			vels(1) = -obj.out_joint_velocities(4);
			vels(2) = obj.out_joint_velocities(3);
			vels(3) = -obj.out_joint_velocities(1);
			vels(4) = obj.out_joint_velocities(2);

			% -------------------------------------------------------- %

			geom_factor = (obj.lengthBetweenFrontAndRearWheels/2.0) + (obj.lengthBetweenFrontWheels/2.0);

			long_vel = (-vels(1) + vels(2) - vels(3) + vels(4)) * (obj.wheelRadius/4);
			trans_vel = (vels(1) + vels(2) - vels(3) - vels(4)) * (obj.wheelRadius/4);
			ang_vel = (vels(1) + vels(2) + vels(3) + vels(4)) * ((obj.wheelRadius/4)/geom_factor);

			deltapos(1) =  joints(2) - obj.joint_base_position_prev(1);
			deltapos(2) =  joints(1) - obj.joint_base_position_prev(2);
			deltapos(3) =  joints(3) - obj.joint_base_position_prev(3);
			deltapos(4) =  joints(4) - obj.joint_base_position_prev(4);

			for i=1:4
 				if deltapos(i) > pi
					deltapos(i) = deltapos(i) - (pi * 2);
				elseif deltapos(i) < -pi
					deltapos(i) = deltapos(i) + (2 * pi);        
				end
			end

			for i=1:4
				obj.odom_wheelPositions(i) = obj.odom_wheelPositions(i) + deltapos(i);
			end

			obj.joint_base_position_prev(1) = joints(2);
			obj.joint_base_position_prev(2) = joints(1);
			obj.joint_base_position_prev(3) = joints(3);
			obj.joint_base_position_prev(4) = joints(4);
			
			% ----------------------------------
			% Twist to wheel: from youBot driver
			% ----------------------------------			
			if obj.lastWheelPositionInitialized == false
				obj.lastWheelPositions = obj.odom_wheelPositions;
				obj.longitudinalPos = 0;
				obj.transversalPos = 0;
				obj.angle = 0;
				obj.lastWheelPositionInitialized = true;
			end
			
			wheel_radius_per4 = obj.wheelRadius/4.0;
			geom_factor = (obj.lengthBetweenFrontAndRearWheels/2.0) + (obj.lengthBetweenFrontWheels/2.0);
			
			deltaPositionW1 = obj.odom_wheelPositions(1) - obj.lastWheelPositions(1);
			deltaPositionW2 = obj.odom_wheelPositions(2) - obj.lastWheelPositions(2);
			deltaPositionW3 = obj.odom_wheelPositions(3) - obj.lastWheelPositions(3);
			deltaPositionW4 = obj.odom_wheelPositions(4) - obj.lastWheelPositions(4);			
			
			obj.lastWheelPositions = obj.odom_wheelPositions;
			
			deltaLongitudinalPos = (-deltaPositionW1 + deltaPositionW2 - deltaPositionW3 + deltaPositionW4) * wheel_radius_per4;
			deltaTransversalPos = (deltaPositionW1 + deltaPositionW2 - deltaPositionW3 - deltaPositionW4) * wheel_radius_per4;
			obj.angle = obj.angle + (deltaPositionW1 + deltaPositionW2 + deltaPositionW3 + deltaPositionW4) * (wheel_radius_per4/geom_factor);
			
			obj.longitudinalPos = obj.longitudinalPos + (deltaLongitudinalPos * cos(obj.angle) - deltaTransversalPos * sin(obj.angle));
			obj.transversalPos = obj.transversalPos + (deltaLongitudinalPos * sin(obj.angle) + deltaTransversalPos * cos(obj.angle));
						
			% ----------------------------------
			% End driver
			% ----------------------------------
			
			obj.out_odometry.pose.pose = obj.out_odometry.pose.pose.setPose(-obj.longitudinalPos,obj.transversalPos, 0.095);
			obj.out_odometry.pose.pose = obj.out_odometry.pose.pose.setOrientation(a2quat(0.0,0.0,-obj.angle,'XYZ')); % angle2quat -> Aerospace Toolbox
			obj.out_odometry.twist.twist = obj.out_odometry.twist.twist.setLinear(long_vel,trans_vel, 0.0);
			obj.out_odometry.twist.twist = obj.out_odometry.twist.twist.setAngular(a2quat(0.0,0.0,-ang_vel,'XYZ')); % angle2quat -> Aerospace Toolbox
		end
		
		function obj = setJointSetpoints(obj)
			obj.vrep.simxPauseCommunication(obj.id,1);
			for i = 1:obj.NR_OF_SLAVES
				switch obj.control_modes(i)
					case ctrl_modes.PLANE_ANGLE
						res = obj.vrep.simxSetJointTargetPosition(obj.id, obj.joint_handle(i),obj.in_positions_cmd(i),obj.vrep.simx_opmode_oneshot);
						vrchk(obj.vrep, res, true);
					case ctrl_modes.ANGULAR_VELOCITY
						% Little hack
						sign = [1 -1 1 -1];						
						res = obj.vrep.simxSetJointTargetVelocity(obj.id, obj.joint_handle(i),obj.in_velocities_cmd(i) * sign(i),obj.vrep.simx_opmode_oneshot);
						vrchk(obj.vrep, res, true);
					case ctrl_modes.TORQUE
						res = obj.vrep.simxSetJointForce(obj.id, obj.joint_handle(i),obj.in_efforts_cmd(i),obj.vrep.simx_opmode_oneshot);
						vrchk(obj.vrep, res, true);
					case ctrl_modes.MOTOR_STOP
						res = obj.vrep.simxSetJointTargetPosition(obj.id, obj.joint_handle(i),obj.last_joint_position(i),obj.vrep.simx_opmode_oneshot);
						vrchk(obj.vrep, res, true);
					case ctrl_modes.TWIST
						fprintf('Cannot be in TWIST mode');
					otherwise
						fprintf('CTRL_MODES not recognized for base');
				end
			end
			obj.vrep.simxPauseCommunication(obj.id,0);	
		end
		
		function obj = setTwistSetpoints(obj)			
			wheelVelocities = zeros(4,1);
			
			longitudinalVelocity = obj.in_twist_cmd(1); % linear_x
			transversalVelocity = obj.in_twist_cmd(2); % linear_y
			angularVelocity = obj.in_twist_cmd(3); % angular_z
			
			% ----------------------------------
			% Twist to wheel: from youBot driver
			% ----------------------------------
			RadPerSec_FromX = longitudinalVelocity/obj.wheelRadius;
			RadPerSec_FromY = transversalVelocity/(obj.wheelRadius*obj.slideRatio);

			% Calculate Rotation Component
			RadPerSec_FromTheta = ((obj.lengthBetweenFrontAndRearWheels + obj.lengthBetweenFrontWheels) / (2.0 * obj.wheelRadius)) * angularVelocity;

			wheelVelocities(1) = -RadPerSec_FromX + RadPerSec_FromY + RadPerSec_FromTheta;
			wheelVelocities(2) = RadPerSec_FromX + RadPerSec_FromY + RadPerSec_FromTheta;
			wheelVelocities(3) = -RadPerSec_FromX - RadPerSec_FromY + RadPerSec_FromTheta;
			wheelVelocities(4) = RadPerSec_FromX - RadPerSec_FromY + RadPerSec_FromTheta;			
			% ----------------------------------
			% End driver
			% ----------------------------------
			
			% Little hack
			sign = [1 -1 1 -1];
			
			obj.vrep.simxPauseCommunication(obj.id,1);
			for i = 1:obj.NR_OF_SLAVES
				res = obj.vrep.simxSetJointTargetVelocity(obj.id, obj.joint_handle(i),wheelVelocities(i)*sign(i),obj.vrep.simx_opmode_oneshot);
				vrchk(obj.vrep, res, true);
			end
			obj.vrep.simxPauseCommunication(obj.id,0);
		end
		
	end
end