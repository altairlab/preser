classdef WAM < IRobot & handle
	methods
		function obj = WAM(vrep, id)
			obj = obj@IRobot(vrep, id, 'WAM');
		end
	end
end