classdef WAMKinematics < IRobotKinematics
	properties(Access = protected)
	end
	
	methods
		function obj = WAMKinematics(vrep, id)
			%     theta   d					a        alpha
			DH = [0       0         0        -pi/2;
						0       0         0         pi/2;
						0       0.55      0.045    -pi/2;
						0       0        -0.045     pi/2;
						0       0.3       0        -pi/2;
						0       0         0         pi/2;
						0       0.06      0         0];

			offset = [0 -2 0 pi 0 0 0];
			% https://www.cs.rpi.edu/twiki/pub/RoboticsWeb/WamTrackingSystem/Arm_UsersManual.pdf
			min_limits = [-2.6	-2.0	-2.8	-0.9	-4.76	-1.6	-3.0];
			max_limits = [2.6	2.0	2.8	3.1	1.24	1.6	3.0];
            
			obj = obj@IRobotKinematics(vrep, id, 'base_yaw_joint', DH, 7, 7, offset, min_limits, max_limits);
		end
	end
end