classdef WAMArm < IRobotArm
	methods
		function obj = WAMArm(vrep, id)
			obj = obj@IRobotArm(vrep, id, 7,{'base_yaw_joint','shoulder_pitch_joint','shoulder_yaw_joint','elbow_pitch_joint','wrist_yaw_joint','wrist_pitch_joint','palm_yaw_joint'},'');
		end
	end
end