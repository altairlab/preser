classdef UR10Arm < IRobotArm
	methods
		function obj = UR10Arm(vrep, id)
			obj = obj@IRobotArm(vrep, id, 6,{'UR10_joint1','UR10_joint2','UR10_joint3','UR10_joint4','UR10_joint5','UR10_joint6'});
		end
	end
end