classdef UR10 < IRobot
	methods
		function obj = UR10(vrep, id)
			obj = obj@IRobot(vrep, id, 'UR10');
		end
	end
end