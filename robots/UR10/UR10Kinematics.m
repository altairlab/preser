classdef UR10Kinematics < IRobotKinematics
	properties(Access = protected)
	end
	
	methods
		function obj = UR10Kinematics(vrep,id)
			%     theta       d               a           alpha
			DH = [0						0.1273			    0			      pi/2;
						0			      0               -0.612			0;
						0           0               -0.5723			0;
						0						0.163941	      0						pi/2;
						0		        0.1157					0           -pi/2;
						0		        0.0922					0           0];
			
			offset = [0 -pi/2 0 -pi/2 0 0];
			
			min_limits = -1 * [Inf Inf Inf Inf Inf Inf];
			max_limits = [Inf Inf Inf Inf Inf Inf];
			obj = obj@IRobotKinematics(vrep, id, 'UR10_joint1', DH, 6, 6, offset, min_limits, max_limits);
		end
	end
end